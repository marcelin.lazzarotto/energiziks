(function()
{
    /**
     *  @function main
     *      Script entry point.
     */
    let main = function ()
    {
        MixEnergetik.LoaderManager && MixEnergetik.LoaderManager.initialize();

        MixEnergetik.MasterManager.initializeSilence();

        /* Initialize things */
        MixEnergetik.SoloManager.initialize();
        MixEnergetik.UI.ApplicationControlView.initialize();
        MixEnergetik.UI.SequencerView.initialize();
        MixEnergetik.UI.ChargeView.initialize();
        MixEnergetik.UI.VideoView.initialize();
        MixEnergetik.UI.VideoView.hide();
        MixEnergetik.UI.MapView.initialize();
        MixEnergetik.DragAndDropManager.initialize();
        MixEnergetik.UI.GroupDataView.initialize();
        MixEnergetik.GroupDataManager.initialize();
        MixEnergetik.UI.TradeView.initialize();
        MixEnergetik.TradeManager.initialize();
        MixEnergetik.VideoManager.initialize();
        MixEnergetik.ApplicationControlManager.initialize();
        MixEnergetik.DateManager && MixEnergetik.DateManager.initialize();
        MixEnergetik.UI.DatePickerView && MixEnergetik.UI.DatePickerView.initialize();
        MixEnergetik.GroupManager.initialize();
        MixEnergetik.SampleManager.initialize();
        MixEnergetik.SequencerManager.initialize();

        MixEnergetik.UI.CreditsView.initialize();
        MixEnergetik.UI.Preview.initialize();

        /* Initialize regions with element samples */
        MixEnergetik.RegionManager.initialize();
    };

    {
        function __loadMain()
        {
            window.removeEventListener("load", __loadMain, false);
            main();
        }
        window.addEventListener("load", __loadMain, false);
    }
})();
