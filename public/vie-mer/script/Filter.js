/*
 *  File:   vie-mer/script/Filter.js
 *  Author: Marcelin Lazzarotto
 *  Date:   2019-04-25
 *
 *  Brief:  For vie-mer page, describes a pointer on the map.
 */

"use strict";

/**
 *  @namespace MixEnergetik
 *      The project namespace, to avoid polluting global state.
 */
var MixEnergetik = MixEnergetik || {};

/**
 *  @namespace MixEnergetik.VieMer
 *      Subnamespace for VieMer related elements.
 */
MixEnergetik.VieMer = MixEnergetik.VieMer || {};

/**
 *  @constructor Filter
 *      Constructor for a filter object.
 *      Filters allow for seing specific pointers only.
 *
 *  @param {integer} depth
 *      The filter will detect/undetect pointers of the given depth.
 */
MixEnergetik.VieMer.Filter = (function()
{
    let Filter = function Filter(depth)
    {
        MixEnergetik.Utilitary.Check.native(depth, "number",
            "MixEnergetik.VieMer.Filter()", "depth");

        /*
         *  Declarations.
         */

         /**
          *  @property {integer} depth
          *      The depth of the pointers the object will detect.
          */
        this.depth = null;

        /**
         *  @property {HTMLDivElement} container
         *      The filter container.
         */
        this.container = null;

        /**
         *  @property {boolean} selected
         *      Keeps track of the filter selection.
         */
        this.selected = false;

        /* Helpers */
        const DEPTH_KEY = MixEnergetik.VieMer.DEPTH_PREFIX + depth;

        let self = this;

        let clickCallback = function clickCallback(event)
        {
            if (self.selected)
            {
                self.unselect();
            }
            else
            {
                let allPointers = MixEnergetik.VieMer.pointers[
                    MixEnergetik.VieMer.Navigation.currentMap];

                for (let zone in allPointers)
                {
                    const pointers = allPointers[zone][self.depth];
                    for(let i = 0; i < pointers.length; i++)
                    {
                        setTimeout(function()
                        {
                            if (self.selected)
                            {
                                pointers[i].container.classList.add("detected");
                            }
                        }, 10 * i);
                    }
                }

                self.container.classList.add("selected");
                self.selected = !self.selected;
            }
        };

        /*
         *  Construction
         */
        this.depth = depth;

        this.container = document.createElement("div");
        this.container.classList.add("pointer");
        this.container.classList.add("filter");
        this.container.classList.add(DEPTH_KEY);

        this.container.addEventListener("click", clickCallback, false);
    };

    /**
     *  @function unselect
     *      Helper to unselect the filter and thus hide pointers.
     */
    Filter.prototype.unselect = function unselect()
    {
        if (!this.selected)
        {
            return;
        }

        let allPointers = MixEnergetik.VieMer.pointers[
            MixEnergetik.VieMer.Navigation.currentMap];

        for (let zone in allPointers)
        {
            const pointers = allPointers[zone][this.depth];
            for(let i = 0; i < pointers.length; i++)
            {
                pointers[i].container.classList.remove("detected");
            }
        }

        this.container.classList.remove("selected");
        this.selected = !this.selected;
    };

    return Filter;
})();
