/**
 *  File:   menuMain.js
 *  Author: Marcelin lazzarotto
 *  Date:   2018-07-16
 */

"use strict";

var Menu = Menu || {};

Menu.Main = (function()
{
    let Main = {};

    Main.initialize = function initialize()
    {
        Main_container = document.getElementById("MenuMain");
        Main_buttonEnergyFrance = document.getElementById("MenuMainButtonEnergyFrance");
        Main_buttonCO2 = document.getElementById("MenuMainButtonCO2");
        Main_buttonEnergyPlatform = document.getElementById("MenuMainButtonEnergyPlatform");
        Main_textEnergyFrance = document.getElementById("MenuMainTextEnergyFrance");
        Main_textCO2 = document.getElementById("MenuMainTextCO2");
        Main_textEnergyPlatform = document.getElementById("MenuMainTextEnergyPlatform");

        Main_audioAmbient = new Pizzicato.Sound(
            {
                source: "file",
                options:
                {
                    path: "/energiziks/resources/modules/Menu/Main/Ambient.ogg",
                    loop: true
                }
            }
        );
        Main_audioHover = new Pizzicato.Sound(
            {
                source: "file",
                options:
                {
                    path: "/energiziks/resources/modules/Menu/Main/Hover.ogg",
                    loop: true
                }
            }
        );

        Main_textEnergyFrance.classList.add("hidden");
        Main_textCO2.classList.add("hidden");
        Main_textEnergyPlatform.classList.add("hidden");

        Main_buttonEnergyFrance.addEventListener("mouseenter",
            function(event)
            {
                Main_textEnergyFrance.classList.remove("hidden");
                Main_audioHover.play(0.0, 0.0);
            }, false);
        Main_buttonCO2.addEventListener("mouseenter",
            function(event)
            {
                Main_textCO2.classList.remove("hidden");
                Main_audioHover.play(0.0, 0.0);
            }, false);
        Main_buttonEnergyPlatform.addEventListener("mouseenter",
            function(event)
            {
                Main_textEnergyPlatform.classList.remove("hidden");
                Main_audioHover.play(0.0, 0.0);
            }, false);
        Main_buttonEnergyFrance.addEventListener("mouseleave",
            function(event)
            {
                Main_textEnergyFrance.classList.add("hidden");
                Main_audioHover.stop();
            }, false);
        Main_buttonCO2.addEventListener("mouseleave",
            function(event)
            {
                Main_textCO2.classList.add("hidden");
                Main_audioHover.stop();
            }, false);
        Main_buttonEnergyPlatform.addEventListener("mouseleave",
            function(event)
            {
                Main_textEnergyPlatform.classList.add("hidden");
                Main_audioHover.stop();
            }, false);

        Main_buttonEnergyFrance.addEventListener("click",
            function()
            {
                Menu.Main.hide();
                Menu.EnergyFrance.show();
            }, false);

        Main.hide();
    };

    Main.show = function show()
    {
        Main_container.classList.remove("hidden");
    };

    Main.hide = function hide()
    {
        Main_container.classList.add("hidden");
    };

    Main.playAudio = function playAudio()
    {
        Main_audioAmbient.play(0.0, 0.0);
    };

    let Main_container = null;
    let Main_buttonEnergyFrance = null;
    let Main_buttonCO2 = null;
    let Main_buttonEnergyPlatform = null;
    let Main_textEnergyFrance = null;
    let Main_textCO2 = null;
    let Main_textEnergyPlatform = null;

    let Main_audioAmbient = null;
    let Main_audioHover = null;

    return Main;
})();
