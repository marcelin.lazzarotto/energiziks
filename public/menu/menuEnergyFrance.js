/**
 *  File:   menuEnergyFrance.js
 *  Author: Marcelin lazzarotto
 *  Date:   2018-07-16
 */

"use strict";

var Menu = Menu || {};

Menu.EnergyFrance = (function()
{
    let EnergyFrance = {};

    EnergyFrance.initialize = function initialize()
    {
        EnergyFrance_container = document.getElementById("MenuEnergyFrance");
        EnergyFrance_buttonBack = document.getElementById("ButtonBack");

        EnergyFrance_buttonBack.addEventListener("click",
            function()
            {
                Menu.EnergyFrance.hide();
                Menu.Main.show();
            }, false);

        Menu.EnergyFrance.hide();
    };

    EnergyFrance.show = function show()
    {
        EnergyFrance_container.classList.remove("hidden");
    };

    EnergyFrance.hide = function hide()
    {
        EnergyFrance_container.classList.add("hidden");
    };

    let EnergyFrance_container = null;
    let EnergyFrance_buttonBack = null;

    return EnergyFrance;
})();
