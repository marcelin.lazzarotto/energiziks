/**
 *  File:   menuIntroduction.js
 *  Author: Marcelin lazzarotto
 *  Date:   2018-07-16
 */

"use strict";

var Menu = Menu || {};

Menu.Introduction = (function()
{
    let Introduction = {};

    Introduction.hide = function hide()
    {
        Introduction_container.classList.add("hidden");
    };

    Introduction.show = function show()
    {
        Introduction_container.classList.remove("hidden");
    };

    Introduction.initialize = function initialize()
    {
        Introduction_container = document.getElementById("MenuIntroduction");
        Introduction_video = document.getElementById("MenuIntroductionVideo");
        Introduction_aboutElements = document.getElementsByClassName("about");
        Introduction_buttonReadMore = document.getElementById(
            "MenuIntroductionButtonReadMore");
        Introduction_buttonPrevious = document.getElementById(
            "MenuIntroductionButtonPrevious");
        Introduction_buttonNext = document.getElementById(
            "MenuIntroductionButtonNext");
        Introduction_buttonPass = document.getElementById(
            "MenuIntroductionButtonPass");
        Introduction_audioAmbient = new Pizzicato.Sound("/energiziks/resources/modules/Menu/Introduction/Ambient.ogg",
            function()
            {
                Introduction_video.play();
                Introduction_audioAmbient.play();
            });

        document.getElementById(
            "MenuIntroductionAudioAmbient");

        Introduction_hideAboutElements();
        Introduction_buttonReadMore.classList.add("hidden");
        Introduction_buttonPrevious.classList.add("hidden");
        Introduction_buttonNext.classList.add("hidden");
        Introduction_buttonPass.classList.add("hidden");

        Introduction_video.addEventListener("click", Introduction_pass, false);
        Introduction_video.addEventListener("timeupdate",
            Introduction_showButtonReadMore, false);
        Introduction_buttonReadMore.addEventListener("click",
            function(event)
            {
                Introduction_video.removeEventListener("ended",
                    Introduction_pass, false);

                Introduction_buttonReadMore.classList.add("hidden");
                Introduction_video.classList.add("hidden");

                Introduction_buttonPass.classList.add("slide");
                Introduction_buttonNext.classList.remove("hidden");
                Introduction_aboutElements[0].classList.remove("hidden");
            }, false);

        Introduction_buttonPrevious.addEventListener("click",
            function()
            {
                Introduction_setPage(Introduction_currentPage - 1);
            }, false);
        Introduction_buttonNext.addEventListener("click",
            function()
            {
                Introduction_setPage(Introduction_currentPage + 1);
            }, false);
        Introduction_buttonPass.addEventListener("click", Introduction_pass,
            false);

        Introduction_audioAmbient.on("end",
            function()
            {
                Menu.Main.playAudio();
                Introduction_audioAmbient.off("end");
            }, false);

        // Introduction_audioAmbient.play();
    };

    let Introduction_container = null;
    let Introduction_video = null;
    let Introduction_aboutElements = [];
    let Introduction_buttonReadMore = null;
    let Introduction_buttonPrevious = null;
    let Introduction_buttonNext = null;
    let Introduction_buttonPass = null;
    let Introduction_currentPage = 0;
    let Introduction_audioAmbient = null;

    let Introduction_hideAboutElements = function hideAboutElements(event)
    {
        for (let i = 0; i < Introduction_aboutElements.length; i++)
        {
            Introduction_aboutElements[i].classList.add("hidden");
        }
    };

    let Introduction_pass = function pass(event)
    {
        Menu.Introduction.hide();
        Menu.Main.show();
    }

    let Introduction_showButtonReadMore = function showButtonReadMore(event)
    {
        if (Introduction_video.currentTime < 21.0)
        {
            return;
        }

        Introduction_buttonReadMore.classList.remove("hidden");
        Introduction_buttonPass.classList.remove("hidden");
        Introduction_video.removeEventListener("timeupdate",
            Introduction_showButtonReadMore, false);
        Introduction_video.removeEventListener("click",
            Introduction_pass, false);
        Introduction_video.addEventListener("ended",
            Introduction_pass, false);
    };

    let Introduction_setPage = function setPage(pageNumber)
    {
        const maxPage = Introduction_aboutElements.length - 1;

        if ((pageNumber < 0) || (pageNumber > maxPage))
        {
            console.error("[Menu.Introduction_setPage()] Invalid page number.");
            return;
        }

        Introduction_aboutElements[Introduction_currentPage].classList.add("hidden");

        Introduction_currentPage = pageNumber;

        Introduction_buttonPrevious.classList.remove("hidden");
        Introduction_buttonNext.classList.remove("hidden");

        if (Introduction_currentPage === 0)
        {
            Introduction_buttonPrevious.classList.add("hidden");
        }
        else if (Introduction_currentPage === maxPage)
        {
            Introduction_buttonNext.classList.add("hidden");
        }

        Introduction_aboutElements[Introduction_currentPage].classList.remove("hidden");
    };

    return Introduction;
})();
