/*
 *  File:   SoloManager.js
 *  Author: Marcelin Lazzarotto
 *  Date:   2018-06-06
 */

"use strict";

/**
 *  @namespace MixEnergetik
 *      The project namespace, to avoid polluting global state.
 */
var MixEnergetik = MixEnergetik || {};

/**
 *  @module SoloManager
 *      The kinda big interface between everything.
 */
MixEnergetik.SoloManager = (function()
{
    let SoloManager = {};

    /**
     *  @property {MixEnergetik.UI.AudioItem} soloItem
     *      Keeps track of the AudioItem object played in solo.
     */
    SoloManager.soloItem = null;

    /**
     *  @property {MixEnergetik.Utilitary.Event} event
     *      Event manager for the solo manager.
     *      Possible events are:
     *      "play", when the an audio item has just started playing solo
     *      "stop", when the an audio item has just stopped playing solo
     */
    SoloManager.event = new MixEnergetik.Utilitary.Event();

    /**
     *  @function initialize
     */
    SoloManager.initialize = function initialize()
    {
    };

    /**
     *  @function setSoloItem
     *      Plays the given sample in solo.
     *
     *  @param {MixEnergetik.UI.AudioItem} soloItem
     */
    SoloManager.setSoloItem = function setSoloItem(soloItem)
    {
        const AppCtrlM = MixEnergetik.ApplicationControlManager;
        const SoloM = MixEnergetik.SoloManager;
        const GroupM = MixEnergetik.GroupManager;
        const SeqM = MixEnergetik.SequencerManager;
        const AudioM = MixEnergetik.AudioManager;

        if (SoloM.soloItem !== null)
        {
            SoloManager.clear();
        }

        let elapsedTime = 0.0;

        elapsedTime = AudioM.getCurrentTime();

        if (GroupM.isActive())
        {
            GroupM.pause();
        }
        else if (SeqM.isActive())
        {
            SeqM.pause();
        }

        SoloM.soloItem = soloItem;

        if (soloItem instanceof MixEnergetik.UI.SampleItem)
        {
            AudioM.addSound(soloItem.soundData, elapsedTime);
        }
        else /* if (soloItem instanceof MixEnergetik.UI.GroupItem) */
        {
            AudioM.addGroup(soloItem.groupData, elapsedTime);
        }

        soloItem.soloOn();

        MixEnergetik.SoloManager.event.trigger("play");
    };

    /**
     *  @function clear
     *      Removes all from the solo. Returns to normal too.
     */
    SoloManager.clear = function clear()
    {
        const AppCtrlM = MixEnergetik.ApplicationControlManager;
        const SoloM = MixEnergetik.SoloManager;
        const GroupM = MixEnergetik.GroupManager;
        const SeqM = MixEnergetik.SequencerManager;
        const AudioM = MixEnergetik.AudioManager;

        if (SoloM.soloItem === null)
        {
            return;
        }

        let elapsedTime = 0.0;

        elapsedTime = AudioM.getCurrentTime();

        if (SoloM.soloItem instanceof MixEnergetik.UI.SampleItem)
        {
            AudioM.removeSound(SoloM.soloItem.soundData);
        }
        else /* if (SoloM.soloItem instanceof MixEnergetik.UI.GroupItem) */
        {
            AudioM.removeGroup(SoloM.soloItem.groupData);
        }

        SoloM.soloItem.soloOff();

        if (GroupM.isActive())
        {
            GroupM.resume(elapsedTime);
        }
        else if (SeqM.isActive())
        {
            SeqM.resume(elapsedTime);
        }

        SoloM.soloItem = null;

        MixEnergetik.SoloManager.event.trigger("stop");
    };

    return SoloManager;
})();
