/*
 *  File:   LoaderManager.js
 *  Author: Marcelin Lazzarotto
 *  Date:   2018-05-11

/**
 *  @namespace MixEnergetik
 *      The project namespace, to avoid polluting global state.
 */
var MixEnergetik = MixEnergetik || {};

/**
 *  @module LoaderManager
 *      The manager dealing with things that loads, to lock the app while it
 *      loads.
 */
MixEnergetik.LoaderManager = (function()
{
    let LoaderManager = {};

    const LoaderManager_APPLICATION_ID = "Application";
    const LoaderManager_LOAD_LAYER_ID = "LoadLayer";
    const LoaderManager_LOAD_INFORMATION_ID = "LoadInformation";
    let LoaderManager_applicationContainer = null;
    let LoaderManager_loadLayer = null;
    let LoaderManager_loadInformation = null;
    let LoaderManager_loadedCount = +0;
    let LoaderManager_resourceCount = +0;

    let LoaderManager_printInformation = function printInformation()
    {
        return ("Ressource " + LoaderManager_loadedCount + " sur "
            + LoaderManager_resourceCount + "...");
    };

    LoaderManager.event = new MixEnergetik.Utilitary.Event();

    /**
     *  @function notifyLoading
     *      Function to call when any resource needs to be loaded.
     */
    LoaderManager_notifyLoading = function notifyLoading()
    {
        if ((LoaderManager_resourceCount !== 0))
        {
            /* First */
            LoaderManager_loadLayer.classList.remove("hidden");
            LoaderManager_applicationContainer.classList.add("noninteractable");
        }

        LoaderManager_loadInformation.innerHTML = LoaderManager_printInformation();

        LoaderManager_resourceCount++;
    };

    /**
     *  @function notifyLoaded
     *      Function to call when any resource has successfully been loaded.
     */
    LoaderManager_notifyLoaded = function notifyLoaded()
    {
        LoaderManager_loadedCount++;

        LoaderManager_loadInformation.innerHTML = LoaderManager_printInformation();

        if (LoaderManager_loadedCount >= LoaderManager_resourceCount)
        {
            LoaderManager.event.trigger("complete");

            /* We can leave the hand to the correct program functioning. */
            LoaderManager_loadLayer.classList.add("hidden");
            LoaderManager_applicationContainer.classList.remove("noninteractable");

            LoaderManager_loadedCount   = 0;
            LoaderManager_resourceCount = 0;
        }
    };

    LoaderManager.initialize = function initialize()
    {
        LoaderManager_applicationContainer = document.getElementById(LoaderManager_APPLICATION_ID);
        LoaderManager_loadLayer = document.getElementById(LoaderManager_LOAD_LAYER_ID);
        LoaderManager_loadInformation = document.getElementById(LoaderManager_LOAD_INFORMATION_ID);

        MixEnergetik.Utilitary.SoundBuilder.event.on("fetch", LoaderManager_notifyLoading);
        MixEnergetik.Utilitary.SoundBuilder.event.on("load", LoaderManager_notifyLoaded);
        MixEnergetik.DateManager && MixEnergetik.DateManager.event.on("fetch", LoaderManager_notifyLoading);
        MixEnergetik.DateManager && MixEnergetik.DateManager.event.on("load", LoaderManager_notifyLoaded);
        MixEnergetik.PersistenceManager && MixEnergetik.PersistenceManager.event.on("fetch", LoaderManager_notifyLoaded);
        MixEnergetik.PersistenceManager && MixEnergetik.PersistenceManager.event.on("load", LoaderManager_notifyLoaded);

        LoaderManager_loadLayer.classList.add("hidden");
    };

    return LoaderManager;
})();
