/*
 *  File:   DateManager.js
 *  Author: Marcelin Lazzarotto
 *  Date:   2018-03-03
 */

"use strict";

/**
 *  @namespace MixEnergetik
 *      The project namespace, to avoid polluting global state.
 */
var MixEnergetik = MixEnergetik || {};

/**
 *  @module DateManager
 *      Acts as an interface for the Eco2Mix API calls.
 */
MixEnergetik.DateManager = (function()
{
    /* Request for a day example: https://rte-opendata.opendatasoft.com/api/records/1.0/search/?dataset=eco2mix_regional_tr&rows=288&sort=-date_heure&facet=libelle_region&facet=nature&facet=date_heure&facet=date&refine.date=2018-04-03
    */

    /* RTE request example
    * rte-opendata.opendatasoft.com/api/datasets/1.0/search/
    * for us:
    * /api/records/1.0/search/?dataset=eco2mix_regional_tr&rows=12&sort=date_heure&facet=libelle_region&facet=nature&facet=date_heure
    */

    /**
     *  @constructor Eco2MixRequest
     *      Warps a request to eco2mix.
     */
    let Eco2MixRequest = function Eco2MixRequest()
    {
        /**
         *  @property {string} dataset
         *      Name of the dataset on which we need seeking.
         */
        this.dataset = null;
        /**
         *  @property {Date} date
         *      The date at which we want the data.
         */
        this.date = null;
    };


    /**
     *  @property {string} URL
     *      The base URL on which to perform request.
     */
    Eco2MixRequest.prototype.URL = "https://opendata.reseaux-energies.fr";
    /**
     *  @property {string} SEARCH_PATH
     *      The search path for the resquests.
     */
    Eco2MixRequest.prototype.SEARCH_PATH = "/api/records/1.0/search";
    /**
     *  @property {number} REGION_COUNT
     *      The number of region available in eco2mix.
     */
    Eco2MixRequest.prototype.REGION_COUNT = 12;
    /**
     *  @property {string[]} FACETS
     *      The needed facets to perform the correct research.
     */
    Eco2MixRequest.prototype.FACETS = ["libelle_region", "nature", "date_heure"];

    /**
     *  @function prepareRealTime
     *      Prepare the Eco2MixRequest object for a real time dataset request.
     *
     *  @param {Date} date
     *      The date from which retrieve eco2mix data.
     */
    Eco2MixRequest.prototype.prepareRealTime = function prepareRealTime(date)
    {
        this.dataset = "eco2mix-regional-tr";
        this.date = date;
    };

    /**
     *  @function prepareRealTime
     *      Prepare the Eco2MixRequest object for a consolidated dataset
     *      request.
     *
     *  @param {Date} date
     *      The date from which retrieve eco2mix data.
     */
    Eco2MixRequest.prototype.prepareDefinitive = function prepareDefinitive(date)
    {
        this.dataset = "eco2mix-regional-cons-def";
        this.date = date;
    };

    /**
     *  @function getRequestURL
     *      Returns the URL from the params of the request.
     *
     *  @returns {string}
     *      The request URL, or an empty string if the request wasn't prepared.
     */
    Eco2MixRequest.prototype.getRequestURL = function getRequestURL()
    {
        if ((this.dataset === undefined) || (this.dataset === null))
        {
            return "";
        }

        let url = "";
        let dateString = MixEnergetik.DateManager.formatDate(this.date);
        let timeString = MixEnergetik.DateManager.formatHours(this.date.getHours())
            + "%3A" + MixEnergetik.DateManager.formatMinutes(this.date.getMinutes());

        url += Eco2MixRequest.prototype.URL;
        url += Eco2MixRequest.prototype.SEARCH_PATH;
        url += "/?dataset=" + this.dataset;
        url += "&rows=" + (24 * 4 * Eco2MixRequest.prototype.REGION_COUNT).toString();
        url += "&sort=-date_heure";

        for (let i = 0; i < Eco2MixRequest.prototype.FACETS.length; i++)
        {
            url += "&facet=" + Eco2MixRequest.prototype.FACETS[i];
        }

        url += "&facet=date&refine.date=" + dateString;

        return url;
    };

    /**
     *  @function send
     *      Sends the request to the server so that it retrieves data.
     *      Nothing happens if the request was not prepared.
     *
     *  @param {function} successHandler(response)
     *      Callback in case of successful request to eco2mix. Request can still
     *      have no result.
     *  @param {function} errorHandler()
     *      Callback when the request has failed.
     */
    Eco2MixRequest.prototype.send = function send(successHandler, errorHandler)
    {
        if ((this.dataset === undefined) || (this.dataset === null))
        {
            return;
        }

        let request = new XMLHttpRequest();
        let url = this.getRequestURL();

        request.responseType = "json";
        request.onreadystatechange = function onreadystatechange(event)
        {
            if (request.readyState === XMLHttpRequest.DONE)
            {
                if (request.status === 200)
                {
                    successHandler && successHandler(request.response);
                }
                else
                {
                    errorHandler && errorHandler();
                }
            }
        }

        request.open("GET", url, true);
        request.send(null);
    };

    let DateManager = {};

    /**
     *  @property {Date} currentTime
     *      The time selected for the production/consumption.
     */
    DateManager.currentTime = null;

    /**
     *  @property {MixEnergetik.Utilitary.Event} event
     *      Event interface.
     */
    DateManager.event = new MixEnergetik.Utilitary.Event();

    /**
     *  @property {object} database
     *      Local object to store all the data recovered from eco2mix, so that
     *      we got quick access.
     *
     *  @warning
     *      Might be heavy.
     *
     *  @tree
     *      database:
     *      {
     *          <date_hour_iso>:
     *          {
     *              <libelle_region>: record,
     *              ...
     *          },
     *          ...
     *      }
     */
    DateManager.database = {};

    /**
     *  @function roundToHalf
     *      Returns the closest half of hours lower than given minutes.
     *
     *  @param {number} minutes
     *      The minutes to round.
     *
     *  @returns {number}
     *      The rounded down minutes.
     */
    DateManager.roundToHalf = function roundToHalf(minutes)
    {
        MixEnergetik.Utilitary.Check.native(minutes, "number",
            "MixEnergetik.DateManager.roundToHalf()", "minutes");

        // if (minutes < 15)
        // {
        //     return 0;
        // }
        // else if (minutes < 30)
        // {
        //     return 15;
        // }
        // else if (minutes < 45)
        // {
        //     return 30;
        // }
        // else if (minutes < 60)
        // {
        //     return 45;
        // }
        // else
        // {
        //     return 0;
        // }
        if (minutes < 30)
        {
            return 0;
        }
        else if (minutes < 60)
        {
            return 30;
        }
        else
        {
            return 0;
        }
    };

    /**
     *  @function formatHours
     *      Returns a string describing hours to the format hh.
     *
     *  @param {number} hours
     *      The hours.
     *
     *  @returns {string}
     *      The formated string.
     */
    DateManager.formatHours = function formatHours(hours)
    {
        MixEnergetik.Utilitary.Check.native(hours, "number",
            "MixEnergetik.DateManager.formatHours()", "hours");

        let response = "";

        if (hours > 23)
        {
            hours = 0;
        }

        if (hours < 10)
        {
            response += "0";
        }

        response += hours.toString(10);

        return response;
    };

    /**
     *  @function formatMinutes
     *      Returns a string describing minutes to the format mm.
     *
     *  @param {number} minutes
     *      The minutes.
     *
     *  @returns {string}
     *      The formated string.
     */
    DateManager.formatMinutes = function formatMinutes(minutes)
    {
        MixEnergetik.Utilitary.Check.native(minutes, "number",
            "MixEnergetik.DateManager.formatMinutes()", "minutes");

        let response = "";

        if (minutes > 59)
        {
            minutes = 0;
        }

        if (minutes < 15)
        {
            response += "0";
        }

        response += minutes.toString(10);

        return response;
    };

    /**
     *  @function formatTime
     *      Returns a string describing the time to a proper format (xy:zw).
     *
     *  @param {number} hours
     *      The hours.
     *  @param {number} minutes
     *      The minutes.
     *
     *  @returns {string}
     *      The formated string.
     */
    DateManager.formatTime = function formatTime(hours, minutes)
    {
        MixEnergetik.Utilitary.Check.native(hours, "number",
            "MixEnergetik.DateManager.formatTime()", "hours");
        MixEnergetik.Utilitary.Check.native(minutes, "number",
            "MixEnergetik.DateManager.formatTime()", "minutes");

        return (MixEnergetik.DateManager.formatHours(hours) + ":"
            + MixEnergetik.DateManager.formatMinutes(minutes));
    };

    /**
     *  @function formatDate
     *      Returns a string describing the time to a short ISO 8601 format
     *      (yyyy-mm-dd).
     *
     *  @param {Date} date
     *      The javascript date object.
     *
     *  @returns {string}
     *      The formated string.
     */
    DateManager.formatDate = function formatDate(date)
    {
        MixEnergetik.Utilitary.Check.object(date, Date,
            "MixEnergetik.DateManager.formatDate()", "date", "Date");

        let response = "";
        response += date.getFullYear();
        response += "-";

        {
            let month = date.getMonth() + 1;

            if (month < 10)
            {
                response += "0";
            }

            response += month;
        }

        response += "-";

        {
            let day = date.getDate();

            if (day < 10)
            {
                response += "0";
            }

            response += day;
        }

        return response;
    };

    /**
     *  @function formatAlmostISO
     *      Format the date so that the string is to an ISO format in adequation
     *      with eco2mix.
     *  @note
     *      "date_heure": "2018-04-02T22:00:00+00:00"
     */
    DateManager.formatAlmostISO = function formatAlmostISO(date)
    {
        MixEnergetik.Utilitary.Check.object(date, Date,
            "MixEnergetik.DateManager.formatDate()", "date", "Date");

        let isoDate = "";

        isoDate += MixEnergetik.DateManager.formatDate(date);
        isoDate += "T";
        isoDate += MixEnergetik.DateManager.formatTime(date.getHours(), date.getMinutes());
        isoDate += ":00+00:00";

        return isoDate;
    };

    /**
     *  @function fetchRegions
     *      Fetch regions data for a given date.
     *
     *  @param {Date} date
     *      The date we would like to seek the data of.
     *
     *  @returns {string}
     *      A string describing status:
     *      - "DISTANT_FETCH" if the function had to request for values.
     *      - "LOCAL_FETCH" if the data are already available the application.
     */
    DateManager.fetchRegions = function fetchRegions(date)
    {
        const DateM = MixEnergetik.DateManager;

        let populateDatabase = function populateDatabase(response)
        {
            let records = response.records;

            for (let i = 0; i < records.length; i++)
            {
                let record = records[i];
                let dateEx = record.fields.date;
                let heure = record.fields.heure;
                let libelleRegion = record.fields.libelle_region;

                DateM.database[dateEx] = DateM.database[dateEx] || {};
                DateM.database[dateEx][heure] = DateM.database[dateEx][heure] || {};
                DateM.database[dateEx][heure][libelleRegion] = record.fields;
            }
        };

        let successHandlerDef = function successHandlerDef(response)
        {
            if (response.nhits === 0)
            {
                /* Definitively impossible to find. */
                /* TODO urg */
                alert("Impossible de récupérer les données d'Eco2Mix.");
            }
            else
            {
                populateDatabase(response);
            }

            DateM.event.trigger("load");
        };

        let successHandlerRT = function successHandlerRT(response)
        {
            if (response.nhits === 0)
            {
                /* Failed, search in older data */
                let request = new Eco2MixRequest();

                request.prepareDefinitive(date); /* 2018-04-03 12:00 */
                request.send(successHandlerDef, errorHandler);
            }
            else
            {
                populateDatabase(response);

                DateM.event.trigger("load");
            }
        };

        let errorHandler = function errorHandler()
        {
            /* TODO urg */
            alert("Impossible de récuperer les informations depuis"
            + " eco2mix");

            DateM.event.trigger("load");
        };

        MixEnergetik.Utilitary.Check.object(date, Date,
            "MixEnergetik.DateManager.fetchRegions()", "date", "Date");

        let entry = DateM.database[DateM.formatDate(date)];
        if ((entry !== null) && (entry !== undefined))
        {
            /* Don't do uneeded work. */
            return "LOCAL_FETCH";
        }

        let request = new Eco2MixRequest();

        request.prepareRealTime(date); /* 2018-04-03 12:00 */
        request.send(successHandlerRT, errorHandler);

        DateM.event.trigger("fetch");

        return "DISTANT_FETCH";
    };

    /**
     *  @function getIdFromLibelle
     *      Returns our corresponding id from the eco2mix region's libelle.
     *
     *  @param {string} libelleRegion
     *      The region's libelle, gotten from their JSON response.
     */
    let DateManager_getIdFromLibelle = function getIdFromLibelle(libelleRegion)
    {
        switch (libelleRegion)
        {
            case "Hauts-de-France":
                return "Hauts-de-France";
            case "Normandie":
                return "Normandie";
            case "Bretagne":
                return "Bretagne";
            case "Pays de la Loire":
                return "PaysDeLaLoire";
            case "Nouvelle-Aquitaine":
                return "Nouvelle-Aquitaine";
            case "Ile-de-France":
                return "Ile-de-France";
            case "Grand Est": /* Trash leftover from opendata changing libelle */
                return "GrandEst";
            case "Grand-Est":
                return "GrandEst";
            case "Bourgogne-Franche-Comté":
                return "Bourgogne-Franche-Comte";
            case "Centre-Val de Loire":
                return "Centre-ValDeLoire";
            case "Auvergne-Rhône-Alpes":
                return "Auvergne-Rhone-Alpes";
            case "Occitanie":
                return "Occitanie";
            case "Provence-Alpes-Côte d'Azur":
                return "Provence-Alpes-CoteDAzur";
            default: /* Corse */
                throw new Error("The region: " + libelleRegion
                    + " is not available now"); // TODO
                return "Corse";
        }
    };

    /**
     *  @function updateRegion
     *      Update a region to newly fectched data.
     *
     *  @param {object} regionJSON
     *      The descriptor corresponding to the new data.
     */
    let DateManager_updateRegion = function updateRegion(regionJSON)
    {
        const RegionM = MixEnergetik.RegionManager;
        const MapV = MixEnergetik.UI.MapView;

        const REGION_ID = DateManager_getIdFromLibelle(regionJSON.libelle_region);

        let region = RegionM.regions[REGION_ID];

        region.trade = +regionJSON.ech_physiques || 0;
        region.production.bioenergy = +regionJSON.bioenergies || 0;
        region.production.nuclear = +regionJSON.nucleaire || 0;
        region.production.eolian = +regionJSON.eolien || 0;
        region.production.hydrolic = +regionJSON.hydraulique || 0;
        region.production.thermique = +regionJSON.thermique || 0;
        region.production.solar = +regionJSON.solaire || 0;
        region.production.pumping = +regionJSON.pompage || -0;
        region.consumption = +regionJSON.consommation || 0;
        region.timestamp = new Date(regionJSON.date_heure);
    };

    /**
     *  @function updateRegions
     *      Update regions data for a given date.
     *
     *  @param {Date} date
     *      The date we would like to seek the data of.
     *
     *  @returns {string}
     *      A string describing status:
     *      - "DISTANT_FETCH" if the function had to request for values.
     *      - "LOCAL_FETCH" if the data are already available the application.
     */
    DateManager.updateRegions = function updateRegions(date)
    {
        MixEnergetik.Utilitary.Check.object(date, Date,
            "MixEnergetik.DateManager.updateRegions()", "date", "Date");

        const DateM = MixEnergetik.DateManager;

        let dateEx = DateM.formatDate(date);

        if (!(DateM.database[dateEx]))
        {
            throw new Error("[MixEnergetik.DateManager.updateRegions()]"
                + " function was called on unfetched date");
        }

        let time = DateM.formatTime(date.getHours(), date.getMinutes());
        let records = DateM.database[dateEx][time];

        for (let libelleRegion in records)
        {
            DateManager_updateRegion(records[libelleRegion]);
        }

        MixEnergetik.RegionManager.updateAllSamples();
    }

    /**
     *  @function fetchAndUpdateRegions
     *      Fetch regions data for a given date and triggers region update.
     *
     *  @param {Date} date
     *      The date we would like to seek the data of.
     *
     *  @returns {string}
     *      Status string indicating the update state. Values are:
     *      - "IMMEDIATE_UPDATE" if data were found locally.
     *      - "DELAYED_UPDATE" if date had to be fetch remotelly.
     */
    DateManager.fetchAndUpdateRegions = function fetchAndUpdateRegions(date)
    {
        MixEnergetik.Utilitary.Check.object(date, Date,
            "MixEnergetik.DateManager.fetchAndUpdateRegions()", "date", "Date");

        const DateM = MixEnergetik.DateManager;

        let callbackId = null;

        let delayedUpdate = function delayedUpdate()
        {
            MixEnergetik.DateManager.event.off("load", callbackId);

            DateM.updateRegions(date);
        };

        let status = "";

        status = MixEnergetik.DateManager.fetchRegions(date);

        if (status === "LOCAL_FETCH")
        {
            DateM.updateRegions(date);
            return "IMMEDIATE_UPDATE";
        }
        else
        {
            callbackId = MixEnergetik.DateManager.event.on("load", delayedUpdate);
            return "DELAYED_UPDATE";
        }
    };

    /**
     *  @function initialize
     *      Sets the date to a default day.
     */
    DateManager.initialize = function initialize()
    {
        let date = new Date();
        /* Datas are not available immediatelly, apparently, so get back a
         * little bit. */
        date.setHours(date.getHours() - 2);
        {
            /* Correct date to properly fit with eco2mix datas */
            let minutes = date.getMinutes();
            minutes = MixEnergetik.DateManager.roundToHalf(minutes);
            date.setMinutes(minutes);
        }

        MixEnergetik.DateManager.currentTime = date;
    }

    return DateManager;
})();
