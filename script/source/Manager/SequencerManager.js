/*
 *  File:   SequencerManager.js
 *  Author: Marcelin Lazzarotto
 *  Date:   2018-01-29
 */

/**
 *  @namespace MixEnergetik
 *      The project namespace, to avoid polluting global state.
 */
var MixEnergetik = MixEnergetik || {};

/**
 *  @module SequencerManager
 *      The interface dealing with the sequencer.
 */
MixEnergetik.SequencerManager = (function()
{
    let SequencerManager_sequenceStarted = function sequenceStarted(target)
    {
        target.sequencer_fancy.iconPlay.classList.remove("hidden");
    };
    let SequencerManager_sequenceStopped = function sequenceStopped(target)
    {
        target.sequencer_fancy.iconPlay.classList.add("hidden");
    };

    let SequencerManager = Object.create(null,
    {
        /**
         *  @property {integer} currentSequenceNumber
         *      This helps us to know in which sequence we are playing. Value is -1
         *      if the sequencer is off.
         */
        currentSequenceNumber:
        {
            configurable: false,
            enumerable: false,
            writable: true,

            value: -1
        },

        /**
         *  [UGLY HACK]
         *  @property {integer} lastSequenceNumber
         *      Workaround to keep playing video part on wander/ while playing a
         *      preset. Keeps track of the last sequence played, whether from a
         *      simple loop play or an active sequencer play.
         */
        lastSequenceNumber:
        {
            configurable: false,
            enumerable: true,
            writable: true,

            value: 0
        },

        /**
         *  @property {float} volume
         *      Whole sequencer volume, because why not.
         */
        volume:
        {
            configurable: false,
            enumerable: false,
            writable: true,

            value: 1.0
        },

        /**
         *  @property {bool} status
         *      To keep track of the fact we are active or not.
         *      Really need that thing here.
         */
        status:
        {
            configurable: false,
            enumerable: false,
            writable: true,

            value: "off"
        },

        /**
         *  @function isActive
         *      Helper to know if the sequencer is on or off.
         *
         *  @returns {bool}
         *      true if the sequencer runs (basically if currentSequenceNumber >= 0)
         *      false otherwise (currentSequenceNumber < 0)
         */
        isActive:
        {
            configurable: false,
            enumerable: true,
            writable: false,

            value: function()
            {
                return this.status !== "off";
            }
        },

        /**
         *  @function isEmpty
         *      Helper to check if the sequencer is empty, meaning we got nothing
         *      to play though we are active.
         *
         *  @returns {bool}
         *      true if the sequencer is empty (basically if all sequences got no sample)
         *      false otherwise (we got at least 1 sample somewhere)
         */
        isEmpty:
        {
            configurable: false,
            enumerable: true,
            writable: false,

            value: function()
            {
                /* else we check everything */
                for(let i = 0; i < MixEnergetik.UI.SequencerView.length; i++)
                {
                    let sequence = MixEnergetik.UI.SequencerView.at(i);

                    if (sequence.groupData.soundDatas.length)
                    {
                        return false;
                    }
                }

                return true;
            }
        },

        /**
         *  @function play
         *      Called by click on a non-empty sequence element.
         *
         *  @param {MixEnergetik.UI.SequencerView.Bar} sequence
         *      The sequence to restart sequencer to.
         */
        play:
        {
            configurable: false,
            enumerable: true,
            writable: false,

            value: function(sequence)
            {
                if(!this.isActive())
                {
                    return;
                }

                let index = MixEnergetik.UI.SequencerView.sequences.indexOf(sequence);
                let currentSequence = MixEnergetik.UI.SequencerView.at(this.currentSequenceNumber);

                if (index < 0)
                {
                    console.warn("[SequencerManager.play()] Wut?");
                    return;
                }

                if (currentSequence.on)
                {
                    /* deactivate previous first */
                    MixEnergetik.AudioManager.removeGroup(currentSequence.groupData);

                    currentSequence.unuse();
                }

                this.currentSequenceNumber = index;

                MixEnergetik.AudioManager.addGroup(sequence.groupData);

                sequence.use();
            }
        },

        /**
         *  @function getCurrent
         *      Returns the current playing sequence.
         *
         *  @returns {MixEnergetik.UI.GroupItem}
         */
        getCurrent:
        {
            configurable: false,
            enumerable: true,
            writable: false,

            value: function()
            {
                return MixEnergetik.UI.SequencerView.at(this.currentSequenceNumber);
            }
        },

        /**
         *  @function step
         *      Advances to next sequence.
         */
        step:
        {
            value: function()
            {
                let sequence = MixEnergetik.UI.SequencerView.at(this.currentSequenceNumber);

                if (sequence)
                {
                    /* !sequence === we started at -1 (deactivated
                     * sequencer) */

                    MixEnergetik.SequencerManager.event.trigger("sequenceend");

                    SequencerManager_sequenceStopped(sequence);

                    if (MixEnergetik.SoloManager.soloItem === null)
                    {
                        sequence.unuse();
                    }
                }

                /* Stay in sequencer */
                this.currentSequenceNumber = (this.currentSequenceNumber + 1)
                    % MixEnergetik.UI.SequencerView.length;
                this.lastSequenceNumber = this.currentSequenceNumber;

                if (this.currentSequenceNumber === 0)
                {
                    /* Sequencer restarted so call stop and start */
                    /* TODO fix fake call upon first play */
                    MixEnergetik.SequencerManager.event.trigger("end");
                    MixEnergetik.SequencerManager.event.trigger("start");
                }

                sequence = MixEnergetik.UI.SequencerView.at(this.currentSequenceNumber);

                if (MixEnergetik.SoloManager.soloItem === null)
                {
                    sequence.use();
                }

                MixEnergetik.SequencerManager.event.trigger("sequenceplay");

                SequencerManager_sequenceStarted(sequence);
            }
        },

        /**
         *  @function activate
         *      Activate the sequencer, which means that each sequence will be
         *      heard sequencially.
         */
        activate:
        {
            configurable: false,
            enumerable: true,
            writable: false,

            value: function()
            {
                const SeqM = MixEnergetik.SequencerManager;

                if (SeqM.status !== "off")
                {
                    return;
                }

                SeqM.status = "on";

                SeqM.step();
                MixEnergetik.AudioManager.addGroup(SeqM.getCurrent().groupData);

                /* TODO ugly */
                if (MixEnergetik.UI.MapView)
                {
                    MixEnergetik.UI.MapView.disable();
                }

                MixEnergetik.UI.SequencerView.use();
            }
        },

        /**
         *  @function deactivate
         *      Deactivate the sequencer.
         */
        deactivate:
        {
            configurable: false,
            enumerable: true,
            writable: false,

            value: function()
            {
                if (this.status === "off")
                {
                    return;
                }

                let currentSequence = MixEnergetik.UI.SequencerView.at(this.currentSequenceNumber);
                MixEnergetik.AudioManager.removeGroup(currentSequence.groupData);
                currentSequence.unuse();

                this.currentSequenceNumber = -1;

                this.status = "off";

                /* TODO ugly */
                if (MixEnergetik.UI.MapView)
                {
                    MixEnergetik.UI.MapView.enable();
                }

                MixEnergetik.SequencerManager.event.trigger("sequencestop");

                SequencerManager_sequenceStopped(currentSequence);

                MixEnergetik.UI.SequencerView.unuse();

                MixEnergetik.SequencerManager.event.trigger("stop");
            }
        },

        /**
         *  @function setVolume
         *      The the volume for the whole sequencer.
         *
         *  @param {float} volume
         *      Volume value between 0 and 1. 1 equals 100% and 0 is mute.
         *      The sound cannot go beyond 0 and 1.
         *      Nothing happens if volume is not valid (i.e. null, undefined or
         *      not a number).
         */
        setVolume:
        {
            configurable: true,
            enumerable: true,
            writable: false,

            value: function(volume)
            {
                if ((volume === null) || (volume === undefined)
                    || (typeof volume !== "number"))
                {
                    return;
                }

                let boundVolume = 1.0;

                if (volume < 0.0)
                {
                    boundVolume = 0.0;
                }
                else if (volume > 1.0)
                {
                    boundVolume = 1.0;
                }
                else
                {
                    boundVolume = volume;
                }

                this.volume = boundVolume;

                if ((this.status !== "off") && !(this.isEmpty()))
                {
                    let currentSequence = MixEnergetik.UI.SequencerView.at(this.currentSequenceNumber);

                    /* change sound volume if this sound is playing */
                    MixEnergetik.AudioManager.setGroupVolume(currentSequence.volume * this.volume);
                }
            }
        },

        /**
         *  @function bounce
         *      Let the magic happen.
         *      Take what's inside the sequencer and bounce it together in a music.
         */
        bounce:
        {
            configurable: false,
            enumerable: true,
            writable: false,

            value: function()
            {
                if(this.isEmpty())
                {
                    return;
                }

                let allSounds = [];

                for (let i = 0; i < MixEnergetik.UI.SequencerView.length; i++)
                {
                    let sequence = MixEnergetik.UI.SequencerView.at(i);

                    allSounds.push(sequence);
                }

                if (MixEnergetik.SequencerManager.isActive())
                {
                    MixEnergetik.SequencerManager.deactivate();
                }
                if (MixEnergetik.GroupManager.isActive())
                {
                    MixEnergetik.GroupManager.deactivate();
                }

                if (MixEnergetik.VideoManager)
                {
                    MixEnergetik.UI.VideoView.videoElement.currentTime = 0;
                    MixEnergetik.UI.VideoView.videoElement.play();
                }

                MixEnergetik.AudioManager.bounce(allSounds);
            }
        }
    });

    /**
     *  @property {MixEnergetik.Utilitary.Event} event
     *      Event system for the sequencer. Available events are:
     *      "sequenceplay" when a sequence just started playing
     *      "sequenceend" when a sequence naturally finished playing
     *      "sequencestop" when a sequence was stopped during playing
     *      "start" when the sequencer is activated
     *      "end" when the sequencer has naturally reached its end
     *      "stop" when the sequencer was deactivated
     */
    SequencerManager.event = new MixEnergetik.Utilitary.Event();

    /**
     *  @function pause
     *      Used to pause the sequencer, without deactivating it.
     *      Typically when solo mode is on.
     */
    SequencerManager.pause = function pause()
    {
        const SeqM = MixEnergetik.SequencerManager;
        const AudioM = MixEnergetik.AudioManager;

        if (MixEnergetik.SequencerManager.status !== "on")
        {
            return;
        }

        let currentSequence = SeqM.getCurrent();
        AudioM.removeGroup(currentSequence.groupData);
        currentSequence.unuse();

        SeqM.status = "paused";
    };

    /**
     *  @function resume
     *      Used to resume the sequencer from pause.
     *      Typically when solo mode is on.
     *
     *  @param {number} offset
     *      Time at which the playing sequence should restart.
     */
    SequencerManager.resume = function resume(offset)
    {
        const SeqM = MixEnergetik.SequencerManager;
        const AudioM = MixEnergetik.AudioManager;

        if (MixEnergetik.SequencerManager.status !== "paused")
        {
            return;
        }

        let currentSequence = SeqM.getCurrent();
        AudioM.addGroup(currentSequence.groupData);
        currentSequence.use();

        SeqM.status = "on";
    };

    /**
     *  @function onAudioEnd
     *      Callback on AudioManager when a group's play has ended
     */
    SequencerManager_onAudioEnd = function onAudioEnd()
    {
        const SeqM = MixEnergetik.SequencerManager;
        const AudioM = MixEnergetik.AudioManager;

        if (SeqM.isActive())
        {
            if (MixEnergetik.SoloManager.soloItem !== null)
            {
                /* If solo mode is on, just step to next sequence but do nothing
                 * more */
                SeqM.step();
            }
            else
            {
                /* at first remove all */
                AudioM.removeGroup(SeqM.getCurrent().groupData);

                SeqM.step();

                /* get all sounds from SeqM for next bar to play */
                AudioM.addGroup(SeqM.getCurrent().groupData);
            }

            return false;
        }
    };

    /**
     *  @function onGroupPlay
     *      Callback when a group has started playing.
     */
    SequencerManager_onGroupPlay = function onGroupPlay()
    {
        const groupItem = MixEnergetik.GroupManager.getActive();
        const SeqV = MixEnergetik.UI.SequencerView;
        const sequenceIndex = SeqV.sequences.indexOf(groupItem);

        if (sequenceIndex > -1)
        {
            MixEnergetik.SequencerManager.lastSequenceNumber = sequenceIndex;
        }
    };

    /**
     *  @function initialize
     *      Initialize the sequencer manager.
     */
    SequencerManager.initialize = function initialize()
    {
        MixEnergetik.AudioManager.event.on("end", SequencerManager_onAudioEnd);
        MixEnergetik.GroupManager.event.on("play", SequencerManager_onGroupPlay);
    };

    return SequencerManager;
})();
