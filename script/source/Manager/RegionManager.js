/*
 *  File:   RegionManager.js
 *  Author: Marcelin Lazzarotto
 *  Date:   2018-02-27
 */

"use strict";

/**
 *  @namespace MixEnergetik
 *      The project namespace, to avoid polluting global state.
 */
var MixEnergetik = MixEnergetik || {};

/**
 *  @module RegionManager
 *      Deals with region, and sample contained inside.
 *      Stores all regions sample per region per element per level.
 *      Also keeps an information about current region levels ("current" as
 *      "levels when the user launched the app" (TODO when the users changes
 *      levels with a time slider ?)).
 */
MixEnergetik.RegionManager = (function()
{
    let RegionManager = {};

    RegionManager.regionIds =
    [
        "Hauts-de-France",
        "Normandie",
        "Bretagne",
        "PaysDeLaLoire",
        "Nouvelle-Aquitaine",
        "Ile-de-France",
        "GrandEst",
        "Bourgogne-Franche-Comte",
        "Centre-ValDeLoire",
        "Auvergne-Rhone-Alpes",
        "Occitanie",
        // "Provence-Alpes-CoteDAzur",
        // "Corse"];
        "Provence-Alpes-CoteDAzur"
    ];

    /**
     *  @property {Object} elements
     *      Map storing a description of elements in use with regions.
     *      Contains samples' directory name (on the server), power levels and,
     *      once initialized, samples associated to power levels.
     *  @note
     *      Only the lower power limit is stored (call this the threshold)
     */
    RegionManager.elements =
    {
        air:
        {
            association: "eolian",
            directory: "air",
            scale:
            {
                0:
                {
                    requiredPower:   20, /* Modified compared to éco2mix levels */
                    soundIndex: null
                },
                1:
                {
                    requiredPower:   40, /* Modified compared to éco2mix levels */
                    soundIndex: null
                },
                2:
                {
                    requiredPower:  100,
                    soundIndex: null
                },
                3:
                {
                    requiredPower:  300,
                    soundIndex: null
                },
                4:
                {
                    requiredPower:  500,
                    soundIndex: null
                },
                5:
                {
                    requiredPower:  700,
                    soundIndex: null
                }
            },
            minLevel: 0,
            maxLevel: 5
        },
        earth:
        {
            association: "nuclear",
            directory: "nuc",
            scale:
            {
                2:
                {
                    requiredPower:  500,
                    soundIndex: null
                },
                3:
                {
                    requiredPower: 4000,
                    soundIndex: null
                },
                4:
                {
                    requiredPower: 6000,
                    soundIndex: null
                },
                5:
                {
                    requiredPower: 8000,
                    soundIndex: null
                }
            },
            minLevel: 2,
            maxLevel: 5
        },
        fire:
        {
            association: "solar",
            directory: "feu",
            scale:
            {
                0:
                {
                    requiredPower:   20, /* Modified compared to éco2mix levels */
                    soundIndex: null
                },
                1:
                {
                    requiredPower:   40, /* Modified compared to éco2mix levels */
                    soundIndex: null
                },
                2:
                {
                    requiredPower:  100,
                    soundIndex: null
                },
                3:
                {
                    requiredPower:  300,
                    soundIndex: null
                },
                4:
                {
                    requiredPower:  500,
                    soundIndex: null
                },
                5:
                {
                    requiredPower:  700,
                    soundIndex: null
                }
            },
            minLevel: 0,
            maxLevel: 5
        },
        water:
        {
            association: "hydrolic",
            directory: "eau",
            scale:
            {
                0:
                {
                    requiredPower:   20, /* Modified compared to éco2mix levels */
                    soundIndex: null
                },
                1:
                {
                    requiredPower:   40, /* Modified compared to éco2mix levels */
                    soundIndex: null
                },
                2:
                {
                    requiredPower:  100,
                    soundIndex: null
                },
                3:
                {
                    requiredPower:  500,
                    soundIndex: null
                },
                4:
                {
                    requiredPower: 1500,
                    soundIndex: null
                },
                5:
                {
                    requiredPower: 3000,
                    soundIndex: null
                },
                6:
                {
                    requiredPower: 4500,  /* Modified compared to éco2mix levels */
                    soundIndex: null
                }
            },
            minLevel: 0,
            maxLevel: 6
        // },
        }
        // bioenergy:
        // {
        //     directory: "bio",
        //     powerLevels:
        //     {
        //         2:  40,
        //         3:  100,
        //         4:  180
        //     }
        // }
    };

    /**
     *  @property {object} energetic
     *      Map of energetic power level and its sample. Kinda same as elements.
     */
    RegionManager.energetic =
    {
        scale:
        {
            1:
            {
                requiredPower:     0,
                paths:
                {
                },
                soundIndices:
                {
                }
            },
            2:
            {
                requiredPower:  1000,
                paths:
                {
                },
                soundIndices:
                {
                }
            },
            3:
            {
                requiredPower:  2000,
                paths:
                {
                },
                soundIndices:
                {
                }
            },
            4:
            {
                requiredPower:  3000,
                paths:
                {
                },
                soundIndices:
                {
                }
            },
            5:
            {
                requiredPower:  4000,
                paths:
                {
                },
                soundIndices:
                {
                }
            },
            6:
            {
                requiredPower:  5000,
                paths:
                {
                },
                soundIndices:
                {
                }
            },
            7:
            {
                requiredPower:  6000,
                paths:
                {
                },
                soundIndices:
                {
                }
            },
            8:
            {
                requiredPower:  7000,
                paths:
                {
                },
                soundIndices:
                {
                }
            },
            9:
            {
                requiredPower:  8000,
                paths:
                {
                },
                soundIndices:
                {
                }
            },
            10:
            {
                requiredPower:  9000,
                paths:
                {
                },
                soundIndices:
                {
                }
            },
            11:
            {
                requiredPower: 10000,
                paths:
                {
                },
                soundIndices:
                {
                }
            },
            12:
            {
                requiredPower: 11000,
                paths:
                {
                },
                soundIndices:
                {
                }
            },
            13:
            {
                requiredPower: 12000,
                paths:
                {
                },
                soundIndices:
                {
                }
            },
            14:
            {
                requiredPower: 13000,
                paths:
                {
                },
                soundIndices:
                {
                }
            },
            15:
            {
                requiredPower: 14000,
                paths:
                {
                },
                soundIndices:
                {
                }
            },
            16:
            {
                requiredPower: 15000,
                paths:
                {
                },
                soundIndices:
                {
                }
            },
            17:
            {
                requiredPower: 16000,
                paths:
                {
                },
                soundIndices:
                {
                }
            }
        },
        maxLevel: 17
    };

    /**
     *  @property {object} trade
     *      Map of trade power level and its sample. Kinda same as elements and
     *      energetic.
     */
    RegionManager.trade =
    {
        directory: "trade",
        scale:
        {
            0:
            {
                requiredPower:     0,
                soundIndices:
                {
                }
            },
            1:
            {
                requiredPower:     10,
                soundIndices:
                {
                }
            },
            2:
            {
                requiredPower:  1000,
                soundIndices:
                {
                }
            },
            3:
            {
                requiredPower:  2500,
                soundIndices:
                {
                }
            },
            4:
            {
                requiredPower:  5000,
                soundIndices:
                {
                }
            },
            5:
            {
                requiredPower:  7500,
                soundIndices:
                {
                }
            },
            6:
            {
                requiredPower:  8500,
                soundIndices:
                {
                }
            }
        },
        maxLevel: 6
    };

    /**
     *  @property {MixEnergetik.RegionData objectmap}
     */
    RegionManager.regions = {};

    /**
     *  @function initialize
     *      Add samples to region.
     */
    RegionManager.initialize = function initialize()
    {
        if (MixEnergetik.UI.EnergeticSelectionView)
        {
            RegionManager_retrieveEnergeticPaths();
        }
        else /* if (MixEnergetik.UI.GroupDataView) */
        {
            RegionManager_retrieveElementPaths();
        }
    };

    RegionManager.levelFromScale = function levelFromScale(power, scale)
    {
        let powerLevel = -1;

        for (let level in scale)
        {
            if ((scale[level].requiredPower === undefined)
                || (scale[level].requiredPower === null))
            {
                continue;
            }

            if (scale[level].requiredPower <= power)
            {
                powerLevel = level;
            }
            else
            {
                break;
            }
        }

        return +powerLevel;
    };

    /**
     *  @function updateAllSamples
     *      Updates all samples inside all regions.
     *
     *  @param {string} tradeAction
     *      Action for the trade samples. Values are:
     *      "TRADE_RESET" (default) where import samples are reset;
     *      "TRADE_UNCHANGED" where import samples are not modified;
     */
    RegionManager.updateAllSamples = function updateAllSamples(tradeAction)
    {
        tradeAction = tradeAction || "TRADE_RESET";

        if (MixEnergetik.TradeManager && (tradeAction === "TRADE_RESET"))
        {
            MixEnergetik.TradeManager.clear();
        }

        for (let i = 0; i < MixEnergetik.RegionManager.regionIds.length; i++)
        {
            let regionId = MixEnergetik.RegionManager.regionIds[i];

            RegionManager_updateSamples(regionId, tradeAction);
        }

        if (MixEnergetik.UI.TradeView && (tradeAction === "TRADE_RESET"))
        {
            MixEnergetik.UI.TradeView.resetImport();
        }
    };

    /**
     *  @function updateSamples
     *      Updates the samples chosen for a region inside the map, so that it
     *      corresponds to the power levels.
     *
     *  @param {string} regionId
     *      The region to update.
     *  @param {string} tradeAction
     *      Action for the trade samples. Values are:
     *      "TRADE_RESET" (default) where import samples are reset;
     *      "TRADE_UNCHANGED" where import samples are not modified;
     */
    let RegionManager_updateSamples = function updateSamples(regionId,
        tradeAction)
    {
        MixEnergetik.Utilitary.Check.native(regionId, "string",
            "MixEnergetik.RegionManager.updateSamples()", "regionId");

        const SndBldr = MixEnergetik.Utilitary.SoundBuilder;
        const AudioM = MixEnergetik.AudioManager;

        tradeAction = tradeAction || "TRADE_RESET";

        /* Clarity */
        let regionData = MixEnergetik.RegionManager.regions[regionId];
        let regionItem = MixEnergetik.UI.MapView.regions[regionId];

        /* Clean first. */
        MixEnergetik.UI.MapView.unmuteBaseSound(regionId);
        regionItem.clear(tradeAction);
        regionItem.specialization.regionBaseSound = [];

        /* Send corresponding production of consumption samples to the group */
        if (MixEnergetik.UI.EnergeticSelectionView)
        {
            let power = 0;
            let descriptor = MixEnergetik.RegionManager.energetic;
            let status = MixEnergetik.UI.EnergeticSelectionView.status;

            if (status === "production")
            {
                power = regionData.getProductionSum();
            }
            else /* if (status === "consumption") */
            {
                power = regionData.consumption;
            }

            let level = RegionManager.levelFromScale(power, descriptor.scale);

            level = (level === -1) ? 1 : level;

            let style = MixEnergetik.UI.EnergeticSelectionView.style;

            /*
                "Hey Mars

                Sur conso-prod il y a 16 niveaux.... sauf pour "ROCK on !" avec un 17 eme

                Pourrais tu mettre ce 17 eme niveau. POur les 2 restant ( Piano et INdus ) fais jouer le niveau 16 avec 17 ....
                oki ?
                :)
                Bizatoa
                C"
                Safety
            */
            while ((level > 1)
                // && (descriptor.scale[level])
                && !(descriptor.scale[level].paths[style]))
            {
                level--;
            }

            if (descriptor.scale[level].paths[style])
            {
                if (!descriptor.scale[level].soundIndices[style])
                {
                    /* Delayed download */
                    let fullPath = descriptor.scale[level].paths[style];
                    let soundWrapper = SndBldr.createSoundWrapper(fullPath);
                    let soundIndex = AudioM.provideSound(soundWrapper);
                    descriptor.scale[level].soundIndices[style] = soundIndex;
                }

                let soundIndex = descriptor.scale[level].soundIndices[style];

                let soundData = new MixEnergetik.SoundData(soundIndex);
                regionItem.addSound(soundData);
                regionItem.specialization.regionBaseSound.push(soundIndex);
            }

            /* Deals with previous import samples too */
            if (status === "production")
            {
                /* Reload eventual imported samples. */
                for (let soundIndex in regionItem.importData.soundDatas)
                {
                    const soundData = regionItem.importData.soundDatas[soundIndex];
                    regionItem.addSound(soundData);
                }
            }
        }
        else /* if (<elements>) */
        {
            /* Send corresponding samples to the region's own group of samples */
            for (let elementId in MixEnergetik.RegionManager.elements)
            {
                /* Renewable thing */
                const GrpDtV = MixEnergetik.UI.GroupDataView;
                if (GrpDtV
                    && (GrpDtV.mode === "MODE_RENEWABLE")
                    && (elementId === "earth"))
                {
                    continue;
                }

                let descriptor = MixEnergetik.RegionManager.elements[elementId];
                let power = regionData.production[descriptor.association];
                let level = RegionManager.levelFromScale(power, descriptor.scale);

                /* Nuclear might not be enough for min level */
                if ((level !== -1) && descriptor.scale[level])
                {
                    if ((!descriptor.scale[level].soundIndex)
                        && (!!descriptor.scale[level].path))
                    {
                        const path = descriptor.scale[level].path;
                        const soundWrapper = SndBldr.createSoundWrapper(path);
                        const soundIndex = AudioM.provideSound(soundWrapper);

                        descriptor.scale[level].soundIndex = soundIndex;
                    }

                    let soundIndex = descriptor.scale[level].soundIndex;

                    let soundData = new MixEnergetik.SoundData(soundIndex);
                    regionItem.addSound(soundData);
                    regionItem.specialization.regionBaseSound.push(soundIndex);
                }
            }
        }
    }; /* updateSamples */

    /**
     *  @function retrieveEnergeticPaths
     *      Retrieve pathes for samples for the energiziks/base/ page.
     */
    let RegionManager_retrieveEnergeticPaths = function retrieveEnergeticPaths()
    {
        let successHandler = function successHandler(fileTree)
        {
            const MasterM = MixEnergetik.MasterManager;
            const RegionM = MixEnergetik.RegionManager;
            const BASE_PATH = "../resources/samples/base";
            const TRUE_BASE_PATH = MasterM.PROJECT_ROOT
                + "/resources/samples/base";

            if ((fileTree.name !== BASE_PATH)
                || (fileTree.type !== "directory"))
            {
                throw new Error("[RegionManager_retrieveEnergeticPaths()] Can't"
                    + " use MasterManager.retrievePaths() returned data.");
            }

            for (let i = 0; i < fileTree.content.length; i++)
            {
                let styleDir = fileTree.content[i];

                if (styleDir.type !== "directory")
                {
                    console.warn("[RegionManager_retrieveEnergeticPaths()]"
                        + " Expected style directory, found file.");

                    continue;
                }

                let styleDirPath = (TRUE_BASE_PATH + "/" + styleDir.name);

                for (let j = 0; j < styleDir.content.length; j++)
                {
                    let fileObject = styleDir.content[j];

                    if (fileObject.type === "directory")
                    {
                        if (fileObject.name !== RegionM.trade.directory)
                        {
                            console.warn("[RegionManager_retrieveEnergeticPaths()]"
                                + " Expected trade directory or sample file,"
                                + " found unexpected directory.");

                            continue;
                        }

                        const tradeDirPath = styleDirPath + "/" + fileObject.name;

                        RegionManager_retrieveTradePaths(fileObject,
                            tradeDirPath, styleDir.name);
                    }
                    else /* if (fileObject.type === "file") */
                    {
                        /* Actual file */
                        let fullPath = styleDirPath + "/" + fileObject.name;
                        let powerLevel = +(fileObject.name.split(".")[0]);

                        if (!RegionM.energetic.scale[powerLevel])
                        {
                            RegionM.energetic.scale[powerLevel] = {};
                            RegionM.energetic.scale[powerLevel].paths = {};
                            RegionM.energetic.scale[powerLevel].soundIndices = {};
                        }

                        /* For delayed update */
                        RegionM.energetic.scale[powerLevel].paths[styleDir.name] = fullPath;
                    }
                }

                /* Also add style to style selection */
                if (MixEnergetik.UI.EnergeticSelectionView) /* Safety */
                {
                    MixEnergetik.UI.EnergeticSelectionView.addStyle(
                        styleDir.name);
                }
            }

            MixEnergetik.DateManager.fetchAndUpdateRegions(
                MixEnergetik.DateManager.currentTime);
        }; /* successHandler() */

        let errorHandler = function errorHandler(status, errorString)
        {
            throw new Error("[RegionManager_retrieveEnergeticPaths()]"
                + " Couldn't retrieve sample paths\n"
                + errorString + " with status " + status);
        };

        MixEnergetik.MasterManager.retrievePaths("samples", "base",
            successHandler, errorHandler);
    }; /* RegionManager_retrieveEnergeticPaths */

    let RegionManager_retrieveElementPaths = function retrieveElementPaths()
    {
        let successHandler = function successHandler(fileTree)
        {
            const RegionM = MixEnergetik.RegionManager;

            const BASE_PATH = "../resources/samples/extraction";
            const TRUE_BASE_PATH = MixEnergetik.MasterManager.PROJECT_ROOT
                + "/resources/samples/extraction";

            let __getElement = function __getElement(dirName)
            {
                for (let element in RegionM.elements)
                {
                    if (RegionM.elements[element].directory === dirName)
                    {
                        return element;
                    }
                };

                return "";
            };

            if ((fileTree.name !== BASE_PATH)
                || (fileTree.type !== "directory"))
            {
                throw new Error("[RegionManager_retrieveElementPaths()] Can't"
                    + " use MasterManager.retrievePaths() returned data.");
            }

            for (let i = 0; i < fileTree.content.length; i++)
            {
                let baseDir = fileTree.content[i];

                if (baseDir.type !== "directory")
                {
                    console.warn("[RegionManager_retrieveElementPaths()]"
                        + " Expected trade or element directory, found file"
                        + " instead.");

                    continue;
                }

                let baseDirPath = (TRUE_BASE_PATH + "/" + baseDir.name);

                if (baseDir.name === RegionM.trade.directory)
                {
                    RegionManager_retrieveTradePaths(baseDir, baseDirPath,
                        "element");
                }
                else
                {
                    /* Should be element */
                    let elemId = __getElement(baseDir.name);

                    if (elemId === "")
                    {
                        console.warn("[RegionManager_retrieveElementPaths()]"
                            + " Found an unexpected non-element, non-trade"
                            + " directory.");

                        continue;
                    }

                    for (let j = 0; j < baseDir.content.length; j++)
                    {
                        let fileObject = baseDir.content[j];

                        if (fileObject.type !== "file")
                        {
                            console.warn("[RegionManager_retrieveElementPaths()]"
                                + " Expected sample file, found directory");

                            continue;
                        }

                        let fullPath = baseDirPath + "/" + fileObject.name;
                        let powerLevel = +(fileObject.name.split(".")[0]);

                        if (!RegionM.elements[elemId].scale[powerLevel])
                        {
                            RegionM.elements[elemId].scale[powerLevel] = {};
                        }

                        RegionM.elements[elemId].scale[powerLevel].path = fullPath;
                    }
                }
            }

            MixEnergetik.DateManager.fetchAndUpdateRegions(MixEnergetik.DateManager.currentTime);

            if (MixEnergetik.UI.GroupDataView)
            {
                MixEnergetik.UI.GroupDataView.prepareSampleItems();
            }
        }; /* successHandler() */

        let errorHandler = function errorHandler(status, errorString)
        {
            throw new Error("[MixEnergetik.MasterManager.initializeRegions()]"
                + " Couldn't retrieve sample paths\n"
                + errorString + " with status " + status);
        };

        MixEnergetik.MasterManager.retrievePaths("samples", "extraction",
            successHandler, errorHandler);
    }; /* RegionManager_retrieveElementPaths */

    /**
     *  @function retrieveTradePaths
     *      Retrieve the path for the given trade directory.
     *
     *  @param {object} tradeDir
     *      The directory object containing the trade subdirs.
     *  @param {string} dirPath
     *      The full directory path.
     *  @param {string} style
     *      The style name, to know where to store the samples.
     */
    let RegionManager_retrieveTradePaths = function retrieveTradePaths(tradeDir,
        dirPath, style)
    {
        const TradeM = MixEnergetik.TradeManager;
        if (!TradeM)
        {
            /* Safety */
            return;
        }

        /* Init */
        TradeM.tradeItems = TradeM.tradeItems || {};
        TradeM.tradeItems[style] = TradeM.tradeItems[style] || {};
        TradeM.tradeItems[style].import =
            TradeM.tradeItems[style].import || [];
        TradeM.tradeItems[style].export =
            TradeM.tradeItems[style].export || {};
        TradeM.tradeItems[style].importPaths =
            TradeM.tradeItems[style].importPaths || [];
        TradeM.tradeItems[style].exportPaths =
            TradeM.tradeItems[style].exportPaths || {};

        let importDir = null;
        let exportDir = null;

        for (let l = 0; l < tradeDir.content.length; l++)
        {
            let dir = tradeDir.content[l];
            if (dir.name === "import")
            {
                importDir = dir;
            }
            else if (dir.name === "export")
            {
                exportDir = dir;
            }
            else
            {
                console.warn("[RegionManager_retrieveTradePaths()] Found a "
                    + " file object under trade that is not a specified trade "
                    + " element");

                continue;
            }
        }

        importDir = importDir || {"content": []};
        exportDir = exportDir || {"content": []};

        /* Imports */
        for (let l = 0; l < importDir.content.length; l++)
        {
            const fileObject = importDir.content[l];

            if (fileObject.type !== "file")
            {
                console.warn("[RegionManager_retrieveTradePaths()] Unexpected"
                    + " directory, I don't know what to do with that");

                continue;
            }

            const fullPath = dirPath + "/import/" + fileObject.name;

            TradeM.tradeItems[style].importPaths.push(fullPath);
        }

        /* Exports */
        for (let l = 0; l < exportDir.content.length; l++)
        {
            let regionDir = exportDir.content[l];

            if (regionDir.type !== "directory")
            {
                console.warn("[RegionManager_retrieveTradePaths()] Unexpected"
                    + " file, I don't know what to do with that.");
                continue;
            }

            const regionId = regionDir.name;
            const regionPath = dirPath + "/export/" + regionId;

            TradeM.tradeItems[style].export[regionId] =
                TradeM.tradeItems[style].export[regionId] || [];
            TradeM.tradeItems[style].exportPaths[regionId] =
                TradeM.tradeItems[style].exportPaths[regionId] || [];

            for (let m = 0; m < regionDir.content.length; m++)
            {
                const fileObject = regionDir.content[m];

                if (fileObject.type !== "file")
                {
                    console.warn("Unexpected directory, I don't know"
                        + " what to do with that");
                    continue;
                }

                const fullPath = regionPath + "/" + fileObject.name;

                TradeM.tradeItems[style].exportPaths[regionId].push(fullPath);
            }
        }
    }; /* RegionManager_retrieveTradePaths */

    /* Builds the map of regions which contain a map of element which contain a
     * map of levels which correspond to a soundWrapperId */
    (function initialize()
    {
        for (let i = 0; i < RegionManager.regionIds.length; i++)
        {
            let regionId = RegionManager.regionIds[i];

            RegionManager.regions[regionId] = new MixEnergetik.RegionData();
            RegionManager.regions[regionId].id = regionId;
        }
    })();

    return RegionManager;
})();
