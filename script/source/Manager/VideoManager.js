/*
 *  File:   VideoManager.js
 *  Author: Marcelin Lazzarotto
 *  Date:   2018-04-12
 */

"use strict";

/**
 *  @namespace MixEnergetik
 *      The project namespace, to avoid polluting global state.
 */
var MixEnergetik = MixEnergetik || {};

/**
 *  @module VideoManager
 *      The manager dealing with the video.
 */
MixEnergetik.VideoManager = (function()
{
    let VideoManager = {};

    VideoManager.initialize = function initialize()
    {
        if (!MixEnergetik.VieMer)
        {
            MixEnergetik.GroupManager.event.on("play", onGroupPlay);
            MixEnergetik.GroupManager.event.on("end", onGroupEnd);
            MixEnergetik.GroupManager.event.on("stop", onGroupStop);

            MixEnergetik.SequencerManager.event.on("start", onSequenceStart);
            MixEnergetik.SequencerManager.event.on("end", onSequenceEnd);
            MixEnergetik.SequencerManager.event.on("stop", onSequenceStop);

            /* In case we are on MapView, videos might be longer so loop */
            if (MixEnergetik.UI.MapView)
            {
                MixEnergetik.UI.VideoView.videoElement.loop = true;
            }
        }
    }; /* initialize() */

    let onGroupPlay = function()
    {
        const groupItem = MixEnergetik.GroupManager.getActive();
        const sequenceIndex = MixEnergetik.UI.SequencerView.sequences.indexOf(groupItem);
        const VideoV = MixEnergetik.UI.VideoView;
        const PrePanV = MixEnergetik.UI.PresetPanelView;
        const sampleDuration = MixEnergetik.Utilitary.Configuration.Audio.sampleDuration;
        let regionId = "";

        if (MixEnergetik.UI.MapView)
        {
            regionId = MixEnergetik.UI.MapView.getRegionItemId(groupItem);
        }

        if (/* MixEnergetik.UI.MapView && */ regionId !== "")
        {
            const BASE_URL = MixEnergetik.MasterManager.PROJECT_ROOT
                + "/resources/videos"
            let basePlusURL = "";

            if (MixEnergetik.UI.EnergeticSelectionView)
            {
                basePlusURL = BASE_URL + "/base";
            }
            else
            {
                basePlusURL = BASE_URL + "/extraction";
            }

            const videoURL = basePlusURL + "/" + regionId  + ".mp4";

            if (VideoV.getVideoURL() !== videoURL)
            {
                VideoV.setVideoURL(videoURL, regionId);
            }

            if (VideoV.videoElement.ended)
            {
                const currentTime = MixEnergetik.AudioManager.getCurrentTime();

                VideoV.videoElement.currentTime = currentTime;
            }

            VideoV.videoElement.play();

            VideoV.show();
        }
        else if (sequenceIndex > -1)
        {
            /* Group is actually a sequence, play video at same time. */
            VideoV.videoElement.currentTime = sequenceIndex * sampleDuration;
            VideoV.videoElement.play();
        }
        else if (PrePanV && (PrePanV.presets.indexOf(groupItem) > -1))
        {
            const lastSequenceIndex = MixEnergetik.SequencerManager.lastSequenceNumber;

            VideoV.videoElement.currentTime = lastSequenceIndex * sampleDuration;
            VideoV.videoElement.play();
        }
    };

    let onGroupEnd = function()
    {
        /* Either a sequence or a region, stop video */

        if (MixEnergetik.UI.MapView)
        {
            MixEnergetik.UI.VideoView.hide();
        }
        else
        {
            MixEnergetik.UI.VideoView.videoElement.pause();
        }
    };

    let onGroupStop = function()
    {
        /* If group is actually a sequence o region, stop video at same time.
         * In our case, whatever happens stop the video since we stopped all
         * groups. */
        MixEnergetik.UI.VideoView.videoElement.pause();

        if (MixEnergetik.UI.MapView)
        {
            MixEnergetik.UI.VideoView.hide();
        }
    };

    let onSequenceStart = function onSequenceStart()
    {
        if (MixEnergetik.UI.Wander)
        {
            MixEnergetik.UI.VideoView.videoElement.currentTime = 0;
            MixEnergetik.UI.VideoView.videoElement.play();
        }
    };

    let onSequenceEnd = function onSequenceEnd()
    {
        if (MixEnergetik.UI.Wander)
        {
            MixEnergetik.UI.VideoView.videoElement.pause();
        }
    };

    let onSequenceStop = function onSequenceStop()
    {
        if (MixEnergetik.UI.Wander)
        {
            MixEnergetik.UI.VideoView.videoElement.pause();
        }
    };

    return VideoManager;
})();
