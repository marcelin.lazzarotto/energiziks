/*
 *  File:   PersistenceManager.js
 *  Author: Marcelin Lazzarotto
 *  Date:   2018-06-12
 */

"use strict";

/**
 *  @namespace MixEnergetik
 *      The project namespace, to avoid polluting global state.
 */
var MixEnergetik = MixEnergetik || {};

/**
 *  @module PersistenceManager
 *      The manager dealing with saves and load.
 */
MixEnergetik.PersistenceManager = (function()
{
    let PersistenceManager = {};

    let PseudoSound = function PseudoSound()
    {
        this.url = "";
        this.volume = 1;
    };

    PseudoSound.prototype.toSoundData = function()
    {
        let soundIndex = MixEnergetik.AudioManager.retrieveSoundIndexByUrl(this.url);

        if (soundIndex === -1)
        {
            // TODO
            console.log(this.url);
            console.error("A sound was removed");
            return null;
        }

        let soundData = new MixEnergetik.SoundData(soundIndex);
        soundData.volume = this.volume;

        return soundData;
    };

    PseudoSound.prototype.fromSoundData = function(soundData)
    {
        this.url = MixEnergetik.AudioManager.soundWrappers[soundData.soundIndex].path;
        this.volume = soundData.volume;
    };

    PseudoSound.prototype.fromObject = function(lostPseudoSound)
    {
        this.url = PersistenceManager_cleanUrl(lostPseudoSound.url);
        this.volume = lostPseudoSound.volume;
    }

    let PseudoGroup = function PseudoGroup()
    {
        this.volume = 1;
        this.pseudoSounds = [];
    };

    PseudoGroup.prototype.toGroupData = function()
    {
        let groupData = new MixEnergetik.GroupData();
        groupData.volume = this.volume;

        for (let i = 0; i < this.pseudoSounds.length; i++)
        {
            let soundData = this.pseudoSounds[i].toSoundData();
            if (soundData !== null)
            {
                groupData.addSoundData(soundData);
            }
        }

        return groupData;
    };

    PseudoGroup.prototype.fromGroupData = function(groupData)
    {
        this.volume = groupData.volume;

        for (let i = 0; i < groupData.soundDatas.length; i++)
        {
            let pseudoSound = new PseudoSound();

            pseudoSound.fromSoundData(groupData.soundDatas[i]);

            this.pseudoSounds.push(pseudoSound);
        }
    };

    PseudoGroup.prototype.fromObject = function(lostPseudoGroup)
    {
        this.volume = lostPseudoGroup.volume;

        for (let i = 0; i < lostPseudoGroup.pseudoSounds.length; i++)
        {
            let pseudoSound = new PseudoSound();

            pseudoSound.fromObject(lostPseudoGroup.pseudoSounds[i]);

            this.pseudoSounds.push(pseudoSound);
        }
    };

    /**
     *  @function cleanUrl
     *      The project's changed name, and since the architecture was
     *      bad to begin with, all save files may contain traces of the
     *      previous url.
     *      This allows for correction on the go, before breaking things.
     *
     *  @param {string} url
     *      The url to clean
     *
     *  @returns {string}
     *      A cleaned version of the given url.
     */
    let PersistenceManager_cleanUrl = function(url)
    {
        // without the g option, it's only the first occurence which is perfect
        const OLD_NAME = /mix-energetik/;

        return url.replace(OLD_NAME, MixEnergetik.MasterManager.PROJECT_NAME);
    };

    let PersistenceManager_fetchSaves = function fetchSaves()
    {
        let getDirName = function getDirName(dirObject)
        {
            for (let dirName in dirObject)
            {
                if (dirObject.hasOwnProperty(dirName))
                {
                    return dirName;
                }
            }

            return "";
        };

        let xhr = new XMLHttpRequest();
        let url = MixEnergetik.MasterManager.PROJECT_ROOT
            + "/server/retrieveLocation.php";
        xhr.open("POST", url, true);

        // Send the proper header information along with the request
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.responseType = "json";

        xhr.onreadystatechange = function()
        {
            if(xhr.readyState === XMLHttpRequest.DONE)
            {
                if (xhr.status === 200)
                {
                    let location = xhr.response || null;

                    if (location === null)
                    {
                        throw new Error(
                            "[MixEnergetik.UI.PersistenceManager_fetchSaves()]"
                            + " Debug:Something weird happened.");
                    }

                    let dirName = getDirName(location);

                    for (let i = 0; i < location[dirName].length; i++)
                    {
                        let saveName = location[dirName][i].split('.')[0]

                        MixEnergetik.UI.PersistenceView.addSave(saveName);
                    }
                }
                else
                {
                    throw new Error(
                        "[MixEnergetik.UI.PersistenceManager_fetchSaves()]"
                        + " Couldn't retrieve URLs");
                }
            }
        };

        xhr.send("type=saves");
    };

    /**
     *  @function serialize
     *      Saves the state of the program and return it as an object.
     *
     *  @param {string} name
     *      The name of the save.
     */
    let PersistenceManager_serialize = function serialize(saveName)
    {
        MixEnergetik.Utilitary.Check.native(saveName, "string",
            "MixEnergetik.PersistenceManager_serialize()", "saveName");

        let save = {};

        save.name = saveName;

        /* Video */
        {
            save.video = {};
            save.video.url = MixEnergetik.UI.VideoView.getVideoURL();
            save.video.name = MixEnergetik.UI.VideoView.getVideoName();
        }

        /* Presets */
        {
            save.presets = [];

            const presets = MixEnergetik.UI.PresetPanelView.presets;
            for (let i = 0; i < presets.length; i++)
            {
                let pseudoGroup = new PseudoGroup();

                pseudoGroup.fromGroupData(presets[i].groupData);

                save.presets.push(pseudoGroup);
            }
        }

        /* Sequencer */
        {
            save.sequencer = {};
            save.sequencer.volume = MixEnergetik.SequencerManager.volume;
            save.sequencer.sequences = [];

            const sequences = MixEnergetik.UI.SequencerView.sequences
            for (let i = 0; i < sequences.length; i++)
            {
                let pseudoGroup = new PseudoGroup();

                pseudoGroup.fromGroupData(sequences[i].groupData);

                save.sequencer.sequences.push(pseudoGroup);
            }
        }

        return JSON.stringify(save);
    };

    /**
     *  @function deserialize
     *      Parse the content of the given object, loading a new configuration.
     */
    let PersistenceManager_deserialize = function deserialize(save)
    {
        MixEnergetik.Utilitary.Check.native(save, "object",
            "MixEnergetik.PersistenceManager_deserialize", "save");

        /* Video */
        {
            let url = PersistenceManager_cleanUrl(save.video.url);
            MixEnergetik.UI.VideoView.setVideoURL(url,
                save.video.name);
        }

        /* Presets */
        /* Warning: thoses appeared after, so safety for first save versions
         * without presets */
        if (save.presets)
        {
            const PrePanV = MixEnergetik.UI.PresetPanelView;

            PrePanV.clear();
            let presets = PrePanV.presets;

            for (let i = 0; i < save.presets.length; i++)
            {
                let pseudoGroup = new PseudoGroup();

                pseudoGroup.fromObject(save.presets[i]);

                let groupData = pseudoGroup.toGroupData();

                presets[i].addGroup(groupData);
            }
        }

        /* Sequencer */
        {
            MixEnergetik.SequencerManager.setVolume(save.sequencer.volume);
            MixEnergetik.UI.SequencerView.zero();

            for (let i = 0; i < save.sequencer.sequences.length; i++)
            {
                MixEnergetik.UI.SequencerView.addSequence();

                let pseudoGroup = new PseudoGroup();

                pseudoGroup.fromObject(save.sequencer.sequences[i]);

                let groupData = pseudoGroup.toGroupData();

                MixEnergetik.UI.SequencerView.sequences[i].addGroup(groupData);
            }
        }
    };

    /**
     *  @property {MixEnergetik.Utilitary.Event} event
     *      The event manager linked with this manager. Events are:
     *      - "fetch", when the load function tries to retrieve a save from the
     *      server
     *      - "load", when the load function has returned.
     */
    PersistenceManager.event = new MixEnergetik.Utilitary.Event();

    PersistenceManager.initialize = function initialize()
    {
        PersistenceManager_fetchSaves();
    };

    PersistenceManager.save = function save(saveName)
    {
        MixEnergetik.Utilitary.Check.native(saveName, "string",
            "MixEnergetik.PersistenceManager.save()", "saveName");

        let save = PersistenceManager_serialize(saveName);
        let xhr = new XMLHttpRequest();
        let formData = new FormData();
        let url = MixEnergetik.MasterManager.PROJECT_ROOT
            + "/server/persistence.php";
        xhr.open("POST", url, true);

        // Send the proper header information along with the request
        // xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.responseType = "text";

        xhr.onreadystatechange = function()
        {
            if(xhr.readyState === XMLHttpRequest.DONE)
            {
                if (xhr.status === 200)
                {
                    let response = xhr.response || null;

                    if (response === null)
                    {
                        throw new Error(
                            "[MixEnergetik.PersistenceManager.save()]"
                            + "Debug:Something weird happened.");
                    }

                    switch (response)
                    {
                        case "PERSISTENCE_SAVE_SUCCESS":
                            MixEnergetik.UI.PersistenceView.addSave(saveName);

                            break;
                        case "PERSISTENCE_AUTHENTICATION_FAILURE":
                            /* TURBOMEH */
                            alert("Vous n'avez pas les droits pour cela.");
                        default:
                            /* TURBOMEH */
                            alert("Une erreur est advenue.");
                    }
                }
                else if (xhr.status === 401)
                {
                    let response = xhr.response || null;

                    if (response === null)
                    {
                        throw new Error(
                            "[MixEnergetik.PersistenceManager.save()]"
                            + "Debug:Something weird happened.");
                    }

                    if (response !== "PERSISTENCE_AUTHENTICATION_FAILURE")
                    {
                        console.log("?");
                    }

                    alert("Vous n'avez pas les droits pour cela.");
                }
                else
                {
                    throw new Error(
                        "[MixEnergetik.UI.PersistenceManager.save()]"
                        + " Couldn't resolve request");
                }
            }
        };

        formData.append("action", "save");
        formData.append("filename", saveName);
        formData.append("file", new Blob([save], {type : "application/json"}));

        xhr.send(formData);
    };

    /**
     *  @function load
     *      Fetch a save file from the server and load this save.
     *
     *  @param {string} saveName
     *      The name of the save to load.
     */
    PersistenceManager.load = function load(saveName)
    {
        MixEnergetik.Utilitary.Check.native(saveName, "string",
            "MixEnergetik.PersistenceManager.load()", "saveName");

        let xhr = new XMLHttpRequest();
        let url = MixEnergetik.MasterManager.PROJECT_ROOT
            + "/server/persistence.php";
        xhr.open("POST", url, true);

        // Send the proper header information along with the request
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.responseType = "text";

        xhr.onreadystatechange = function()
        {
            if(xhr.readyState === XMLHttpRequest.DONE)
            {
                if (xhr.status === 200)
                {
                    let response = xhr.response || null;

                    if (response === null)
                    {
                        throw new Error(
                            "[MixEnergetik.PersistenceManager.load()]"
                            + "Debug:Something weird happened.");
                    }

                    if (response === "PERSISTENCE_AUTHENTICATION_FAILURE")
                    {
                        /* TURBOMEH */
                        alert("Vous n'avez pas les droits pour cela.");
                    }
                    else
                    {
                        let save = JSON.parse(response);
                        PersistenceManager_deserialize(save);
                    }
                }
                else if (xhr.status === 401)
                {
                    let response = xhr.response || null;

                    if (response === null)
                    {
                        throw new Error(
                            "[MixEnergetik.PersistenceManager.load()]"
                            + "Debug:Something weird happened.");
                    }

                    if (response !== "PERSISTENCE_AUTHENTICATION_FAILURE")
                    {
                        console.log("?");
                    }

                    alert("Vous n'avez pas les droits pour cela.");
                }
                else
                {
                    throw new Error(
                        "[MixEnergetik.UI.PersistenceManager.load()]"
                        + " Couldn't resolve request");
                }

                MixEnergetik.PersistenceManager.event.trigger("load");
            }
        };

        MixEnergetik.PersistenceManager.event.trigger("fetch");

        xhr.send("action=load&filename=" + saveName);
    };

    /**
     *  @function remove
     *      Ask the server to remove the save of given name.
     *
     *  @param {string} saveName
     *      The name of the save to remove.
     */
    PersistenceManager.remove = function remove(saveName)
    {
        MixEnergetik.Utilitary.Check.native(saveName, "string",
            "MixEnergetik.PersistenceManager.load()", "saveName");

        let xhr = new XMLHttpRequest();
        let url = MixEnergetik.MasterManager.PROJECT_ROOT
            + "/server/persistence.php";
        xhr.open("POST", url, true);

        // Send the proper header information along with the request
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.responseType = "text";

        xhr.onreadystatechange = function()
        {
            if(xhr.readyState == XMLHttpRequest.DONE)
            {
                if (xhr.status == 200)
                {
                    let response = xhr.response || null;

                    if (response === null)
                    {
                        throw new Error(
                            "[MixEnergetik.UI.PersistenceManager.remove()]"
                            + "Debug:Something weird happened.");
                    }

                    switch (response)
                    {
                        case "PERSISTENCE_SAVE_REMOVAL_SUCCESS":
                            MixEnergetik.UI.PersistenceView.removeSave(saveName);

                            break;
                        case "PERSISTENCE_AUTHENTICATION_FAILURE":
                            /* TURBOMEH */
                            alert("Vous n'avez pas les droits pour cela.");
                        default:
                            /* TURBOMEH */
                            alert("Impossible de supprimer la sauvegarde, une"
                                + " erreur s'est produite.");
                    }
                }
                else if (xhr.status === 401)
                {
                    let response = xhr.response || null;

                    if (response === null)
                    {
                        throw new Error(
                            "[MixEnergetik.PersistenceManager.remove()]"
                            + "Debug:Something weird happened.");
                    }

                    if (response !== "PERSISTENCE_AUTHENTICATION_FAILURE")
                    {
                        console.log("?");
                    }

                    alert("Vous n'avez pas les droits pour cela.");
                }
                else
                {
                    throw new Error(
                        "[MixEnergetik.UI.PersistenceManager.remove()]"
                        + " Couldn't resolve request");
                }
            }
        };

        xhr.send("action=remove&filename=" + saveName);
    };

    /**
     *  @function loadDefaultMix
     *      Loads the default mix for the given video name.
     *
     *  @param {string} videoName
     *      The name of the video for which we need the default mix.
     */
    PersistenceManager.loadDefaultMix = function loadDefaultMix(videoName)
    {
        MixEnergetik.Utilitary.Check.native(videoName, "string",
            "MixEnergetik.PersistenceManager.load()", "videoName");

        const BASE_URL = "/energiziks/resources/examples";
        let xhr = new XMLHttpRequest();
        let url = BASE_URL + "/" + videoName + ".json";

        xhr.open("GET", url, true);
        // Send the proper header information along with the request
        // xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.responseType = "text";

        xhr.onreadystatechange = function()
        {
            if(xhr.readyState === XMLHttpRequest.DONE)
            {
                if (xhr.status === 200)
                {
                    let response = xhr.response || null;

                    if (response === null)
                    {
                        throw new Error(
                            "[MixEnergetik.UI.PersistenceManager.loadDefaultMix()]"
                            + "Debug:Something weird happened.");
                    }

                    if (response === "PERSISTENCE_AUTHENTICATION_FAILURE")
                    {
                        alert("Vous n'avez pas les droits pour cela.");
                    }
                    else
                    {
                        let save = JSON.parse(response);

                        PersistenceManager_deserialize(save);

                        /* Start the mix */
                        // MixEnergetik.SequencerManager.deactivate();
                        MixEnergetik.GroupManager.deactivate();
                        MixEnergetik.SequencerManager.activate();
                    }
                }
                else if (xhr.status === 401)
                {
                    let response = xhr.response || null;

                    if (response === null)
                    {
                        throw new Error(
                            "[MixEnergetik.PersistenceManager.loadDefaultMix()]"
                            + "Debug:Something weird happened.");
                    }

                    if (response !== "PERSISTENCE_AUTHENTICATION_FAILURE")
                    {
                        console.log("?");
                    }

                    alert("Vous n'avez pas les droits pour cela.");
                }
                else if (xhr.status === 404)
                {
                    alert("Aucun mix d'exemple n'a été trouvé pour cette vidéo.");
                }
                else
                {
                    throw new Error(
                        "[MixEnergetik.UI.PersistenceManager.loadDefaultMix()]"
                        + " Couldn't resolve request");
                }

                MixEnergetik.PersistenceManager.event.trigger("load");
            }
        };

        MixEnergetik.PersistenceManager.event.trigger("fetch");

        xhr.send();
    };

    return PersistenceManager;
})();
