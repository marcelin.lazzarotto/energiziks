/*
 *  File:   MasterManager.js
 *  Author: Marcelin Lazzarotto
 *  Date:   2018-01-29
 */

/**
 *  @namespace MixEnergetik
 *      The project namespace, to avoid polluting global state.
 */
var MixEnergetik = MixEnergetik || {};

/**
 *  @module MasterManager
 *      The kinda big interface between everything.
 */
MixEnergetik.MasterManager = (function()
{
    let MasterManager = {};

    MasterManager.PROJECT_NAME = "energiziks";
    MasterManager.PROJECT_ROOT = "/energiziks";

    MasterManager.initializeSilence = function initializeSilence()
    {
        let silencePath = MixEnergetik.MasterManager.PROJECT_ROOT
            + "/resources/default/Silence_3_504.ogg";
        let silenceWrapper = MixEnergetik.Utilitary.SoundBuilder.createSoundWrapper(silencePath);

        MixEnergetik.AudioManager.provideSilence(silenceWrapper);
    };

    /**
     *  @function loadSample
     *      Load a sample from given path into the application with the given
     *      name.
     *
     *  @param {string} path
     *      The path where to find the sample.
     *
     *  @returns {number}
     *      The index of the newly loaded sound.
     */
    MasterManager.loadSample = function loadSample(path)
    {
        let soundWrapper = MixEnergetik.Utilitary.SoundBuilder.createSoundWrapper(path);

        let soundIndex = MixEnergetik.AudioManager.provideSound(soundWrapper);

        return soundIndex;
    }

    /**
     *  @function loadSamples
     *      Load samples contained in pathTree parameter, in a special form.
     *      Recursive.
     *
     *  @param {string} basePath
     *      The basePath from which the sampleFileTree takes its (potentially
     *      relative) root
     *  @param {object || string} sampleFileTree
     *      The file tree object, containing an array of file/directory.
     *  @param {number} soundIndices
     *      After loading the sounds, soundIndices contains all indices of
     *      those sounds.
     */
    MasterManager.loadSamples = function loadSamples(basePath, sampleFileTree, soundIndices)
    {
        MixEnergetik.Utilitary.Check.native(basePath, "string",
            "MixEnergetik.MasterManager.loadSamples()", "basePath");
        MixEnergetik.Utilitary.Check.array(soundIndices,
            "MixEnergetik.MasterManager.loadSamples()", "soundIndices");

        if(typeof sampleFileTree === "string")
        {
            /* File */
            soundIndices.push(MixEnergetik.MasterManager.loadSample(basePath + "/" + sampleFileTree, sampleFileTree));
        }
        else if(typeof sampleFileTree === "object")
        {
            /* Directory */
            for (let path in sampleFileTree)
            {
                if (sampleFileTree.hasOwnProperty(path))
                {
                    for (let i = 0; i < sampleFileTree[path].length; i++)
                    {
                        MixEnergetik.MasterManager.loadSamples(basePath + "/" + path, sampleFileTree[path][i], soundIndices);
                    }
                }
            }
        }
    };

    /**
     *  @function retrievePaths
     *      Retrieve paths in the application for the ressources.
     *      Depending on the type of ressources and the phase of the
     *      application, the paths may vary.
     *      Retrieved paths are given to successHandler as the parameter, with
     *      is json formated data.
     *
     *  @note
     *      Data returned by retrievePaths can be used directly with loadSamples
     *      for samples. However, it can also be used freely. Data structure is:
     *      - Object for directory, with only one property:
     *          - The key is the directory name, while the value is an array of
     *          the directory's content.
     *      - Strings are for filenames.
     *      Return value's example:
     *      {
     *          "Dir0":[{"Dir1":["file1_0.txt"]}, "file0_0.txt", "file0_1.txt"]
     *      }
     *
     *  @param {string} type
     *      The type of the resource to retrieve its path.
     *  @param {string} phase
     *      The phase in which the application is, to get the correct path.
     *  @param {function} successHandler
     *      Callback for when the XMLHttpRequest finish, on success. Receives
     *      data as json format in parameter.
     *      (signature: function(jsonResponse))
     *  @param {function} errorHandler <optional>
     *      Callback in case of error. Receives the status of the XMLHttpRequest
     *      and a string describing the error.
     *      (signature: function(status, errorString))
     */
    MasterManager.retrievePaths = function retrievePaths(type, phase,
        successHandler, errorHandler)
    {
        MixEnergetik.Utilitary.Check.native(type, "string",
            "MixEnergetik.MasterManager.retrievePaths()", "type");
        MixEnergetik.Utilitary.Check.native(phase, "string",
            "MixEnergetik.MasterManager.retrievePaths()", "phase");
        MixEnergetik.Utilitary.Check.native(successHandler, "function",
            "MixEnergetik.MasterManager.retrievePaths()", "successHandler");

        if (errorHandler && (typeof errorHandler !== "function"))
        {
            throw new Error("[MixEnergetik.MasterManager.retrievePaths()] the"
                + "errorHandler parameter must, if provided, be a function.");
        }

        /***/

        let xhr = new XMLHttpRequest();
        let url = MixEnergetik.MasterManager.PROJECT_ROOT
            + "/server/retrieveLocation.php";
        let paramString = "";

        xhr.open("POST", url, true);

        // Send the proper header information along with the request
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.responseType = "json";

        xhr.onreadystatechange = function()
        {
            if(xhr.readyState == XMLHttpRequest.DONE)
            {
                if (xhr.status == 200)
                {
                    let response = xhr.response || null;

                    if (response === null)
                    {
                        throw new Error("[MixEnergetik.MasterManager.retrievePaths()]"
                            + " Server returned a non parsable response.");
                    }

                    /* Check if its of good form */
                    if ((!response.name)
                        || (!response.type)
                        || ((response.type === "directory")
                            && (!response.content)))
                    {
                        throw new Error("[MixEnergetik.MasterManager.retrievePaths()]"
                            + " The server's response is not a valid tree.");
                    }

                    successHandler(response);
                }
                else
                {
                    if(errorHandler)
                    {
                        errorHandler(xhr.status, "Couldn't retrieve URLs");
                    }
                }
            }
        };

        paramString = "type=" + type + "&phase=" + phase;

        xhr.send(paramString);
    };

    return MasterManager;
})();
