/*
 *  File:   UI.js
 *  Author: Marcelin Lazzarotto
 *  Date:   2018-01-29
 */

/**
 *  @namespace MixEnergetik
 *      The project namespace, to avoid polluting global state.
 */
var MixEnergetik = MixEnergetik || {};

/**
 *  @namespace MixEnergetik.UI
 *      Subnamespace for UI related objects.
 */
MixEnergetik.UI = MixEnergetik.UI || {};

/**
 *  @module UI
 *      The global module dealing with user interface.
 *      Groups all UI submodule under its namespace.
 *      Also kinda act as a controller for a few special functions.
 */
MixEnergetik.UI._ = (function()
{
    /**
     *  @constructor State
     *      Defines a state in the application.
     */
    function State()
    {
        this.player = 0;
        this.options = {};
    }

    State.Player =
    {
        NONE: 0,
        PRESET: 1,
        MIXER: 2,
    };

    /* Array of states - heap */
    let states = [];

    let Whatever = Object.create(null,
    {
        /**
         *  @function saveState
         *      Must absolutely be called before a global disable, to keep track
         *      of what the state of the application was.
         */
        saveState:
        {
            configurable: false,
            enumerable: true,
            writable: false,

            value: function()
            {
                let state = new State();

                if (MixerController.isActive())
                {
                    state.player = State.Player.MIXER;
                }
                else if (MixEnergetik.GroupController.isActive())
                {
                    let activePreset = MixEnergetik.GroupController.getActive();

                    state.player = State.Player.PRESET;
                }
                else
                {
                    state.player = State.Player.NONE;
                }

                states.push(state);
            }
        },

        /**
         *  @function restoreState
         *      Must be called when a restoration is in order, to reenable the
         *      correct samples.
         */
        restoreState:
        {
            configurable: false,
            enumerable: true,
            writable: false,

            value: function()
            {
                let state = states.pop();
                if (!state)
                {
                    return;
                }

                /* In all case, enable all */
                this.enableAll();

                if (state.player === State.Player.MIXER)
                {
                    MixEnergetik.UI.MapView.disable();
                    MixEnergetik.UI.PresetPanelView.enable();
                }
            }
        },

        /**
         *  @function disableAll
         *      Disable all samples in the application.
         *      Used with solo.
         */
        disableAll:
        {
            configurable: false,
            enumerable: true,
            writable: false,

            value: function()
            {
                MixEnergetik.UI.MapView.disable();
                MixEnergetik.UI.PresetPanelView.disable();
                MixEnergetik.UI.SequencerView.disable();
            }
        },

        /**
         *  @function enableAll
         *      Enable all samples in the application.
         */
        enableAll:
        {
            configurable: false,
            enumerable: true,
            writable: false,

            value: function()
            {
                MixEnergetik.UI.MapView.enable();
                MixEnergetik.UI.PresetPanelView.enable();
                MixEnergetik.UI.SequencerView.enable();
            }
        },
    });

    return Whatever;
})();


/**
 *  Roadmap (TODO)
 *  - Prepare functions to draw items. We need:
 *      - a function to draw&add a sample square
 *      - to decide how we put this sample in somewhere
 *      - a function to draw the presets. There are 2 modes, empty and grouped:
 *          - empty is a preset in which we can put environment samples
 *          - grouped is a preset in which we can add environment samples and
 *          use in the mixer
 *      - a function to draw the mixer table's cells with two button to
 *          add/remove tempo (cells) and a default of 8 cells
 *      - to add function to the mixer buttons to play/pause and mute the mix
 *      - to add a master volume slider on the mix (just for the window, not for
 *          the final mix)
 *      - left click/right click routines for respectively listen to sample with
 *      all currently playing : listen to sample in solo
 *      - left click drag&drop to add an element sample to a mixer element cell
 *      - left click drag&drop to add a sample to a preset
 *      - left click drag&drop to add a preset to a mixer environmentCell
 */

/*
Vocabulary note: greyed == less colorful, but also non playable if not forced
    solo (== right click) except special cases like drag & drop
Unless stated otherwise, left and right click are maintained to keep effect

Case 0:
No sound playing from mixer.
No sound playing from a preset.
    Element source
        Left click == solo play
        Right click == solo play
            Note: all other audio sources greyed
        Left click drag&drop == put the sample in a mixer element cell
            Note: all other non-usable element greyed during drag until drop
                or cancel.
    Environnement source
        Left click == solo play
        Right click == solo play
            Note: all other audio sources greyed
        Left click drag&drop == put the audio source in one of the preset
            audio source.
            Note: all audio source greyed during drag except the preset of
                same environment. This lasts until drop or cancel.
    Preset source
        Left click == activate (!) (single click, do nothing if maintained
            until keyup)
            Note: all environment audio sources greyed except those from the
            same environment as the preset
        Right click == solo play
            Note: all other audio sources greyed
        Left click drag&drop == put the preset audio source inside one of
            the mixer cells. All audio source greyed except environment
            mixer cells until drop or cancel.
    Mixer
        Element container
            Left click == grey all but elements (to indicate what goes
                there) and plays preset of same tempo if present
        Environnement container
            Left click == grey all but presets (to indicate what goes there)
                and plays element of same tempo if present
        Element source
            Left click == play the clicked element source alongside the
                preset source that is in the same tempo.
                Note: grey the rest during play
            Right click == play solo
                Note: grey the rest during play
            Left click drag&drop == remove source (or move it?)
        Environnement preset source
            Left click == play the clicked element source alongside the
                preset source that is in the same tempo.
                Note: grey the rest during play
            Right click == play solo
                Note: grey the rest during play
            Left click drag&drop == remove source (or move it?)
        Play button
            Left click == toggle on whole sequence play && loop.
                Replace play button with pause button.
        Mute button
            Silencing the whole mix sequence (not really useful while not
                running)
                Replace mute button by unmute button
        Unmute button
            Make it loud again (not really useful while not running)
                Replace unmute button by mute button
Case 1:
No sound playing from mixer.
Preset activated and playing sound (or silent)
    Element source
        Left click == play alongside preset, start at corresponding time
        Right click == solo play
            Note: all other audio sources greyed and resume preset playing when
                unclicked
        Left click drag&drop == put the sample in a mixer element cell
            Note: all other non-usable element greyed during drag until drop
                or cancel.
    Environnement source
        Left click == if source is in same environment as preset then play
            alongside preset, start at corresponding time, do nothing otherwise
        Right click == solo play
            Note: all other audio sources greyed and resume preset playing when
                unclicked
        Left click drag&drop == if source is in same environment as preset then
            can move it and add the audio source in one of the preset audio
            source (same source can't be added a second time). Do nothing
            otherwise
            Note: all audio source greyed during drag except the preset of
                same environment. This lasts until drop or cancel.
    Preset source
        Left click == if clicked is activated : deactivate (!) (single click,
            do nothing if maintained until keyup) else deactivate current and
            activate clicked
            Note: all environment audio sources returns to normal design
        Right click == solo play, so... do nothing if current == clicked, play
            the other alone otherwise
            Note: all other audio sources greyed
        Left click drag&drop == put the preset audio source inside one of
            the mixer cells. All audio source greyed except environment
            mixer cells until drop or cancel. If drag has started then the
            preset is deactivated like a normal click, whatever the drop.
    Mixer
        Element container
            Left click == grey all but elements (to indicate what goes
                there) and do nothing
        Environnement container
            Left click == grey all but presets (to indicate what goes there)
                and plays element of same tempo if present alongside currently
                playing preset in sync
        Element source
            Left click == play the clicked element source alongside the
                preset currently playing and in sync
                Note: grey the rest during play
            Right click == solo play
                Note: grey the rest during play and resume preset playing when
                    unclicked
            Left click drag&drop == remove source (or move it?)
        Environnement preset source
            Left click == do nothing
            Right click == solo play
                Note: grey the rest during play and resume preset playing when
                    unclicked
            Left click drag&drop == remove source (or move it?)
        Play button
            Left click == toggle on whole sequence play && loop.
                ! deactivate preset play
                Replace play button with pause button.
        Mute button
            Silencing the whole mix sequence (not really useful while not
                running)
                Replace mute button by unmute button
        Unmute button
            Make it loud again (not really useful while not running)
                Replace unmute button by mute button
Case 2:
No sound playing from a preset
Sound playing from mixer.
    Element source
        Left click == play alongside mix sequence instead of whatever element in
            the mix tempo, start at corresponding time and loops while
            maintained
        Right click == solo play
            Note: all other audio sources greyed, pause mix and resume it when
                unclicked
        Left click drag&drop == put the sample in a mixer element cell
            Note: all other non-usable element greyed during drag until drop
                or cancel.
    Environnement source
        Left click == do nothing
        Right click == solo play
            Note: all other audio sources greyed, pause mix and resume it when
                unclicked
        Left click drag&drop == move the source to a preset from same
            environment. Does not activate preset but still change audio in mix
            if preset is used.
            Note: all audio source greyed during drag except the preset of
                same environment. This lasts until drop or cancel.
    Preset source
        Left click == activate (!) (single click, do nothing if maintained
            until keyup)
            deactivate mix (pause button becomes play)
            Note: all environment audio sources greyed except those from the
            same environment as the preset
        Right click == solo play
            Note: all other audio sources greyed, pause mix and resume it when
                unclicked
        Left click drag&drop == put the preset audio source inside one of
            the mixer cells. All audio source greyed except environment
            mixer cells until drop or cancel.
    Mixer
        Element container
            Left click == grey all but elements (to indicate what goes
                there) and put mixer sequence in this tempo. mixer continues
                playing but loop in this tempo if the click is maintained
        Environnement container
            Left click == grey all but presets (to indicate what goes there)
                and put mixer sequence in this tempo. mixer continues playing
                but loop in this tempo if the click is maintained
        Element source
            Left click == put mixer sequence in this tempo. mixer continues
                playing but loop in this tempo if the click is maintained
                Note: grey the rest during play
            Right click == solo play
                Note: grey the rest during play, stop mix and resume it when
                    unclicked
            Left click drag&drop == remove source (or move it?)
        Environnement preset source
            Left click == put mixer sequence in this tempo. mixer continues
                playing but loop in this tempo if the click is maintained
                Note: grey the rest during play
            Right click == solo play
                Note: grey the rest during play, stop mix and resume it when
                    unclicked
            Left click drag&drop == remove source (or move it?)
        Stop button
            Left click == toggle off whole sequence play && loop.
                Replace play button with pause button.
        Mute button
            Silencing the whole mix sequence. Mix still runs but with no sound.
                Replace mute button by unmute button
        Unmute button
            Make it loud again
                Replace unmute button by mute button
*/

/*
Unclassified note: changing a preset while it is used by the mix update it in
    the mix also !
*/
