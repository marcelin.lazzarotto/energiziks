/*
 *  File:   RegionData.js
 *  Author: Marcelin Lazzarotto
 *  Date:   2018-07-07
 */

"use strict";

/**
 *  @namespace MixEnergetik
 *      The project namespace, to avoid polluting global state.
 */
var MixEnergetik = MixEnergetik || {};

/**
 *  @constructor MixEnergetik.RegionData
 *      Wrapper for Pizzicato.Effect sub objects so that we contain additional
 *      information.
 *      Just here to contain information, to be used by a SoundWrapper object.
 */
MixEnergetik.RegionData = (function()
{
    /**
     *  @constructor RegionData
     *      RegionData object represent region data as the program will use
     *      them. After retrieving opendata-rte results, the data will be stored
     *      inside this object for easiest manipulation.
     *
     *  @note
     *      All energyX properties are in MW dimension, as transmitted by RTE.
     */
    let RegionData = function RegionData()
    {
        /**
         *  @property {string} name
         *      Name of the region.
         */
        this.name = "";
        /**
         *  @property {string} id
         *      Id of the region, as specified by ourselfves.
         */
        this.id = "";
        /**
         *  @property {Date} timestamp
         *      The time at which region's data correspond (i.e. the moment they
         *      were collected by the eco2mix organism).
         *      Default to 1970-01-01T00:00.
         */
        this.timestamp = new Date(1970, 0, 1, 0, 0);
        /**
         *  @property {number} consumption
         *      The total energy consumption of the region.
         */
        this.consumption = +0;
        /**
         *  @property {Object} production
         *      "Namespace" for the energy production of the region.
         */
        this.production =
        {
            /**
             *  @property {number} thermic
             *      Thermic energy production (== coal).
             *      We don't use this.
             */
            thermic: +0,
            /**
             *  @property {number} nuclear
             *      Nuclear energy production (== nuclear power plant).
             */
            nuclear: +0,
            /**
             *  @property {number} eolian
             *      Eolian energy production (== wind turbine).
             */
            eolian: +0,
            /**
             *  @property {number} solar
             *      Solar energy production (== solar power plant).
             */
            solar: +0,
            /**
             *  @property {number} hydrolic
             *      Hydrolic energy production (== tidal turbine).
             */
            hydrolic: +0,
            /**
             *  @property {number} bioenergy
             *      Biomass related energy production (== burning
             *      wood/paper...).
             */
            bioenergy: +0,
            /**
             *  @property {number} pumping
             *      Energy pumped in the STEP (Station d'Échange d'Énergie de
             *      Pompage), which are hydrolic power plant who store water
             *      in huge water pool when there is an energy's surplus. This
             *      allows to reuse said water as an hydrolic energy source when
             *      needed.
             *      Value is alwars <= 0 (apparently), or null.
             */
            pumping: -0
        };
        /**
         *  @property {number} trade
         *      Energy traded with adjacent regions. Negative is exporting,
         *      positive is importing.
         */
        this.trade = +0;
    };

    /**
     *  @function getProductionSum
     *      Sums all the sources of production together and return that value.
     */
    RegionData.prototype.getProductionSum = function getProductionSum()
    {
        return (this.production.thermic + this.production.nuclear
            + this.production.eolian + this.production.solar
            + this.production.hydrolic + this.production.bioenergy
            + this.production.pumping);
    };

    /**
     *  @function getProductionLevel
     *      Returns the production level corresponding with the values.
     */
    RegionData.prototype.getProductionLevel = function getProductionLevel()
    {
        let level = MixEnergetik.RegionManager.levelFromScale(this.getProductionSum(),
            MixEnergetik.RegionManager.energetic.scale);

        level = (level === -1) ? 1 : level;

        return level;
    };
    /**
     *  @function getConsumptionLevel
     *      Returns the consumption level corresponding with the values.
     */
    RegionData.prototype.getConsumptionLevel = function getConsumptionLevel()
    {
        let level = MixEnergetik.RegionManager.levelFromScale(this.consumption,
            MixEnergetik.RegionManager.energetic.scale);

        level = (level === -1) ? 1 : level;

        return level;
    };

    /**
     *  @function getTradeType
     *      Returns the type of trade the region do.
     *
     *  @returns {string}
     *      Possible values are:
     *      - "TYPE_SELF_SUFFICIENT" where the region neither import nor export
     *      - "TYPE_EXPORT" where the region will export sounds.
     *      - "TYPE_IMPORT" where the region can import sounds.
     */
    RegionData.prototype.getTradeType = function getTradeType()
    {
        const level = this.getTradeLevel();

        if (level > 0)
        {
            return "TYPE_IMPORT";
        }
        else if (level < 0)
        {
            return "TYPE_EXPORT";
        }
        else
        {
            return "TYPE_SELF_SUFFICIENT";
        }
    };

    /**
     *  @function getTradeLevel
     *      Returns the trade level corresponding with the values.
     */
    RegionData.prototype.getTradeLevel = function getTradeLevel()
    {
        let level = 0;

        if (this.trade > 0)
        {
            level = MixEnergetik.RegionManager.levelFromScale(this.trade,
                MixEnergetik.RegionManager.trade.scale);

            level = (level === -1) ? 0 : level;
        }
        else if (this.trade < 0) /* which means exportation */
        {
            level = MixEnergetik.RegionManager.levelFromScale(-this.trade,
                MixEnergetik.RegionManager.trade.scale);

            level = (level === -1) ? 0 : -level;
        }

        return level;
    };

    return RegionData;
})();
