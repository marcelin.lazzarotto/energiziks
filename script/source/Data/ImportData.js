/*
 *  File:   ImportData.js
 *  Author: Marcelin Lazzarotto
 *  Date:   2018-07-07
 */

"use strict";

/**
 *  @namespace MixEnergetik
 *      The project namespace, to avoid polluting global state.
 */
var MixEnergetik = MixEnergetik || {};

/**
 *  @constructor MixEnergetik.ImportData
 *      Wrapper for Pizzicato.Effect sub objects so that we contain additional
 *      information.
 *      Just here to contain information, to be used by a SoundWrapper object.
 */
MixEnergetik.ImportData = (function()
{
    let ImportData = function ImportData()
    {
        /**
         *  @property {number} length
         *      Indicates the number of sample imported.
         */
        this.length = 0;

        /**
         *  @property {MixEnergetik.SoundData objectmap} soundDatas
         *      The related sound datas are stored here.
         *      Identified by their sound index.
         */
        this.soundDatas = {};
    }; /* constructor MixEnergetik.ImportData */

    /**
     *  @function addSound
     *      Add a sound data object to the array of sound data.
     *
     *  @param {MixEnergetik.SoundData} soundData
     *      The sound data to import.
     */
    ImportData.prototype.addSound = function addSound(soundData)
    {
        MixEnergetik.Utilitary.Check.object(soundData, MixEnergetik.SoundData,
            "MixEnergetik.ImportData.removeSoundData()", "soundData",
            "MixEnergetik.SoundData");


        this.soundDatas[soundData.soundIndex] = soundData;
        this.length++;
    };

    /**
     *  @function removeSound
     *      Removes an imported sample from the object.
     *
     *  @param {number} soundIndex
     *      The sound index of the sound to remove.
     */
    ImportData.prototype.removeSound = function removeSound(soundIndex)
    {
        MixEnergetik.Utilitary.Check.native(soundIndex, "number",
            "MixEnergetik.ImportData.removeSound()", "soundIndex");

        if (this.soundDatas[soundIndex])
        {
            this.soundDatas[soundIndex] = null;
            delete this.soundDatas[soundIndex];
            this.length--;
        }
    };

    /**
     *  @function clear
     *      Removes all import data.
     */
    ImportData.prototype.clear = function clear()
    {
        this.length = 0;
        this.soundDatas = {};
    };

    return ImportData;
})();
