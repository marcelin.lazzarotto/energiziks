/*
 *  File:   Helpers.js
 *  Author: Marcelin Lazzarotto
 *  Date:   2018-04-07
 *
 *  Brief:  Contains helper functions used by several modules that can't be
 *      classed.
 */

"use strict";

/**
 *  @namespace MixEnergetik
 *      The project namespace, to avoid polluting global state.
 */
var MixEnergetik = MixEnergetik || {};

/**
 *  @namespace MixEnergetik.Utilitary
 *      Subnamespace for Utilitary related objects.
 */
MixEnergetik.Utilitary = MixEnergetik.Utilitary || {};

/**
 *  @namespace Helpers
 *      Subnamespace for helpers.
 */
MixEnergetik.Utilitary.Helpers = (function()
{
    let Helpers = {};

    /**
     *  @function makeElementSlideable
     *      Add routines to make the given element slideable (drab & slide).
     *      Requires both an element and its parent.
     *
     *  @param {HTMLElement} element
     *      The element to
     */
    Helpers.makeElementSlideable = function makeElementSlideable(element, parent)
    {
        let grabbing = false;
        let lastX = 0;
        let lastY = 0;

        element.addEventListener("mousedown", function(event)
            {
                grabbing = true;
            }, false);

        element.addEventListener("mousemove", function(event)
            {
                const BUTTON_MAIN = 1;
                const x = event.clientX;
                const y = event.clientY;

                if (grabbing)
                {
                    let offsetX = lastX - x;
                    let offsetY = lastY - y;

                    const scrollX = parent.scrollLeft + offsetX;
                    const scrollY = parent.scrollTop  + offsetY;

                    parent.scrollTo(scrollX, scrollY);
                }

                lastX = x;
                lastY = y;
            }, false);
        document.addEventListener("mouseup", function(event)
            {
                grabbing = false;
            }, false);
    }

    return Helpers;
})();
