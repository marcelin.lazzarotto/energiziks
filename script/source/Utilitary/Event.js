/*
 *  File:   Event.js
 *  Author: Marcelin Lazzarotto
 *  Date:   2018-01-29
 */

"use strict";

/**
 *  @namespace MixEnergetik
 *      The project namespace, to avoid polluting global state.
 */
var MixEnergetik = MixEnergetik || {};

/**
 *  @namespace MixEnergetik.Utilitary
 *      Subnamespace for Utilitary related objects.
 */
MixEnergetik.Utilitary = MixEnergetik.Utilitary || {};

/**
 *  @constructor Event
 *      Custom event system. Mostly taken from Pizzicato (which is prob. taken
 *      from somewhere else still).
 */
MixEnergetik.Utilitary.Event = (function()
{
    let Identification = {};

    Identification.count = 0;
    Identification.freeds = [];

    Identification.allocate = function allocate()
    {
        let id = 0;

        if (Identification.freeds.length > 0)
        {
            id = Identification.freeds.pop();
        }
        else
        {
            Identification.count++;
            id = Identification.count;
        }

        return id;
    };

    Identification.deallocate = function deallocate(identifier)
    {
        if (identifier > Identification.count)
        {
            return;
        }
        else if (identifier === Identification.count)
        {
            Identification.count--;
        }
        else
        {
            Identification.freeds.push(identifier);
        }
    };

    let Event = function Event()
    {
    };

    /**
     *  @function on
     *      Adds an event handler that will be treated upon the triggering of
     *      that event.
     *
     *  @param {string} name
     *      Name of the event, used to identify it.
     *  @param {function} callback
     *      The function to call upon event triggered.
     *  @param {object} context
     *      An additional context in which the callback should be executed.
     *
     *  @returns {number}
     *      An identifier which identify the callback. Can be used with off()
     *      function to remove a precise callback.
     */
    Event.prototype.on = function on(name, callback, context)
    {
        MixEnergetik.Utilitary.Check.native(name, "string",
            "MixEnergetik.Utilitary.Event.on", "name");
        MixEnergetik.Utilitary.Check.native(callback, "function",
            "MixEnergetik.Utilitary.Event.on", "callback");

        this._events = this._events || {};
        let _event = this._events[name] || (this._events[name] = {});
        let identifier = Identification.allocate();

        /* ? */
        _event[identifier] = {
            callback: callback,
            context: context || this
        };

        return identifier;
    };

	/**
     *  @function trigger
     *      Triggers a particular event. If a handler is linked to that event,
     *      the handler will be executed.
	 */
	Event.prototype.trigger = function trigger(name)
    {
        MixEnergetik.Utilitary.Check.native(name, "string",
            "MixEnergetik.Utilitary.Event.trigger", "name");

		let _event;
		let length;
        let args;

		this._events = this._events || {};
		_event = this._events[name] || (this._events[name] = {});

		if (!_event)
        {
            return;
        }

		length = Math.max(0, arguments.length - 1);
		args = [];

		for (let i = 0; i < length; i++)
        {
            args[i] = arguments[i + 1];
        }

        for (let identifier in _event)
        {
            if (_event.hasOwnProperty(identifier))
            {
                _event[identifier].callback.apply(_event[identifier].context, args);
            }
        }
	};

	/**
     *  @function off
     *      Removes an event handler. If no identifier is provided, all handlers
     *      for this events are removed. If no name is provided, all handlers
     *      for this object will be removed.
     *
     *  @param {string} name
     *      The name of the event to remove callback(s) from.
     *  @param {number} identifier
     *      The identifier of the callback to remove from the event. Nothing
     *      happens if the identifier does not correspond to the event.
	 */
	Event.prototype.off = function off(name, identifier)
    {
		if (name)
        {
            if (this._events[name])
            {
                if (identifier)
                {
                    delete this._events[name][identifier];
                }
                else
                {
                    this._events[name] = undefined;
                }
            }
            else
            {
                console.log("[Event] No event named \"" + name + "\"");
            }
        }
		else
        {
            this._events = {};
        }
    };

    /**
     *  @function oneshot
     *      Used to register a listener that will be called at most only once.
     *      The listener is automatically removed afterward.
     *      The functions doesn't return any id.
     *
     *  @param {string} name
     *      Name of the event, used to identify it.
     *  @param {function} callback
     *      The function to call upon event triggered.
     *  @param {object} context
     *      An additional context in which the callback should be executed.
     */
    Event.prototype.oneshot = function oneshot(name, callback, context)
    {
        let callbackId = 0;

        let wrapper = function()
        {
            this.off(event, callbackId);

            callback();
        };

        callbackId = this.on(event, callback, context);
    };

    return Event;
})();
