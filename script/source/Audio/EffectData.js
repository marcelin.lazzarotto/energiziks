/*
 *  File:   EffectData.js
 *  Author: Marcelin Lazzarotto
 *  Date:   2018-01-29
 */

/**
 *  @namespace MixEnergetik
 *      The project namespace, to avoid polluting global state.
 */
var MixEnergetik = MixEnergetik || {};

/**
 *  @constructor MixEnergetik.EffectData
 *      Create a wrapper for effects so that we keep data depending on contexts.
 *
 *  @param {integer} effectIndex
 *      Index of the effect in its owner effect array.
 *  @param {integer} position
 *      Position of the effect in the side chain.
 *      -1 indicates the effect is not used for its owner, but then it should
 *      not appear here.
 *
 *  @throws {Error}
 *      If any of the parameters is invalid.
 */
MixEnergetik.EffectData = (function()
{
    let EffectData = function EffectData(effectIndex, position)
    {
        /* Declarations */
        this.index = 0;
        this.position = 0;
        this.options = {};
        this.callback = null;

        /* Construction */

        if ((effectIndex === undefined)
            || (effectIndex === null)
            || (effectIndex < 0))
        {
            throw new Error("[MixEnergetik.EffectData()] the effectIndex parameter"
                + " must be provided and a valid index");
        }
        if ((position === undefined)
            || (position === null)
            || (position < 0))
        {
            throw new Error("[MixEnergetik.EffectData()] the position parameter"
                + " must be provided and a valid position");
        }

        this.index = effectIndex;
        this.position = position;
    }; /* EffectData */

    /**
     *  @function copyFrom
     *      Save the given effect options (and callback) into this.
     *
     *  @param {MixEnergetik.EffectData || baseEffect <Pizzicato>} effect
     *      An effect containing information. Either a EffectData object or
     *      a baseEffect (from Pizzicato) object, since both are constructed
     */
    EffectData.prototype.copyFrom = function copyFrom(effect)
    {
        if ((effect === null)
            || (effect === undefined)
            || !((effect instanceof EffectData)
                || (effect instanceof baseEffect))
            )
        {
            throw new Error("[EffectData.copyFrom()] the effect parameter"
                + " must be provided and a valid effect descriptor");
        }

        for (let key in effect.options)
        {
            this.options[key] = effect.options[key];
        }

        this.callback = effect.callback;
    }

    /**
     *  @function copyTo
     *      Load the given effect options (and callback) into the destination
     *      effect holder.
     *      The destination should be able to deal with effect description.
     */
    EffectData.prototype.copyTo = function copyTo(destination)
    {
        if ((destination === null)
            || (destination === undefined)
            || !((destination instanceof EffectData)
                || (destination instanceof baseEffect))
            )
        {
            throw new Error("[EffectData.copyTo()] the destination parameter"
                + " must be provided and a valid destination descriptor");
        }

        for (let key in this.options)
        {
            destination.options[key] = this.options[key];
        }

        destination.callback = this.callback;
    }

    /**
     *  @function clone
     *      Clones the EffectData object. Creates a new EffectData with same
     *      values.
     *
     *  @returns {MixEnergetik.EffectData}
     *      The cloned EffectData object with copied information.
     */
    EffectData.prototype.clone = function clone()
    {
        let clone = new EffectData(this.index, this.position);

        clone.index = this.index;
        clone.position = this.position;

        this.copyTo(clone);

        return clone;
    }

    return EffectData;
})();
