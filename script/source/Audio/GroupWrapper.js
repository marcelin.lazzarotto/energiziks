/*
 *  File:   GroupWrapper.js
 *  Author: Marcelin Lazzarotto
 *  Date:   2018-06-02
 */

/**
 *  @namespace MixEnergetik
 *      The project namespace, to avoid polluting global state.
 */
var MixEnergetik = MixEnergetik || {};

/**
 *  @constructor MixEnergetik.GroupWrapper
 *      Wrapper around Pizzicato group to kinda have event.
 */
MixEnergetik.GroupWrapper = (function()
{
    /**
     *  @constructor GroupWrapper
     *      Contains the information to allow the play to take place.
     */
    function GroupWrapper()
    {
        /**
         *  @property {integer} semaphore
         *      Big name for not exactly it, but kinda.
         *      Fact 1: loop is broken as we cannot add sounds starting to the
         *          time we want when we do so
         *      Fact 2: with non-looped samples this works but the group must
         *      have to wait for all samples to finish before restarting (this
         *      is not much but that is still a 0.000001s delay or something).
         *      So I (Marcelin) came up with this idea. Not so good but, oh,
         *      well.
         */
        this.semaphore = 0;
        /**
         *  @property {float} lastTimePlayed
         *      Second synchronization property we need, the time at which the
         *      audio restarted playing, to be sure to add the correct time
         *      offset when adding a sound in the middle of a play.
         */
        this.lastTimePlayed = 0.0;
        /**
         *  @property {Pizzicato.Group} group
         *      The group containing the sounds.
         */
        this.group = new Pizzicato.Group();
        /**
         *  @property {MixEnergetik.EffectHandler} effectHandler
         *      Handler for effects we will add to the group.
         */
        this.effectHandler = new MixEnergetik.EffectHandler();

        /**
         *  @property {MixEnergetik.Utilitary.Event} event
         *      Event manager of a group.
         *      Supported events are:
         *      "end" when a group has finished playing.
         */
        this.event = new MixEnergetik.Utilitary.Event();
    };

    GroupWrapper.prototype = Object.create(null,
    {
        /**
         *  @function contains
         *      Checks if a sound is inside the group.
         *
         *  @param {Pizzicato.Sound} sound
         *      The sound to check presence.
         *
         *  @returns {bool}
         *      true if sound is inside this.group
         *      false otherwise
         */
        contains:
        {
            configurable: false,
            enumerable: true,
            writable: false,

            value: function(sound)
            {
                return (this.group.sounds.indexOf(sound) > -1);
            }
        },

        /**
         *  @function isEmpty
         *      Checks if the audio group is empty.
         *
         *  @returns {bool}
         *      true if the group contains no sound
         *      false otherwise
         */
        isEmpty:
        {
            configurable: false,
            enumerable: true,
            writable: false,

            value: function()
            {
                return (this.group.sounds.length === 0);
            }
        },

        /**
         *  @function addSound
         *      Add the given sound to the group.
         *
         *  @param {Pizzicato.Sound} sound
         *      The sound to add.
         */
        addSound:
        {
            configurable: false,
            enumerable: false,
            writable: false,

            value: function(sound)
            {
                if (this.contains(sound))
                {
                    return;
                }

                /* mandatory manual looping */
                sound.on("end", this.loop, this);

                if (this.isEmpty())
                {
                    this.group.volume = 1.0; // TODO
                    this.group.addSound(sound);
                    this.reset();
                }
                else
                {
                    /* in case the group is actively running, it'd be best to
                    * have the sample play at the same time so here's a little
                    * workaround to get this right */
                    let elapsedTime = Pizzicato.context.currentTime
                        - this.lastTimePlayed;

                    /* start the sound with an offset if needed */
                    sound.play(0, elapsedTime);

                    this.group.addSound(sound);
                }
            }
        },

        /**
         *  @function removeSound
         *      Remove a sound from the group of sound.
         *      Do nothing if the sound is not in the group.
         *
         *  @param {Pizzicato.Sound} sound
         *      The sound to remove.
         */
        removeSound:
        {
            configurable: false,
            enumerable: true,
            writable: false,

            value: function(sound)
            {
                if (!this.contains(sound))
                {
                    return;
                }

                sound.off("end");

                this.group.removeSound(sound);

                sound.stop();

                /* in case we removed the last sample before semaphore unlocked,
                 * we need to check */
                if (this.semaphore === this.group.sounds.length)
                {
                    /* might never happen, might happen */
                    this.reset();
                }
            }
        },

        /**
         *  @function start
         *      Starts the group.
         */
        play:
        {
            configurable: false,
            enumerable: true,
            writable: false,

            value: function()
            {
                this.group.play();

                this.event.trigger("play");
            }
        },

        /**
         *  @function pause
         *      Pauses the loop.
         */
        pause:
        {
            configurable: false,
            enumerable: true,
            writable: false,

            value: function()
            {
                this.group.pause();
            }
        },

        /**
         *  @function stop
         *      Stop the sounds.
         */
        stop:
        {
            value: function()
            {
                this.group.stop();
            }
        },

        /**
         *  @function setCurrentTime
         *      Play all the samples to given current time
         *
         *  @param {number} time
         *      The time to move the sample "cursor" to.
         */
        setCurrentTime:
        {
            configurable: false,
            enumerable: true,
            writable: false,

            value: function (time)
            {
                MixEnergetik.Utilitary.Check.native(time, "number",
                    "MixEnergetik.GroupWrapper.setCurrentTime()", "time");

                const sounds = this.group.sounds;

                for (let i = 0; i < this.group.sounds.length; i++)
                {
                    sounds[i].stop();
                    sounds[i].play(0, time);
                }

                /* Reflect on time change.
                 * TODO: check that time is not too big. */
                this.semaphore = 0;
                this.lastTimePlayed = Pizzicato.context.currentTime - time;
            }
        },

        /**
         *  @function getCurrentTime
         *      Retrieve the elapsed time of the group's play.
         *
         *  @returns {number}
         *      The elapsed time of the group.
         */
        getCurrentTime:
        {
            configurable: false,
            enumerable: true,
            writable: false,

            value: function ()
            {
                if (this.isEmpty())
                {
                    return 0.0;
                }
                else
                {
                    return Pizzicato.context.currentTime - this.lastTimePlayed;
                }
            }
        },

        /**
         *  @function clear
         *      Stops the sounds and remove them.
         */
        clear:
        {
            configurable: false,
            enumerable: true,
            writable: false,

            value: function()
            {
                this.stop();

                for (let i = this.group.sounds.length - 1; i >= 0; i--)
                {
                    this.removeSound(this.group.sounds[i]);
                }

                this.semaphore = 0;
                this.lastTimePlayed = 0;
            }
        },

        /**
         *  @function reset
         *      Reset the values and start as clean.
         */
        reset:
        {
            configurable: false,
            enumerable: false,
            writable: false,

            value: function()
            {
                this.semaphore = 0;
                this.lastTimePlayed = Pizzicato.context.currentTime;
                this.group.play();
                
                this.event.trigger("play");
            }
        },

        /**
         *  @function loop
         *      Try to loop this group of sample when all have stopped playing.
         */
        loop:
        {
            configurable: false,
            enumerable: false,
            writable: false,

            value: function()
            {
                this.semaphore++;

                if (this.semaphore === this.group.sounds.length)
                {
                    this.reset();

                    this.event.trigger("end");
                }
            }
        },
    });

    return GroupWrapper;
})();
