/*
 *  File:   EffectHandler.js
 *  Author: Marcelin Lazzarotto
 *  Date:   2018-01-29
 */

/**
 *  @namespace MixEnergetik
 *      The project namespace, to avoid polluting global state.
 */
var MixEnergetik = MixEnergetik || {};

/**
 *  @constructor MixEnergetik.EffectHandler
 *      Utilitary toolset for manipulating a list of effect, since we will need
 *      such a list with a given set of function in both SoundWrapper and
 *      GroupItem objects.
 */
MixEnergetik.EffectHandler = (function()
{
    let EffectHandler = function EffectHandler()
    {
        /**
        *  @property {MixEnergetik.EffectWrapper[]} array
        *      Array of effect, wrapped in our own object.
        */
        this.array = [];

        /**
        *  @property {integer[]} usedEffects
        *      Indices of the effects used, in order of the side chain.
        *      Those are the effect that should be "currently" used, when the sound
        *      is playing.
        */
        this.usedEffects = [];
    }; /* constructor MixEnergetik.EffectHandler */

    /**
     *  @function allocate
     *      Add an effect to the effect array.
     *      This function is a bit weird, as it first checks if an effect of
     *      same type was already given to the sound, AND if the effect is not
     *      already used. We use the effect if we can, then marking it as used,
     *      or create another.
     *      This allows for the user to side chain several time the same effects
     *      if he wants. This also helps for configuration.
     *
     *  @param {function <constructor>} effectConstructor
     *      The type of effect to add to the sound. If no usable effect is
     *      found, a new one will be created based on this constructor function.
     *  @param {Object} options
     *      The options to pass to the effect, according to Pizzicato
     *      documentation.
     *      (Optional)
     *  @param {function} callback
     *      An eventual callback to pass to the effect.
     *      (Optional)
     *  @param {integer} position
     *      Position of the effect in the side chain.
     *
     *  @return {integer}
     *      -1 if effectConstructor is not a valid parameter;
     *      The effect identifier in this sound otherwise.
     */
    EffectHandler.prototype.allocate = function allocate(effectConstructor,
        options, callback, position)
    {
        if (!effectConstructor
            || !(effectConstructor.prototype instanceof baseEffect))
        {
            /* We checking if the constructor is a valid one. */
            return new Error("[SoundWrapper.allocate()] effect should be a"
                + " provided and a valid Pizzicato.Effect sub constructor.");
        }

        if ((position === undefined)
            || (position === null)
            || (typeof position !== "number"))
        {
            return new Error("[SoundWrapper.allocate()] position must be"
                + " provided and an integer.");
        }

        let effectWrapper = null;
        let index = -1;

        for (let i = 0; i < this.array.length; i++)
        {
            if((this.array[i].pzEffect instanceof effectConstructor)
                && (this.array[i].position === -1))
            {
                effectWrapper = this.array[i];
                index = i;

                break;
            }
        }

        /* create the effect if none usable were found */
        if(!effectWrapper)
        {
            effectWrapper = new MixEnergetik.EffectWrapper();

            if (effectConstructor === Pizzicato.Effects.Convolver)
            {
                /* The only Pizzicato.Effect that takes a callback with
                 * constructor */
                effectWrapper.effect = new effectConstructor(options, callback);
            }
            else
            {
                effectWrapper.effect = new effectConstructor(options);
            }

            index = this.array.push(effectWrapper) - 1;
        }
        else
        {
            for (let key in defaults)
            {
                effectWrapper.effect[key] = options[key];
            }
        }

        this.setPosition(index, position);

        return index;
    };

    /**
     *  @function deallocate
     *      Remove an effect from the array.
     *      Actually don't delete the effect, (but we should?) just put it to
     *      rest since another configuration for the sound might use the said
     *      effect.
     *
     *      Do nothing if effectIndex parameter is invalid.
     *
     *  @param {integer} effectIndex
     *      The index of the effect to delete (=== unuse for now (TODO?)).
     */
    EffectHandler.prototype.deallocate = function(effectIndex)
    {
        /* checks */
        if ((effectIndex === undefined)
            || (effectIndex === null)
            || (typeof effectIndex !== "number")
            || (effectIndex < 0) || (effectIndex >= this.array.length))
        {
            return;
        }

        if(this.array[effectIndex].position > -1)
        {
            this.setPosition(index, position);
        }
    };

    /**
     *  @function setPosition
     *      Change the position of the effect at index effect, which also change
     *      position of some other effects in the side chain.
     *
     *  @param {integer} index
     *      The index of the effect to setPosition.
     *  @param {integer} newPosition
     *      The new position of the effect. Can be -1, meaning the effect
     *      is not used anymore.
     */
    EffectHandler.prototype.setPosition = function(index, newPosition)
    {
        /* Declarations */
        let min = function min(a, b)
        {
            return (a < b) ? a : b;
        }

        let max = function max(a, b)
        {
            return (a > b) ? a : b;
        }

        /* checks */
        if ((effectIndex === undefined)
            || (effectIndex === null)
            || (typeof effectIndex !== "number")
            || (effectIndex < 0) || (effectIndex >= this.array.length))
        {
            return;
        }
        if ((newPosition === undefined)
            || (newPosition === null)
            || (typeof newPosition !== "number"))
        {
            return;
        }

        if (this.array[index].position === newPosition)
        {
            /* nothing to do here */
            return;
        }

        /* value to which we will shift other impacted effects in the chain */
        let inf = 0;
        let sup = 0;

        if (this.array[index.position] === -1)
        {
            /* adding item */
            this.usedEffects.splice(newPosition, 0, index);

            inf = newPosition;
            sup = this.array[index].length;
        }
        if (newPosition === -1)
        {
            /* removing item */
            inf = this.array[index].position;

            this.usedEffects.splice(this.array[index].position, 1);

            sup = this.array[index].length;
        }
        else
        {
            if ((newPosition < 0) || (newPosition > this.usedEffects.length))
            {
                /* safety */
                return;
            }

            inf = min(this.array[index].position, newPosition);
            sup = max(this.array[index].position, newPosition) + 1;
        }

        for (let i = inf; i < sup; i++)
        {
            this.usedEffects[i].position = i;
        }

        this.array[index].position = newPosition;
    };

    return EffectHandler;
})();
