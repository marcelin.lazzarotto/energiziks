/*
 *  File:   PresetPanelView/script.js
 *  Author: Marcelin Lazzarotto
 *  Date:   2018-01-29
 */

"use strict";

/**
 *  @namespace MixEnergetik
 *      The project namespace, to avoid polluting global state.
 */
var MixEnergetik = MixEnergetik || {};

/**
 *  @namespace MixEnergetik.UI
 *      Subnamespace for UI related objects.
 */
MixEnergetik.UI = MixEnergetik.UI || {};

MixEnergetik.UI.PresetPanelView = (function()
{
    let PresetPanelView = {};

    /**
    *  @property {MixEnergetik.UI.GroupItem[]} presets
    *      The presets contained in the application.
    */
    PresetPanelView.presets = [];

    /**
     *  @property {MixEnergetik.Utilitary.KeyControl} keyControl
     *      The object monitoring key presses for the view.
     */
    PresetPanelView.keyControl = new MixEnergetik.Utilitary.KeyControl();

    /**
     *  @function initialize
     *      Should be called at startup.
     */
    PresetPanelView.initialize = function initialize()
    {
        PresetPanelView_container = document.getElementById(
            PresetPanelView_PRESET_VIEW_ID);
        PresetPanelView_presetsContainer = document.getElementById(
            PresetPanelView_PRESET_CONTAINER_ID);

        MixEnergetik.UI.PresetPanelView.keyControl.setKeys(
            PresetPanelView_KEY_CONTROL_KEYS);

        for (let i = 0; i < PresetPanelView_PRESET_DEFAULT_COUNT; i++)
        {
            MixEnergetik.UI.PresetPanelView.addPreset();
        }
    };

    /**
     *  @function clear
     *      Clears all presets inside the view.
     */
    PresetPanelView.clear = function clear()
    {
        for (let i = 0; i < PresetPanelView.presets.length; i++)
        {
            PresetPanelView.presets[i].clear();
        }
    };

    /**
     *  @function disable
     *      Disable all presets (typically used when the mixer is playing).
     */
    PresetPanelView.disable = function()
    {
        for (let i = 0; i < PresetPanelView.presets.length; i++)
        {
            PresetPanelView.presets[i].disable();
        }
    };

    /**
     *  @function enable
     *      Enable all presets (typically used when the mixer is playing).
     */
    PresetPanelView.enable = function()
    {
        for (let i = 0; i < PresetPanelView.presets.length; i++)
        {
            PresetPanelView.presets[i].enable();
        }
    };

    /**
     *  @function extend
     *      Extends the view.
     */
    PresetPanelView.extend = function extend()
    {
        PresetPanelView_container.classList.add("extended");
    };

    /**
     *  @function retract
     *      Retracts the view.
     */
    PresetPanelView.retract = function retract()
    {
        PresetPanelView_container.classList.remove("extended");
    };

    /**
     *  @function hide
     *      Hides the view.
     */
    PresetPanelView.hide = function hide()
    {
        PresetPanelView_container.classList.add("hidden");
    };

    /**
     *  @function show
     *      Shows the view.
     */
    PresetPanelView.show = function show()
    {
        PresetPanelView_container.classList.remove("hidden");
    };

    /**
     *  @function addPreset
     *      Adds an empty preset to the preset view.
     */
    PresetPanelView.addPreset = function addPreset()
    {
        let preset = new MixEnergetik.UI.GroupItem();

        preset.setContainer(document.createElement("div"),
            "CONTROL_ENABLE", "DRAG_ENABLE");
        preset.container.classList.add("preset");

        PresetPanelView.presets.push(preset);

        PresetPanelView.keyControl.addItem(preset);

        PresetPanelView_presetsContainer.appendChild(preset.container);
    };

    const PresetPanelView_PRESET_VIEW_ID = "PresetPanelView";
    const PresetPanelView_PRESET_CONTAINER_ID = "PresetPanelViewContainer";
    const PresetPanelView_PRESET_DEFAULT_COUNT = 8;
    const PresetPanelView_KEY_CONTROL_KEYS = ["a", "z", "e", "r",
        "q", "s", "d", "f"];

    let PresetPanelView_container = null;
    let PresetPanelView_presetsContainer = null;

    return PresetPanelView;
})();
