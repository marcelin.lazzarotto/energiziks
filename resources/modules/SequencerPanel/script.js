/*
 *  File:   SequencerView.js
 *  Author: Marcelin Lazzarotto
 *  Date:   2018-01-29
 */

"use strict";

/**
 *  @namespace MixEnergetik
 *      The project namespace, to avoid polluting global state.
 */
var MixEnergetik = MixEnergetik || {};

/**
 *  @namespace MixEnergetik.UI
 *      Subnamespace for UI related objects.
 */
MixEnergetik.UI = MixEnergetik.UI || {};

/**
 *  @submodule ElementView
 *      Deals with the element view (i.e. the element box) of the application.
 *      Keeps in mind the list of ElementItem.
 */
MixEnergetik.UI.SequencerView = (function()
{
    let SequencerView = {};

    Object.defineProperties(SequencerView,
    {
        /**
         *  @property {MixEnergetik.UI.GroupItem[]} sequences
         *      The sequencer's sequences.
         */
        sequences:
        {
            configurable: false,
            enumerable: false,
            writable: false,

            value: []
        },

        /**
         *  @function initialize
         *      Initialize the function interactions.
         */
        initialize:
        {
            configurable: false,
            enumerable: false,
            writable: false,

            value: function()
            {
                SequencerView_sequencerPanel = document.getElementById(
                    SequencerView_BASE_AUDIO_SEQUENCER_PANEL_ID);
                SequencerView_sequencerContainer = document.getElementById(
                    SequencerView_BASE_AUDIO_SEQUENCER_CONTAINER_ID);
                SequencerView_buttonPlay = document.getElementById(
                    SequencerView_BUTTON_PLAY_ID);
                SequencerView_buttonStop = document.getElementById(
                    SequencerView_BUTTON_STOP_ID);
                SequencerView_sliderVolume = document.getElementById(
                    SequencerView_SLIDER_VOLUME_ID);
                SequencerView_buttonAddSequence = document.getElementById(
                    SequencerView_BUTTON_ADD_SEQUENCE_ID);
                SequencerView_buttonRemoveSequence = document.getElementById(
                    SequencerView_BUTTON_REMOVE_SEQUENCE_ID);
                SequencerView_buttonBounce = document.getElementById(
                    SequencerView_BUTTON_BOUNCE_ID);
                SequencerView_groupBinContainer = document.getElementById(
                    SequencerView_GROUP_BIN_ID);

                let self = this;

                let volumeSliderCallback = function volumeSliderCallback(event)
                {
                    // Update the current slider value (each time you drag the slider handle)
                    MixEnergetik.SequencerManager.setVolume(+SequencerView_sliderVolume.value); // cast
                };

                SequencerView_buttonPlay.addEventListener("click", function()
                {
                    if (!MixEnergetik.SequencerManager.isActive())
                    {
                        /* if a preset is activated, deactivate it beforehand */
                        MixEnergetik.GroupManager.deactivate();
                        MixEnergetik.SequencerManager.activate();
                    }
                }, false);
                SequencerView_buttonStop.addEventListener("click", function()
                {
                    if (MixEnergetik.SequencerManager.isActive())
                    {
                        MixEnergetik.SequencerManager.deactivate();
                    }
                    if (MixEnergetik.GroupManager.isActive())
                    {
                        MixEnergetik.GroupManager.deactivate();
                    }
                }, false);
                SequencerView_sliderVolume.oninput = volumeSliderCallback;
                SequencerView_buttonAddSequence.addEventListener("click", function()
                    {
                        self.addSequence();
                    }, false);
                SequencerView_buttonRemoveSequence.addEventListener("click", function()
                    {
                        self.removeSequence();
                    }, false);

                SequencerView_buttonBounce.addEventListener("click", function()
                    {
                        MixEnergetik.SequencerManager.bounce();
                    }, false);

                /* sequencer content init */
                MixEnergetik.UI.SequencerView.setNumberOfSequences(SequencerView_BASE_SEQUENCE_NUMBER);

                SequencerView_groupBin = new MixEnergetik.UI.BinItem(
                    SequencerView_groupBinContainer, "BIN_GROUP");
                SequencerView_groupBinContainer.classList.add("unclickable");
                SequencerView_groupBinContainer.addEventListener("click",
                    SequencerView_groupBinButtonCallback, false);
                /* Button bin style */
                MixEnergetik.GroupManager.event.on("play", SequencerView_onGroupPlay);
                MixEnergetik.GroupManager.event.on("end", SequencerView_onGroupEnd);
                MixEnergetik.GroupManager.event.on("stop", SequencerView_onGroupStop);

                SequencerView_buttonResetMix = document.getElementById(
                    SequencerView_BUTTON_RESET_MIX_ID);
                SequencerView_buttonResetMix.addEventListener("click",
                    SequencerView_buttonResetMixCallback, false);
            }
        },

        /**
         *  @accessors length
         *      Accessor for the number of sequence.
         */
        length:
        {
            configurable: false,
            enumerable: true,

            get: function()
            {
                return this.sequences.length;
            }
        },

        /**
         *  @function indexOf
         *      Returns the index of a sequence in the sequencer.
         *
         *  @param {MixEnergetik.UI.GroupItem} groupItem
         *      The GroupItem object to test.
         *
         *  @returns {integer}
         *      A positive integer corresponding to the index of the groupItem
         *      in the sequencer if the GroupItem
         *      object is a sequence
         *      -1 otherwise
         */
        indexOf:
        {
            configurable: false,
            enumerable: true,
            writable: false,

            value: function(groupItem)
            {
                MixEnergetik.Utilitary.Check.object(groupItem,
                    MixEnergetik.UI.GroupItem,
                    "MixEnergetik.UI.SequencerView.isSequence()",
                    "groupItem",
                    "MixEnergetik.UI.GroupItem");

                return MixEnergetik.UI.SequencerView.sequences.indexOf(groupItem);
            }
        },

        /**
         *  @function at
         *      Returns the sequence of given index.
         *
         *  @param {integer} sequenceNumber
         *      The number of the sequence to access.
         *
         *  @returns {MixEnergetik.UI.GroupItem}
         *      The sequence requested or undefined if out of bounds.
         */
        at:
        {
            configurable: false,
            enumerable: true,
            writable: false,

            value: function(sequenceNumber)
            {
                return (((sequenceNumber >= 0) && (sequenceNumber < this.length))
                    ? this.sequences[sequenceNumber]
                    : undefined);
            }
        },

        /**
         *  @function addSequence
         *      Adds a new sequence at the end of the sequencer list.
         */
        addSequence:
        {
            configurable: false,
            enumerable: true,
            writable: false,

            value: function()
            {
                let createIcon = function createIcon(src)
                {
                    let icon = document.createElement("img");

                    icon.src = src;
                    icon.classList.add("indication");
                    icon.classList.add("icon");
                    icon.classList.add("hidden");

                    return icon;
                }

                let sequence = new MixEnergetik.UI.GroupItem();
                sequence.setContainer(document.createElement("div"), "CONTROL_ENABLE", "DRAG_ENABLE");

                sequence.container.classList.add("sequence");

                const indexValue = this.sequences.push(sequence);

                SequencerView_sequencerContainer.appendChild(sequence.container);

                /* Add video specific styling */
                if (MixEnergetik.UI.VideoSelectionView)
                {
                    let indexDiv = document.createElement("div");
                    indexDiv.classList.add("audio");
                    indexDiv.classList.add("item");
                    indexDiv.classList.add("index");
                    indexDiv.classList.add("noninteractable");
                    indexDiv.innerHTML = indexValue;

                    sequence.source.appendChild(indexDiv);
                }

                /* Add an icon for when it either play in video mode or in
                 * complete */
                const baseUrl = MixEnergetik.MasterManager.PROJECT_ROOT
                    + "/resources/default";
                sequence.sequencer_fancy = {};
                sequence.sequencer_fancy.iconPlay = createIcon(baseUrl + "/PlayIcon.png");
                sequence.sequencer_fancy.iconLoop = createIcon(baseUrl + "/LoopIcon.png");
                sequence.sequencer_fancy.iconPlay.classList.add("playing");
                sequence.sequencer_fancy.iconLoop.classList.add("looping");
                sequence.source.appendChild(sequence.sequencer_fancy.iconPlay);
                sequence.source.appendChild(sequence.sequencer_fancy.iconLoop);
            }
        },

        /**
         *  @function removeSequence
         *      Removes the last sequence from the sequencer list (cleans the content).
         */
        removeSequence:
        {
            configurable: false,
            enumerable: true,
            writable: false,

            value: function()
            {
                let sequence = this.sequences.pop();

                if (sequence)
                {
                    sequence.clear();

                    /* to avoid deleting the void */
                    SequencerView_sequencerContainer.removeChild(sequence.container);
                }
            }
        },

        /**
         *  @function clear
         *      Removes every sounds inside sequences.
         *      Calls each GroupItem objects clear function.
         */
        clear:
        {
            configurable: false,
            enumerable: true,
            writable: false,

            value: function()
            {
                /* First, desactivate the sequencer */
                MixEnergetik.SequencerManager.deactivate();

                MixEnergetik.UI.SequencerView.zero();

                // MixEnergetik.UI.SequencerView.setNumberOfSequences(SequencerView_BASE_SEQUENCE_NUMBER);
            }
        },

        /**
         *  @function disable
         *      Used to make the sequencer sequences appear disabled.
         */
        disable:
        {
            configurable: false,
            enumerable: true,
            writable: false,

            value: function()
            {
                for (let i = 0; i < this.sequences.length; i++)
                {
                    this.sequences[i].disable();
                }
            }
        },

        /**
         *  @function enable
         *      To enable those sequences again.
         */
        enable:
        {
            configurable: false,
            enumerable: true,
            writable: false,

            value: function()
            {
                for (let i = 0; i < this.sequences.length; i++)
                {
                    this.sequences[i].enable();
                }
            }
        },
    });

    /**
     *  @function zero
     *      Removes all sequences.
     */
    SequencerView.zero = function zero()
    {
        const SeqV = MixEnergetik.UI.SequencerView;

        for (let i = SeqV.sequences.length - 1; i >= 0; i--)
        {
            SeqV.removeSequence();
        }
    };

    /**
     *  @function setNumberOfSequences
     *      Make it so the sequencer contains the given number of sequences.
     *
     *  @param {number} number
     *      The number of sequences.
     */
    SequencerView.setNumberOfSequences = function setNumberOfSequences(number)
    {
        MixEnergetik.Utilitary.Check.native(number, "number",
            "MixEnergetik.UI.SequencerView.setNumberOfSequences()", "number");

        const SeqV = MixEnergetik.UI.SequencerView;

        if (SeqV.sequences.length > number)
        {
            for (let i = SeqV.sequences.length; i > number; i--)
            {
                SeqV.removeSequence();
            }
        }
        else if (SeqV.sequences.length < number)
        {
            for (let i = SeqV.sequences.length; i < number; i++)
            {
                SeqV.addSequence();
            }
        }
    };

    /**
     *  @function hide
     *      Hides the sequencer in base/ and wander/ to clean things up.
     */
    SequencerView.hide = function hide()
    {
        if (MixEnergetik.SequencerManager.isActive())
        {
            MixEnergetik.SequencerManager.deactivate();
        }

        SequencerView_sequencerPanel.classList.add("hidden");
    };

    /**
     *  @function show
     *      Shows the sequencer in base/ and wander/ to advance things up.
     */
    SequencerView.show = function show()
    {
        SequencerView_sequencerPanel.classList.remove("hidden");
    };

    /**
     *  @function use
     *      Style me this
     */
    SequencerView.use = function use()
    {
        SequencerView_buttonPlay.classList.add("on");
    };
    /**
     *  @function unuse
     *      Unstyle me this
     */
    SequencerView.unuse = function unuse()
    {
        SequencerView_buttonPlay.classList.remove("on");
    };

    /**
     *  @function specializeForVieMer
     *      Routine for changing stuff just for the vie-mer/ page.
     */
    SequencerView.specializeForVieMer = function specializeForVieMer()
    {
        SequencerView_buttonAddSequence.innerHTML = "+";
        SequencerView_buttonRemoveSequence.innerHTML = "-";
        SequencerView_buttonBounce.innerHTML = "Bounce";
        SequencerView_buttonResetMix.innerHTML = "Reset";
        SequencerView_buttonAddSequence.classList.add("nonselectable");
        SequencerView_buttonRemoveSequence.classList.add("nonselectable");
        SequencerView_buttonBounce.classList.add("nonselectable");
        SequencerView_buttonResetMix.classList.add("nonselectable");
    };

    /*
     *  The useful DOM elements.
     */
    const SequencerView_BASE_AUDIO_SEQUENCER_PANEL_ID = "SequencerPanel";
    const SequencerView_BASE_AUDIO_SEQUENCER_CONTAINER_ID = "SequencerContainer";
    /* button init */
    const SequencerView_BUTTON_PLAY_ID = "ButtonPlay";
    const SequencerView_BUTTON_STOP_ID = "ButtonStop";
    const SequencerView_SLIDER_VOLUME_ID = "SequencerVolumeSlider";
    const SequencerView_BUTTON_ADD_SEQUENCE_ID = "ButtonAddSequence";
    const SequencerView_BUTTON_REMOVE_SEQUENCE_ID = "ButtonRemoveSequence";
    const SequencerView_BUTTON_BOUNCE_ID = "ButtonBounce";
    const SequencerView_GROUP_BIN_ID = "GroupBin";
    const SequencerView_BUTTON_RESET_MIX_ID = "SequencerPanelResetMixButton";

    let SequencerView_sequencerPanel = null;
    let SequencerView_sequencerContainer = null;
    let SequencerView_buttonPlay = null;
    let SequencerView_buttonStop = null;
    let SequencerView_sliderVolume = null;
    let SequencerView_buttonAddSequence = null;
    let SequencerView_buttonRemoveSequence = null;
    let SequencerView_buttonBounce = null;
    let SequencerView_groupBinContainer = null;
    let SequencerView_buttonResetMix = null;

    let SequencerView_groupBin = null;

    /*
     *  The base number of sequences in the sequencer.
     */
    const SequencerView_BASE_SEQUENCE_NUMBER = 8;
    let SequencerView_groupBinButtonCallback = function groupBinButtonCallback()
    {
        const groupItem = MixEnergetik.GroupManager.getActive();

        if (groupItem === null)
        {
            return;
        }

        if (MixEnergetik.UI.SequencerView.indexOf(groupItem) === -1)
        {
            return;
        }

        groupItem.stop();
        groupItem.clear();
    };

    let SequencerView_buttonResetMixCallback = function resetMixButtonCallback()
    {
        MixEnergetik.UI.SequencerView.clear();

        if (!MixEnergetik.UI.MapView)
        {
            const duration = MixEnergetik.UI.VideoView.videoElement.duration;
            const sampleDuration = MixEnergetik.Utilitary.Configuration.Audio.sampleDuration;
            let numberOfSequences = Math.floor(duration / sampleDuration);

            MixEnergetik.UI.SequencerView.setNumberOfSequences(numberOfSequences);
        }
        else
        {
            MixEnergetik.UI.SequencerView.setNumberOfSequences(SequencerView_BASE_SEQUENCE_NUMBER);
        }
    };

    /**
     *  @function onGroupPlay
     *      Used to style the group bin button.
     */
    let SequencerView_onGroupPlay = function onGroupPlay()
    {
        const groupItem = MixEnergetik.GroupManager.getActive();

        if (MixEnergetik.UI.SequencerView.indexOf(groupItem) > -1)
        {
            SequencerView_groupBinContainer.classList.remove("unclickable");
        }
    };
    /**
     *  @function onGroupEnd
     *      Used to style the group bin button.
     */
    let SequencerView_onGroupEnd = function onGroupEnd()
    {
        SequencerView_groupBinContainer.classList.add("unclickable");
    };
    /**
     *  @function onGroupStop
     *      Used to style the group bin button.
     */
    let SequencerView_onGroupStop = function onGroupStop()
    {
        SequencerView_groupBinContainer.classList.add("unclickable");
    };

    return SequencerView;
})();
