/*
 *  File:   ChargeView.js
 *  Author: Marcelin Lazzarotto
 *  Date:   2018-04-20
 */

"use strict";

/**
 *  @namespace MixEnergetik
 *      The project namespace, to avoid polluting global state.
 */
var MixEnergetik = MixEnergetik || {};
/**
 *  @namespace MixEnergetik.UI
 *      Subnamespace for UI related objects.
 */
MixEnergetik.UI = MixEnergetik.UI || {};

/**
 *  @constructor MixEnergetik.UI.ChargeView
 *      Deals with the charge panel with the video (accumulator and bins).
 */
MixEnergetik.UI.ChargeView = (function()
{
    let ChargeView = {};

    /**
     *  @property {BatteryItems[]} batteryItems
     *      Keeps reference of batteryItems in order to seek sample items
     *      inside.
     */
    ChargeView.batteryItems = [];

    /**
     *  @property {MixEnergetik.Utilitary.KeyControl} keyControl
     *      The object monitoring key presses for the view.
     */
    ChargeView.keyControl = new MixEnergetik.Utilitary.KeyControl();

    /**
     *  @function initialize
     *      Initialize the view.
     */
    ChargeView.initialize = function initialize()
    {
        ChargeView_container = document.getElementById(
            ChargeView_CONTAINER_ID);
        ChargeView_accumulatorContainer = document.getElementById(
            ChargeView_ACCUMULATOR_CONTAINER_ID);
        ChargeView_recyclorContainer = document.getElementById(
            ChargeView_RECYCLOR_CONTAINER_ID);
        ChargeView_defaultRecycleImage = document.getElementById(
            ChargeView_DEFAULT_RECYCLE_IMAGE_ID);
        ChargeView_extendedRecycleImage = document.getElementById(
            ChargeView_EXTENDED_RECYCLE_IMAGE_ID);
        ChargeView_buttonShowHide = document.getElementById(
            ChargeView_RECYCLOR_BUTTON_SHOW_HIDE_ID);
        ChargeView_helpIndicator = document.getElementById(
            ChargeView_HELP_INDICATOR_ID);
        ChargeView_tutorialPopupContainer = document.getElementById(
            ChargeView_TUTORIAL_POPUP_CONTAINER_ID);
        ChargeView_tutorialVideo = document.getElementById(
            ChargeView_TUTORIAL_VIDEO_ID);

        const BIN_ITEM = 3;

        MixEnergetik.UI.ChargeView.keyControl.setKeys(ChargeView_DEFAULT_KEYS);

        for (let i = 0; i < BIN_ITEM; i++)
        {
            MixEnergetik.UI.ChargeView.createBin("RecyclorBin" + i);
        }

        for (let i = 0; i < ChargeView_ACCUMULATOR_COUNT; i++)
        {
            let batteryItem = new MixEnergetik.UI.BatteryItem();

            MixEnergetik.UI.ChargeView.batteryItems.push(batteryItem);
            MixEnergetik.UI.ChargeView.keyControl.addItem(batteryItem);

            ChargeView_accumulatorContainer.appendChild(batteryItem.container);
        }

        ChargeView_buttonShowHide.addEventListener("click",
            ChargeView_buttonShowHideCallback, false);

        ChargeView_helpIndicator.addEventListener("mouseenter",
            function()
            {
                ChargeView_tutorialPopupContainer.classList.remove("hidden");
                ChargeView_tutorialVideo.currentTime = 0.0;
            }, false);
        ChargeView_helpIndicator.addEventListener("mouseleave",
            function()
            {
                ChargeView_tutorialPopupContainer.classList.add("hidden");
            }, false);

        // TODO
        ChargeView_defaultRecycleImage.classList.add("hidden");
        ChargeView_extendedRecycleImage.classList.add("hidden");
        ChargeView_recyclorContainer.classList.add("hidden");

        ChargeView_tutorialPopupContainer.classList.add("hidden");

        ChargeView_recycleImage = ChargeView_defaultRecycleImage;
    };

    /**
     *  @function retrieveSampleItem
     *      Specifically seek a sample inside batteryItems, so that it can be
     *      highlighted afterward.
     *
     *  @param {number} soundIndex
     *      The "identifier" of the SampleItem object to seek.
     *
     *  @returns {MixEnergetik.UI.SampleItem}
     *      The sample item corresponding to soundIndex if it is present inside
     *      the accumulator.
     *      null if not.
     */
    ChargeView.retrieveSampleItem = function retrieveSampleItem(soundIndex)
    {
        MixEnergetik.Utilitary.Check.native(soundIndex, "number",
            "MixEnergetik.UI.ChargeView.retrieveSampleItem()", "soundIndex");

        for (let i = 0; i < MixEnergetik.UI.ChargeView.batteryItems.length; i++)
        {
            let sampleItem = MixEnergetik.UI.ChargeView.batteryItems[i].sampleItem;

            if ((sampleItem !== null)
                && (sampleItem.soundData.soundIndex === soundIndex))
            {
                return sampleItem;
            }
        }

        /* Here nothing has been found */
        return null;
    };

    /**
     *  @function clearAccumulator
     *      Removes all samples from the accumulator.
     */
    ChargeView.clearAccumulator = function clearAccumulator()
    {
        for (let i = 0; i < ChargeView.batteryItems.length; i++)
        {
            ChargeView.batteryItems[i].clear();
        }
    };

    /**
     *  @function extendRecycle
     *      Extends the view (only pages with map).
     */
    ChargeView.extendRecycle = function extendRecycle()
    {
        ChargeView_recycleImage = ChargeView_extendedRecycleImage;
        if (!ChargeView_defaultRecycleImage.classList.contains("hidden"))
        {
            ChargeView_recycleImage.classList.remove("hidden");
        }

        ChargeView_defaultRecycleImage.classList.add("hidden");
        ChargeView_recyclorContainer.classList.add("extended");
    };

    /**
     *  @function retractRecycle
     *      Extends the view (only pages with map).
     */
    ChargeView.retractRecycle = function retractRecycle()
    {
        ChargeView_recycleImage = ChargeView_defaultRecycleImage;
        if (!ChargeView_extendedRecycleImage.classList.contains("hidden"))
        {
            ChargeView_recycleImage.classList.remove("hidden");
        }

        ChargeView_extendedRecycleImage.classList.add("hidden");
        ChargeView_recyclorContainer.classList.remove("extended");
    };

    /**
     *  @function extend
     *      Extends the view.
     */
    ChargeView.extend = function extend()
    {
        MixEnergetik.UI.ChargeView.extendRecycle();
        ChargeView_container.classList.add("extended");
        ChargeView_buttonShowHide.classList.add("extended");
    };

    /**
     *  @function retract
     *      Retracts the view.
     */
    ChargeView.retract = function retract()
    {
        MixEnergetik.UI.ChargeView.retractRecycle();
        ChargeView_container.classList.remove("extended");
        ChargeView_buttonShowHide.classList.remove("extended");
    };

    /**
     *  @function hide
     *      Hides the view.
     */
    ChargeView.hide = function hide()
    {
        ChargeView_container.classList.add("hidden");
        ChargeView_buttonShowHide.classList.add("hidden");
    };

    /**
     *  @function show
     *      Shows the view.
     */
    ChargeView.show = function show()
    {
        ChargeView_container.classList.remove("hidden");
        ChargeView_buttonShowHide.classList.remove("hidden");
    };

    /**
     *  @function createBin
     *      Create a bin item and add it to the recyclor container.
     */
    ChargeView.createBin = function createBin()
    {
        let binItem = new MixEnergetik.UI.BinItem();
        ChargeView_binItems.push(binItem);

        binItem.container.id = "RecyclorBin" + ChargeView_binItems.length;

        ChargeView_recyclorContainer.appendChild(binItem.container);
    };

    const ChargeView_CONTAINER_ID = "ChargePanel";
    const ChargeView_ACCUMULATOR_CONTAINER_ID = "ChargePanelAccumulator";
    const ChargeView_RECYCLOR_CONTAINER_ID = "Recyclor";
    const ChargeView_DEFAULT_RECYCLE_IMAGE_ID = "RecyclorImageDefault";
    const ChargeView_EXTENDED_RECYCLE_IMAGE_ID = "RecyclorImageExtended";
    const ChargeView_RECYCLOR_BUTTON_SHOW_HIDE_ID = "RecyclorButtonShowHide";
    const ChargeView_HELP_INDICATOR_ID = "ChargePanelHelpIndicator";
    const ChargeView_TUTORIAL_POPUP_CONTAINER_ID = "ChargeTutorialPopup";
    const ChargeView_TUTORIAL_VIDEO_ID = "ChargeTutorialVideo";

    const ChargeView_ACCUMULATOR_COUNT = 8;
    const ChargeView_DEFAULT_KEYS = ["w", "x", "c", "v", "b", "n", ",", ";"];

    let ChargeView_defaultRecycleImage = null;
    let ChargeView_extendedRecycleImage = null;
    let ChargeView_recycleImage = null;
    let ChargeView_container = null;
    let ChargeView_accumulatorContainer = null;
    let ChargeView_recyclorContainer = null;
    let ChargeView_buttonShowHide = null;
    let ChargeView_helpIndicator = null;
    let ChargeView_tutorialPopupContainer = null;
    let ChargeView_tutorialVideo = null;
    let ChargeView_binItems = [];

    /**
     *  @function buttonShowHideCallback
     *      Function called to make the recycle view appear or disappear.
     */
    let ChargeView_buttonShowHideCallback = function buttonShowHideCallback(event)
    {
        // TODO
        ChargeView_recycleImage.classList.toggle("hidden");
        ChargeView_recyclorContainer.classList.toggle("hidden");
    };

    return ChargeView;
})();
