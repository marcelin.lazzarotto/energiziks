/*
 *  File:   Exploration/script.js
 *  Author: Marcelin Lazzarotto
 *  Date:   2019-01-10
 */

"use strict";

/**
 *  @namespace MixEnergetik
 *      The project namespace, to avoid polluting global state.
 */
var MixEnergetik = MixEnergetik || {};

/**
 *  @namespace MixEnergetik.UI
 *      Subnamespace for UI related objects.
 */
MixEnergetik.UI = MixEnergetik.UI || {};

/**
 *  @module Exploration
 *      Module for the preview UI element. A look into the future.
 */
MixEnergetik.UI.Exploration = (function()
{
    let Exploration = {};

    /**
     *  @prototype MapArea
     *      Resulting objects are representation of clickable areas on the map.
     */
    let MapArea = function MapArea(id)
    {
        this.name = "exploration-" + id;
        this.url = MixEnergetik.MasterManager.PROJECT_ROOT
            + "/resources/videos/wander/" + this.name + ".mp4";
        this.container = document.getElementById("Exploration" + id);

        let self = this;

        this.container.addEventListener("click", function()
            {
                MixEnergetik.UI.VideoView.setVideoURL(self.url,
                    self.name);
                Exploration_close();
            }, false);
    };

    /**
     *  @function initialize
     *      Sets all buttons callback.
     */
    Exploration.initialize = function initialize()
    {
        /* Prep shit */
        Exploration_button = document.getElementById("ExplorationButton");
        Exploration_container = document.getElementById("ExplorationContainer");
        Exploration_content = document.getElementById("ExplorationContent");
        Exploration_map = document.getElementById("ExplorationMap");

        for (let i = 0; i < Exploration_IDS.length; i++)
        {
            Exploration_areas.push(new MapArea(Exploration_IDS[i]));
        }

        Exploration_close();

        Exploration_button.addEventListener("click", Exploration_toggle, false);

        /* Drag and move map */
        MixEnergetik.Utilitary.Helpers.makeElementSlideable(Exploration_content,
            Exploration_container);

        /* Hack into vidselv to hide thingies */
        {
            const VidSelV = MixEnergetik.UI.VideoSelectionView;

            let hideVideoViewItem = function hideVideoViewItem()
            {
                for (let i = 0; i < Exploration_areas.length; i++)
                {
                    let videoItem = VidSelV.videoItems[Exploration_areas[i].name];
                    if (videoItem)
                    {
                        videoItem.container.classList.add("explorationOnly");
                    }
                }
            };

            if (VidSelV.loaded)
            {
                /* Video view has loaded */
                hideVideoViewItem();
            }
            else
            {
                let callbackID = VidSelV.event.on("load", function()
                    {
                        VidSelV.event.off("load", callbackID);
                        hideVideoViewItem();
                    });
            }
        }
    };

    Exploration.extend = function extend()
    {
        Exploration_container.classList.add("extended");
    };

    Exploration.retract = function retract()
    {
        Exploration_container.classList.remove("extended");
    };

    /* Private */

    let Exploration_button = null;
    let Exploration_container = null;
    let Exploration_content = null;
    let Exploration_map = null;
    let Exploration_areas = [];

    const Exploration_IDS = ["Marseille",
        "Prado",
        "Cassis",
        "Mer0",
        "Mer1",
        "Mer2",
        "Mer3",
        "Mer4",
        "Mer5",
        "Mer6",
        "Mer7",
        "Mer8",
        "Mer9",
    ];

    /**
     *  @function toggle
     *      Toggle the exploration view.
     */
    let Exploration_toggle = function toggle()
    {
        Exploration_container.classList.toggle("hidden");
    };

    let Exploration_close = function close()
    {
        Exploration_container.classList.add("hidden");
    }

    return Exploration;
})();
