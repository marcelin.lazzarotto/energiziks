/*
 *  File:   DatePickerView.js
 *  Author: Marcelin Lazzarotto
 *  Date:   2018-05-16
 */

/**
 *  @namespace MixEnergetik
 *      The project namespace, to avoid polluting global state.
 */
var MixEnergetik = MixEnergetik || {};

/**
 *  @namespace MixEnergetik.UI
 *      Subnamespace for UI related objects.
 */
MixEnergetik.UI = MixEnergetik.UI || {};

/**
 *  @submodule DatePickerView
 *      Deals with the date picker so that we can change the date and hear
 *      something else.
 */
MixEnergetik.UI.DatePickerView = (function()
{
    let DatePickerView = {};

    const DatePickerView_CONTAINER_ID = "DateContainer";
    const DatePickerView_MAIN_BUTTON_ID = "DateButton";
    const DatePickerView_CANCEL_BUTTON_ID = "DateButtonCancel";
    const DatePickerView_DAY_PICKER_ID = "DateDayPicker";
    const DatePickerView_TIME_INDICATOR_ID = "DateTimeIndication";
    const DatePickerView_TIME_PICKER_ID = "DateTimePicker";

    let DatePickerView_container = null;
    let DatePickerView_mainButton = null;
    let DatePickerView_cancelButton = null;
    let DatePickerView_dayPicker = null;
    let DatePickerView_timeIndicator = null;
    let DatePickerView_timePicker = null;

    let DatePickerView_isExtended = false;

    /* TODO
     * UNSAFE FOR NOW, IF THE USER IS A BASTARD HE CAN CALL IN THE FUTURE */

    /**
     *  @function asTime
     *      Function to retrieve the hours and minutes from the slider value.
     *
     *  @param {number} value
     *      The value of time retrieved from the slider.
     *
     *  @returns {object}
     *      .hours: the hours
     *      .minutes: the minutes.
     */
    let DatePickerView_asTime = function asTime(value)
    {
        let hours = Math.floor(value);
        let minutes = (((value % 1) * 100) / 25) * 15;

        return {"hours": hours, "minutes": minutes};
    };

    /**
     *  @function asTimeString
     *      Short-hand function to retrieve the hh:mm time from the slider
     *      value.
     *
     *  @param {number} value
     *      The value of time retrieved from the slider.
     *
     *  @returns {string}
     *      The time indicated as a hh:mm format.
     */
    let DatePickerView_asTimeString = function asTimeString(value)
    {
        let time = DatePickerView_asTime(value);

        return MixEnergetik.DateManager.formatTime(time.hours, time.minutes);
    };

    /**
     *  @function asTimeValue
     *      Returns a number value from a time in hours and minutes.
     *      Mostly used for the slider.
     *
     *  @param {number} hours
     *      The hours.
     *  @param {number} minutes
     *      The minutes
     *
     *  @returns {number}
     *      A number for the slider corresponding to the time in hours/minutes.
     */
    let DatePickerView_asTimeValue = function asTimeValue(hours, minutes)
    {
        let roundedMinutes = MixEnergetik.DateManager.roundToHalf(minutes);
        let floatMinutes = (((roundedMinutes / 15) * 25) / 100);

        return (hours + floatMinutes);
    };

    /**
     *  @function completeTimeString
     *      Returns a complete time string with format "dd/mm/yyyy hh:mm".
     */
    let DatePickerView_completeTimeString = function completeTimeString()
    {
        let currentTime = MixEnergetik.DateManager.currentTime;
        let dateTime = MixEnergetik.DateManager.formatTime(currentTime.getHours(), currentTime.getMinutes());
        let response = "";

        {
            let day = currentTime.getDate();

            if (day < 10)
            {
                response += "0";
            }

            response += day;
        }

        response += "/";

        {
            let month = currentTime.getMonth() + 1;

            if (month < 10)
            {
                response += "0";
            }

            response += month;
        }

        response += "/";

        response += currentTime.getFullYear();
        response += " ";
        response += dateTime;

        return response;
    };

    DatePickerView_resetToCurrentTime = function resetToCurrentTime()
    {
        let currentTime = MixEnergetik.DateManager.currentTime;

        let hours = currentTime.getHours();
        let minutes = currentTime.getMinutes()
        let timeValue = DatePickerView_asTimeValue(hours, minutes);

        $(DatePickerView_dayPicker).datepicker("setDate", currentTime);

        DatePickerView_timePicker.value = timeValue;
        DatePickerView_timeIndicator.innerHTML = DatePickerView_asTimeString(timeValue);
        DatePickerView_mainButton.innerHTML = DatePickerView_completeTimeString();
    };

    /**
     *  @function retract
     *      Retract the view to a simple date button with indication.
     */
    DatePickerView_retract = function retract()
    {
        DatePickerView_container.classList.remove("extended");

        DatePickerView_isExtended = false;
    };


    /**
     *  @function mainButtonCallback
     *      Called when the users click on the date button.
     *      Actions depens on the state. If isExtended is false, then it extends
     *      the UI so that the user can select another date. Else it retract the
     *      UI and validate date and time.
     */
    DatePickerView_mainButtonCallback = function mainButtonCallback()
    {
        const mainButton = DatePickerView_mainButton;

        if (!DatePickerView_isExtended)
        {
            /* Extends */
            DatePickerView_container.classList.add("extended");
            DatePickerView_isExtended = !DatePickerView_isExtended;

            mainButton.innerHTML = "Valider";
        }
        else
        {
            /* Confirm & retract. */

            const DateM = MixEnergetik.DateManager;
            const GroupM = MixEnergetik.GroupManager;

            /* Retract */
            DatePickerView_retract();

            let newDate = $(DatePickerView_dayPicker).datepicker("getDate");

            let changeDate = function changeDate()
            {
                /* let newDate = $(DatePickerView_dayPicker).datepicker("getDate"); */
                let newTime = DatePickerView_asTime(DatePickerView_timePicker.value)
                newDate.setHours(newTime.hours);
                newDate.setMinutes(newTime.minutes);
                DateM.currentTime = newDate;

                mainButton.innerHTML = DatePickerView_completeTimeString();
            };

            if (GroupM.isActive())
            {
                let fetchCompleted = false;
                let groupCallbackId = 0;
                let dateCallbackId = 0;

                let update = function update()
                {
                    mainButton.classList.remove("waiting");

                    changeDate();

                    DateM.updateRegions(newDate);
                };

                let onFetchEnd = function onFetchEnd()
                {
                    DateM.event.off("load", dateCallbackId);

                    fetchCompleted = true;
                };

                let onFetchEndPlay = function onFetchEndPlay()
                {
                    DateM.event.off("load", dateCallbackId);

                    update();
                };

                let onGroupEnd = function onGroupEnd()
                {
                    if (!fetchCompleted)
                    {
                        return;
                    }

                    GroupM.event.off("end", groupCallbackId);

                    update();
                };

                let status = "";

                mainButton.innerHTML = DatePickerView_completeTimeString();

                status = DateM.fetchRegions(newDate);

                if (status === "LOCAL_FETCH")
                {
                    fetchCompleted = true;
                }
                else
                {
                    fetchCompleted = false;
                }

                if (GroupM.getActive().isEmpty())
                {
                    if (fetchCompleted)
                    {
                        update();
                    }
                    else
                    {
                        dateCallbackId = DateM.event.on("load", onFetchEndPlay);

                        mainButton.classList.add("waiting");
                    }
                }
                else
                {
                    if (!fetchCompleted)
                    {
                        dateCallbackId = DateM.event.on("load", onFetchEnd);
                    }

                    groupCallbackId = GroupM.event.on("end", onGroupEnd);

                    mainButton.classList.add("waiting");
                }
            }
            else
            {
                changeDate();
                DateM.fetchAndUpdateRegions(newDate);
            }
        }
    };

    /**
     *  @function cancelButtonCallback
     *      Cancels the time change when the button is clicked.
     */
    DatePickerView_cancelButtonCallback = function cancelButtonCallback(event)
    {
        if (!DatePickerView_isExtended)
        {
            /* ? */
            return;
        }

        DatePickerView_resetToCurrentTime();
        DatePickerView_retract();
    };

    /**
     *  @function sliderCallback
     *      Called when the user slide the slider.
     *      Changes the time of the day for the what happens with the energy.
     */
    DatePickerView_sliderCallback = function sliderCallback(event)
    {
        if (!DatePickerView_isExtended)
        {
            /* ? */
            return;
        }

        let timeValue = +DatePickerView_timePicker.value;

        DatePickerView_timeIndicator.innerHTML = DatePickerView_asTimeString(timeValue);
    };

    DatePickerView.initialize = function initialize()
    {
        DatePickerView_container = document.getElementById(DatePickerView_CONTAINER_ID);
        DatePickerView_mainButton = document.getElementById(DatePickerView_MAIN_BUTTON_ID);
        DatePickerView_cancelButton = document.getElementById(DatePickerView_CANCEL_BUTTON_ID);
        DatePickerView_dayPicker = document.getElementById(DatePickerView_DAY_PICKER_ID);
        DatePickerView_timeIndicator = document.getElementById(DatePickerView_TIME_INDICATOR_ID);
        DatePickerView_timePicker = document.getElementById(DatePickerView_TIME_PICKER_ID);

        $(DatePickerView_dayPicker).datepicker({
            dateFormat: "dd/mm/yy",
            minDate: new Date(2013, 0, 1),
            maxDate: 0});

        DatePickerView_resetToCurrentTime();

        DatePickerView_mainButton.addEventListener("click", DatePickerView_mainButtonCallback, false);
        DatePickerView_cancelButton.addEventListener("click", DatePickerView_cancelButtonCallback, false);
        DatePickerView_timePicker.addEventListener("input", DatePickerView_sliderCallback, false);
    };

    return DatePickerView;
})();
