/*
 *  File:   ApplicationControlView/script.js
 *  Author: Marcelin Lazzarotto
 *  Date:   2018-01-29
 */

"use strict";

/**
 *  @namespace MixEnergetik
 *      The project namespace, to avoid polluting global state.
 */
var MixEnergetik = MixEnergetik || {};

/**
 *  @namespace MixEnergetik.UI
 *      Subnamespace for UI related objects.
 */
MixEnergetik.UI = MixEnergetik.UI || {};

MixEnergetik.UI.ApplicationControlView = (function()
{
    let ApplicationControlView = {};

    /**
     *  @function initialize
     *      Sets all buttons callback.
     */
    ApplicationControlView.initialize = function initialize()
    {
        ApplicationControlView_container =
            document.getElementById(ApplicationControlView_CONTAINER_ID);
        ApplicationControlView_buttonReturnToMenu =
            document.getElementById(ApplicationControlView_RETURN_BUTTON_QUIT_ID);
        ApplicationControlView_cancelReturnButton =
            document.getElementById(ApplicationControlView_CANCEL_RETURN_BUTTON_ID);

        ApplicationControlView_buttonReturnToMenu.addEventListener("click",
            ApplicationControlView_showReturnConfirm, false);
        ApplicationControlView_cancelReturnButton.addEventListener("click",
            ApplicationControlView_hideReturnConfirm, false);
    };

    /**
     *  @function hide
     *      Hides the view.
     */
    ApplicationControlView.hide = function hide()
    {
        ApplicationControlView_container.classList.add("hidden");
    };

    /**
     *  @function show
     *      Shows the view.
     */
    ApplicationControlView.show = function show()
    {
        ApplicationControlView_container.classList.remove("hidden");
    };

    const ApplicationControlView_CONTAINER_ID = "ApplicationControl";
    const ApplicationControlView_RETURN_BUTTON_QUIT_ID = "ApplicationControlButtonReturnToMenu";
    const ApplicationControlView_CANCEL_RETURN_BUTTON_ID = "ApplicationControlCancelReturnButton";

    let ApplicationControlView_container = null;
    let ApplicationControlView_buttonReturnToMenu = null;
    let ApplicationControlView_cancelReturnButton = null;

    let ApplicationControlView_showReturnConfirm = function showReturnConfirm()
    {
        ApplicationControlView_container.classList.add("extended");
    };

    let ApplicationControlView_hideReturnConfirm = function hideReturnConfirm()
    {
        ApplicationControlView_container.classList.remove("extended");
    };

    return ApplicationControlView;
})();
