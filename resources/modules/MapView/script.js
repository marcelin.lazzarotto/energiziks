/*
 *  File:   MapView/script.js
 *  Author: Marcelin Lazzarotto
 *  Date:   2018-01-29
 */

"use strict";

/**
 *  @namespace MixEnergetik
 *      The project namespace, to avoid polluting global state.
 */
var MixEnergetik = MixEnergetik || {};

/**
 *  @namespace MixEnergetik.UI
 *      Subnamespace for UI related objects.
 */
MixEnergetik.UI = MixEnergetik.UI || {};

MixEnergetik.UI.MapView = (function()
{
    let MapView = {};

    /**
     *  @property {MixEnergetik.UI.GroupItem{}} regions
     *      Array of sample items present in the map.
     */
    MapView.regions = {};

    /**
     *  @function initialize
     *      Initialize the logic of the map view using the given regions.
     */
    MapView.initialize = function initialize()
    {
        MapView_container = document.getElementById(
            MapView_CONTAINER_ID);
        MapView_toggleExtensionButton = document.getElementById(
            MapView_TOGGLE_EXTENSION_BUTTON_ID);
        MapView_defaultBackground = document.getElementById(
            MapView_DEFAULT_BACKGROUND_ID);
        MapView_extendedBackground = document.getElementById(
            MapView_EXTENDED_BACKGROUND_ID);

        for (let i = 0; i < MixEnergetik.RegionManager.regionIds.length;
            i++)
        {
            /* Clarity */
            let regionId = MixEnergetik.RegionManager.regionIds[i];

            MapView.regions[regionId] = MapView_createRegion(regionId);
        }

        MapView_toggleExtensionButton.addEventListener("click",
            MapView_toggleExtensionButtonCallback, false);

        MapView_retractView();

        MapView_fixSVGDragAndDrop();
    };

    /**
     *  @function disable
     *      Disable all samples
     *      This is used when:
     *          - solos;
     *          - mixer is playing, in which case a function to enable all
     *          preset should be activated afterward
     */
    MapView.disable = function disable()
    {
        // // TODO
        // for (let i = 0; i < MapView.sampleItems.length; i++)
        // {
        //     /* clarity */
        //     MapView.sampleItems[i].disable();
        // }
    };

    /**
     *  @function enable
     *      Enable all items and presets.
     */
    MapView.enable = function enable()
    {
        // // TODO
        // for (let i = 0; i < MapView.sampleItems.length; i++)
        // {
        //     /* clarity */
        //     MapView.sampleItems[i].enable();
        // }
    };

    /**
     *  @function isRegion
     *      Checks if given GroupItem object is a region.
     *
     *  @param {MixEnergetik.UI.GroupItem} groupItem
     *      The item to test.
     *
     *  @returns {boolean}
     *      true if groupItem is a region (belongs to .regions object)
     *      false otherwise.
     */
    MapView.isRegion = function isRegion(groupItem)
    {
        return (MapView.getRegionItemId(groupItem) !== "");
    };

    /**
     *  @function getRegionItemId
     *      Returns the id of the given region item.
     *
     *  @param {MixEnergetik.UI.GroupItem} groupItem
     *
     *  @returns {string}
     *      The id of the region if groupItem is a region,
     *      an empty string otherwise.
     */
    MapView.getRegionItemId = function getRegionItemId(groupItem)
    {
        MixEnergetik.Utilitary.Check.object(groupItem,
            MixEnergetik.UI.GroupItem,
            "MixEnergetik.UI.MapView.getRegionItemId()",
            "groupItem", "MixEnergetik.UI.GroupItem");

        for (let key in MixEnergetik.UI.MapView.regions)
        {
            if (MixEnergetik.UI.MapView.regions.hasOwnProperty(key))
            {
                if (MixEnergetik.UI.MapView.regions[key] === groupItem)
                {
                    return key;
                }
            }
        }

        return "";
    }

    const MapView_CONTAINER_ID = "MapContainer";
    const MapView_TOGGLE_EXTENSION_BUTTON_ID = "MapToggleSequencerButton";
    const MapView_DEFAULT_BACKGROUND_ID = "DefaultBackground";
    const MapView_EXTENDED_BACKGROUND_ID = "ExtendedBackground";

    let MapView_container = null;
    let MapView_toggleExtensionButton = null;
    let MapView_defaultBackground = null;
    let MapView_extendedBackground = null;
    /**
     *  @property {HTMLDivElement objectmap} muters
     *      Muters for the specific region's sound. The map's keys are the
     *      region's id.
     */
    let MapView_muters = {};

    /**
     *  @property {string} display
     *      On pages with the map, the page has two states: low information and
     *      complete information (where the sequencer appears). This property
     *      keeps track of that.
     *      Possible values are:
     *      - "DISPLAY_DEFAULT": the normal case where a minimal number of
     *      things are shown.
     *      - "DISPLAY_EXTENDED": where the sequencer and additional
     *      information are shown.
     */
    let MapView_display = "DISPLAY_DEFAULT";

    /**
     *  @function MapView_createRegion
     *      Routine to create the regions' GroupItem objects.
     *
     *  @param {string} regionId
     *      The id of the region to seek container and create.
     *
     *  @throws {Error}
     *      Throws error if it is not possible to find the region's element on
     *      the open page.
     *
     *  @returns {GroupItem}
     *      The region newly created.
     */
    let MapView_createRegion = function createRegion(regionId)
    {
        if ((regionId === undefined) || (regionId === null)
            || (typeof regionId !== "string"))
        {
            throw new Error("[MixEnergetik.UI.MapView_createRegion()] the"
                + " regionId parameter must be provided and a string");
        }

        let regionContainer = document.getElementById(regionId);

        if (regionContainer === null)
        {
            throw new Error("[MixEnergetik.UI.MapView_createRegion()] the"
                + " regionId parameter must be a valid region identificator");
        }

        let newRegion = new MixEnergetik.UI.GroupItem();
        newRegion.setContainer(regionContainer, "CONTROL_DISABLE", "DRAG_ENABLE");
        newRegion.importData = new MixEnergetik.ImportData();
        newRegion.specialization.regionId = regionId;

        /* Create corresponding muter button */
        {
            let regionMuter = document.createElement("div");
            regionMuter.classList.add("audio");
            regionMuter.classList.add("button");
            regionMuter.id = "Muter" + regionId;

            MapView_container.appendChild(regionMuter);

            MapView_muters[regionId] = regionMuter;

            regionMuter.addEventListener("click", function()
                {
                    if (regionMuter.classList.contains("inactive"))
                    {
                        MapView.unmuteBaseSound(regionId);
                    }
                    else
                    {
                        MapView_muteBaseSound(regionId);
                    }
                }, false);
        }

        return newRegion;
    };

    let MapView_retractView = function retractView()
    {
        const EnerSelV = MixEnergetik.UI.EnergeticSelectionView;
        const VideoV = MixEnergetik.UI.VideoView;
        const ChargeV = MixEnergetik.UI.ChargeView;
        const SeqV = MixEnergetik.UI.SequencerView;

        MapView_container.classList.remove("extended");
        MapView_defaultBackground.classList.remove("hidden");
        MapView_extendedBackground.classList.add("hidden");

        EnerSelV && EnerSelV.retractLevel();

        ChargeV && ChargeV.retractRecycle();

        VideoV && VideoV.retract();

        SeqV.hide();

        MapView_display = "DISPLAY_DEFAULT";
    };

    let MapView_toggleExtensionButtonCallback = function toggleExtensionButtonCallback()
    {
        if (MapView_display === "DISPLAY_DEFAULT")
        {
            /* Extend view */
            const EnerSelV = MixEnergetik.UI.EnergeticSelectionView;
            const VideoV = MixEnergetik.UI.VideoView;
            const ChargeV = MixEnergetik.UI.ChargeView;
            const SeqV = MixEnergetik.UI.SequencerView;

            /* Extends */
            MapView_container.classList.add("extended");
            MapView_defaultBackground.classList.add("hidden");
            MapView_extendedBackground.classList.remove("hidden");

            EnerSelV && EnerSelV.extendLevel();

            ChargeV && ChargeV.extendRecycle();

            VideoV && VideoV.extend();

            SeqV.show();

            MapView_display = "DISPLAY_EXTENDED";
        }
        else /* if (MapView_display === "DISPLAY_EXTENDED") */
        {
            /* Retract */
            MapView_retractView();
        }

    };

    /**
     *  @function fixSVGDragAndDrop
     *      Actual function to fix non-standard SVG drag and drop.
     *      Resort to good ol' mouses down, move and up events.
     */
    let MapView_fixSVGDragAndDrop = function fixSVGDragAndDrop()
    {
        /* Drag check */
        let isDropPossible = function isDropPossible()
        {
            return ((dragged !== null)
                && (target !== null)
                && (target instanceof MixEnergetik.UI.GroupItem)
                && (target.isEmpty())
                && (!MixEnergetik.UI.MapView.isRegion(target)));
        };

        /* X and Y are clientX and clientY */
        let positionDummy = function positionDummy(x, y)
        {
            draggedDummy.style.left = (window.scrollX + x - DUMMY_SIZE / 2.0) + "px";
            draggedDummy.style.top  = (window.scrollY + y - DUMMY_SIZE / 2.0) + "px";
        };

        /* Seek target in sequencer */
        let seekTarget = function seekTarget()
        {
            let sequences = MixEnergetik.UI.SequencerView.sequences;

            for (var i = 0; i < sequences.length; i++)
            {
                if (sequences[i].source === targetElement)
                {
                    return sequences[i];
                }
            }

            return null;
        };

        /* Drag variables */
        let initialPositionX = null;
        let initialPositionY = null;
        let dragging = false;
        let dragged = null;
        let targetElement = null;
        let target  = null;

        const DUMMY_SIZE = 64;
        let draggedDummy = document.createElement("div");
        draggedDummy.style.position = "absolute";
        draggedDummy.style.width =  DUMMY_SIZE + "px";
        draggedDummy.style.height = DUMMY_SIZE + "px";
        draggedDummy.style.backgroundColor = "#00aa0088";
        draggedDummy.style.pointerEvents = "none";
        draggedDummy.style.display = "none";
        document.getElementById("Application").appendChild(draggedDummy);

        for (let i = 0; i < MixEnergetik.RegionManager.regionIds.length; i++)
        {
            /* Clarity */
            let regionId = MixEnergetik.RegionManager.regionIds[i];
            let regionItem = MapView.regions[regionId];

            /* Drag first indicator */
            regionItem.source.addEventListener("mousedown",
                function(event)
                {
                    /* Record a maybe drag starting */
                    initialPositionX = window.scrollX + event.clientX;
                    initialPositionY = window.scrollY + event.clientY;
                    dragged = regionItem;
                }, false);
        }

        /* Can "trigger" dragstart, dragenter, dragleave & drag */
        document.addEventListener("mousemove",
            function(event)
            {
                if ((initialPositionX === null)
                    || (initialPositionY === null))
                {
                    /* Another way to say mouse is up. */
                    return;
                }

                if (!dragging)
                {
                    /* Test drag */
                    const MINIMUM_DRAG_DISTANCE = 20; /* px */
                    let currentPositionX = window.scrollX + event.clientX;
                    let currentPositionY = window.scrollY + event.clientY;
                    let distX = initialPositionX - currentPositionX;
                    let distY = initialPositionY - currentPositionY;

                    let distance = Math.sqrt(distX * distX + distY * distY);

                    if (distance > MINIMUM_DRAG_DISTANCE)
                    {
                        dragging = true;

                        /* Small user response */
                        draggedDummy.style.display = "block";
                        positionDummy(event.clientX, event.clientY);
                    }
                }
                else
                {
                    /* We are dragging right now, track target */
                    if (targetElement !== event.target)
                    {
                        /* Dragleave */
                        if (isDropPossible())
                        {
                            target.dropTargetOff();
                        }

                        targetElement = event.target;
                        target = seekTarget();

                        if (isDropPossible())
                        {
                            target.dropTargetOn();
                        }
                    }

                    /* Drag */
                    positionDummy(event.clientX, event.clientY);
                }

            }, false);

        /* Can "trigger" drag end and drop */
        document.addEventListener("mouseup",
            function(event)
            {
                if (isDropPossible())
                {
                    /* Drop */
                    target.addGroup(dragged.groupData);

                    target.play();

                    target.specialization.origin = dragged;
                }

                /* Drag end */
                if (dragging)
                {
                }

                /* Anyway reset */
                initialPositionX = null;
                initialPositionY = null;
                dragging = false;
                dragged = null;
                targetElement = null;
                target  = null;
                draggedDummy.style.display = "none";
            }, false);
    };

    /**
     *  @function muteBaseSound
     *      Mutes the region base sound.
     *
     *  @param {string} regionId
     *      The id of the region we are trying to mute.
     */
    let MapView_muteBaseSound = function muteBaseSound(regionId)
    {
        MixEnergetik.Utilitary.Check.native(regionId, "string",
            "MixEnergetik.MapView_muteBaseSound()", "regionId");

        MapView_muters[regionId].classList.add("inactive");
        MixEnergetik.UI.MapView.regions[regionId].muteBaseSound();
    };

    /**
     *  @function unmuteBaseSound
     *      Unmutes the region base sound.
     *
     *  @param {string} regionId
     *      The id of the region we are trying to mute.
     */
    MapView.unmuteBaseSound = function MapView_unmuteBaseSound(regionId)
    {
        MixEnergetik.Utilitary.Check.native(regionId, "string",
            "MixEnergetik.MapView_muteBaseSound()", "regionId");

        let regionElement = MapView_muters[regionId];

        /* "Public" function, so safety here */
        if ((regionElement === undefined)
            || (regionElement === null))
        {
            return;
        }
        
        regionElement.classList.remove("inactive");
        MixEnergetik.UI.MapView.regions[regionId].unmuteBaseSound();
    };

    return MapView;
})();
