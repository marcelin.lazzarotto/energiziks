/*
 *  File:   EnergeticSelectionView.js
 *  Author: Marcelin Lazzarotto
 *  Date:   2018-01-29
 */

"use strict";

/**
 *  @namespace MixEnergetik
 *      The project namespace, to avoid polluting global state.
 */
var MixEnergetik = MixEnergetik || {};

/**
 *  @namespace MixEnergetik.UI
 *      Subnamespace for UI related objects.
 */
MixEnergetik.UI = MixEnergetik.UI || {};

/**
 *  @submodule EnergeticSelectionView
 *      Deals with the energetic selection between production and consumption.
 *      Switch samples inside regions accordingly to selected type and selected
 *      music style.
 */
MixEnergetik.UI.EnergeticSelectionView = (function()
{
    let EnergeticSelectionView = {};

    /**
     *  @property {string} status
     *      Indicates the status of the view (production, consumption).
     */
    EnergeticSelectionView.status = "consumption";
    /**
     *  @property {string} style
     *      Keep track of the current selected style.
     */
    EnergeticSelectionView.style = "";

    /**
     *  @function addStyle
     *      Add a style for consumption and production.
     *
     *  @param {string} styleName
     *      The name of the style to add.
     *      Is used in RegionManager too, to identify the needed samples.
     */
    EnergeticSelectionView.addStyle = function addStyle(styleName)
    {
        MixEnergetik.Utilitary.Check.native(styleName, "string",
            "MixEnergetik.UI.EnergeticSelectionView.addStyle()", "styleName");

        let option = document.createElement("option");

        option.value = styleName;
        option.innerHTML = styleName;

        EnergeticSelectionView_energeticSelector.appendChild(option);

        if (MixEnergetik.UI.EnergeticSelectionView.style === "")
        {
            MixEnergetik.UI.EnergeticSelectionView.style = styleName;
        }
    };

    /**
     *  @function extendLevel
     *      Changes the display to the advanced ("extended") version.
     */
    EnergeticSelectionView.extendLevel = function extendLevel()
    {
        /* Hide */
        EnergeticSelectionView_defaultLevelConsoImg.classList.add("hidden");
        EnergeticSelectionView_defaultLevelProdImg.classList.add("hidden");

        /* Show */
        if (EnergeticSelectionView_display === "SHOWN")
        {
            if (MixEnergetik.UI.EnergeticSelectionView.status === "consumption")
            {
                EnergeticSelectionView_extendedLevelConsoImg.classList.remove("hidden");
            }
            else /* if (MixEnergetik.UI.EnergeticSelectionView.status = "production") */
            {
                EnergeticSelectionView_extendedLevelProdImg.classList.remove("hidden");
            }
        }

        /* Change */
        EnergeticSelectionView_levelHidderContainer.classList.add("extended");

        EnergeticSelectionView_levelConsoImg = EnergeticSelectionView_extendedLevelConsoImg;
        EnergeticSelectionView_levelProdImg = EnergeticSelectionView_extendedLevelProdImg;
    };

    /**
     *  @function retractLevel
     *      Changes the display to the default ("retracted") version.
     */
    EnergeticSelectionView.retractLevel = function retractLevel()
    {
        /* Hide */
        EnergeticSelectionView_extendedLevelConsoImg.classList.add("hidden");
        EnergeticSelectionView_extendedLevelProdImg.classList.add("hidden");

        /* Show */
        if (EnergeticSelectionView_display === "SHOWN")
        {
            if (MixEnergetik.UI.EnergeticSelectionView.status === "consumption")
            {
                EnergeticSelectionView_defaultLevelConsoImg.classList.remove("hidden");
            }
            else /* if (MixEnergetik.UI.EnergeticSelectionView.status = "production") */
            {
                EnergeticSelectionView_defaultLevelProdImg.classList.remove("hidden");
            }
        }

        /* Change */
        EnergeticSelectionView_levelHidderContainer.classList.remove("extended");

        EnergeticSelectionView_levelConsoImg = EnergeticSelectionView_defaultLevelConsoImg;
        EnergeticSelectionView_levelProdImg = EnergeticSelectionView_defaultLevelProdImg;
    };

    /**
     *  @function initialize
     *      Initialize the view.
     *      Also uses given parameter containing sample paths as an object, in
     *      order to retrieve and keep samples.
     */
    EnergeticSelectionView.initialize = function initialize()
    {
        EnergeticSelectionView_buttons["production"] = document.getElementById(EnergeticSelectionView_BUTTON_PRODUCTION_ID);
        EnergeticSelectionView_buttons["consumption"] = document.getElementById(EnergeticSelectionView_BUTTON_CONSUMPTION_ID);
        EnergeticSelectionView_energeticSelector = document.getElementById(EnergeticSelectionView_ENERGETIC_SELECTOR_ID);

        /* Default to consumption */
        EnergeticSelectionView.status = "consumption";
        EnergeticSelectionView_buttons["consumption"].classList.add("selected");

        EnergeticSelectionView_buttons["production"].addEventListener("click",
            EnergeticSelectionView_clickCallback, false);
        EnergeticSelectionView_buttons["consumption"].addEventListener("click",
            EnergeticSelectionView_clickCallback, false);
        EnergeticSelectionView_energeticSelector.addEventListener("change",
            EnergeticSelectionView_selectionCallback, false);

        EnergeticSelectionView_defaultLevelConsoImg = document.getElementById(
            EnergeticSelectionView_DEFAULT_LEVEL_CONSO_ID);
        EnergeticSelectionView_defaultLevelProdImg = document.getElementById(
            EnergeticSelectionView_DEFAULT_LEVEL_PROD_ID);
        EnergeticSelectionView_extendedLevelConsoImg = document.getElementById(
            EnergeticSelectionView_EXTENDED_LEVEL_CONSO_ID);
        EnergeticSelectionView_extendedLevelProdImg = document.getElementById(
            EnergeticSelectionView_EXTENDED_LEVEL_PROD_ID);
        EnergeticSelectionView_levelHidderContainer = document.getElementById(
            EnergeticSelectionView_LEVEL_HIDDER_CONTAINER_ID);
        EnergeticSelectionView_levelHidder = document.getElementById(
            EnergeticSelectionView_LEVEL_HIDDER_ID);
        EnergeticSelectionView_levelProdImg = EnergeticSelectionView_defaultLevelConsoImg;
        EnergeticSelectionView_levelConsoImg = EnergeticSelectionView_defaultLevelProdImg;
        EnergeticSelectionView_hideLevel();

        const GroupM = MixEnergetik.GroupManager;

        GroupM.event.on("play", EnergeticSelectionView_showLevel);
        GroupM.event.on("end", EnergeticSelectionView_hideLevel);
        GroupM.event.on("stop", EnergeticSelectionView_hideLevel);
    };

    const EnergeticSelectionView_DEFAULT_LEVEL_CONSO_ID = "EnergeticContainerDefaultConsoLevel";
    const EnergeticSelectionView_DEFAULT_LEVEL_PROD_ID = "EnergeticContainerDefaultProdLevel";
    const EnergeticSelectionView_EXTENDED_LEVEL_CONSO_ID = "EnergeticContainerExtendedConsoLevel";
    const EnergeticSelectionView_EXTENDED_LEVEL_PROD_ID = "EnergeticContainerExtendedProdLevel";
    const EnergeticSelectionView_LEVEL_HIDDER_CONTAINER_ID = "EnergeticContainerLevelHidderContainer";
    const EnergeticSelectionView_LEVEL_HIDDER_ID = "EnergeticContainerLevelHidder";

    const EnergeticSelectionView_BUTTON_PRODUCTION_ID = "EnergeticButtonProduction";
    const EnergeticSelectionView_BUTTON_CONSUMPTION_ID = "EnergeticButtonConsumption";
    const EnergeticSelectionView_ENERGETIC_SELECTOR_ID = "EnergeticSelector";

    /**
     *  @property {HTMLElement[]} buttons
     *      The button corresponding to each actions. Pressing one deactivates
     *      the others.
     */
    let EnergeticSelectionView_buttons = [];

    let EnergeticSelectionView_defaultLevelConsoImg = null;
    let EnergeticSelectionView_defaultLevelProdImg  = null;
    let EnergeticSelectionView_extendedLevelConsoImg = null;
    let EnergeticSelectionView_extendedLevelProdImg  = null;
    let EnergeticSelectionView_levelProdImg = null;
    let EnergeticSelectionView_levelConsoImg = null;
    let EnergeticSelectionView_levelHidderContainer = null

    /**
     *  @property {HTMLElement} levelHidder
     *      This hides the level of conso/prod.
     */
    let EnergeticSelectionView_levelHidder = null;
    /**
     *  @property {HTMLElement} energeticSelector
     *      The select menu allows the user to choose the style of the samples.
     */
    let EnergeticSelectionView_energeticSelector = null;

    /**
     *  @property {string} display
     *      Keep track of the current (level) display.
     *      Possible values are: "SHOWN", "HIDDEN" (default).
     */
    let EnergeticSelectionView_display = "HIDDEN";

    let EnergeticSelectionView_maxHeight = 0;

    /**
     *  @function clickCallback
     *      Callback called after a click on our two buttons.
     */
    let EnergeticSelectionView_clickCallback = function clickCallback(event)
    {
        let status = "";
        let prodAction = "remove";
        let consoAction = "remove";

        if (event.target === EnergeticSelectionView_buttons["production"])
        {
            if (EnergeticSelectionView.status === "production")
            {
                return;
            }

            status = "production";
            prodAction = "add";
        }
        else if (event.target === EnergeticSelectionView_buttons["consumption"])
        {
            if (EnergeticSelectionView.status === "consumption")
            {
                return;
            }

            status = "consumption";
            consoAction = "add";
        }

        if (MixEnergetik.GroupManager.isActive())
        {
            let callbackId = 0;

            let onGroupEnd = function()
            {
                EnergeticSelectionView_buttons[status].classList.remove("waiting");

                EnergeticSelectionView.status = status;
                EnergeticSelectionView_buttons["production"].classList[prodAction]("selected");
                EnergeticSelectionView_buttons["consumption"].classList[consoAction]("selected");

                let currentPlaying = MixEnergetik.GroupManager.getActive();

                MixEnergetik.GroupManager.deactivate();
                MixEnergetik.GroupManager.push(currentPlaying);

                MixEnergetik.GroupManager.event.off("end", callbackId);

                MixEnergetik.RegionManager.updateAllSamples("TRADE_UNCHANGED");
            };

            callbackId = MixEnergetik.GroupManager.event.on("end", onGroupEnd);

            EnergeticSelectionView_buttons["production"].classList.remove("waiting");
            EnergeticSelectionView_buttons["consumption"].classList.remove("waiting");
            EnergeticSelectionView_buttons[status].classList.add("waiting");
        }
        else
        {
            EnergeticSelectionView.status = status;
            EnergeticSelectionView_buttons["production"].classList[prodAction]("selected");
            EnergeticSelectionView_buttons["consumption"].classList[consoAction]("selected");

            MixEnergetik.RegionManager.updateAllSamples("TRADE_UNCHANGED");
        }
    };

    /**
     *  @function selectionCallback
     *      Called after a style was chosen.
     */
    let EnergeticSelectionView_selectionCallback = function selectionCallback(event)
    {
        MixEnergetik.UI.EnergeticSelectionView.style = event.target.value;

        MixEnergetik.RegionManager.updateAllSamples();
    };

    /**
     *  @function renderLevel
     *      Renders the level for the calling frame.
     *      Simply changes the hidder rectangle size.
     */
    let EnergeticSelectionView_renderLevel = function renderLevel()
    {
        if (EnergeticSelectionView_display === "HIDDEN")
        {
            /* Do nothing if we are hidden */
            return;
        }

        const soloItem = MixEnergetik.SoloManager.soloItem;

        let maxHeight = null;

        if (soloItem !== null)
        {
            if (soloItem instanceof MixEnergetik.UI.GroupItem)
            {
                const regionId = soloItem.specialization.regionId;

                if (regionId)
                {
                    const RegionM = MixEnergetik.RegionManager;

                    let level = 0;

                    if (EnergeticSelectionView.status === "production")
                    {
                        level = RegionM.regions[regionId].getProductionLevel();

                        EnergeticSelectionView_levelProdImg.classList.remove("hidden");
                    }
                    else if (EnergeticSelectionView.status === "consumption")
                    {
                        level = RegionM.regions[regionId].getConsumptionLevel();

                        EnergeticSelectionView_levelConsoImg.classList.remove("hidden");
                    }
                    else
                    {
                        throw new Error("Incorrect state");
                    }

                    let ratio = level / RegionM.energetic.maxLevel;
                    let height = (ratio * 100);
                    maxHeight = height;
                }
                else
                {
                    window.requestAnimationFrame(EnergeticSelectionView_renderLevel);
                    return;
                }
            }
            else
            {
                window.requestAnimationFrame(EnergeticSelectionView_renderLevel);
                return;
            }
        }

        maxHeight = maxHeight || EnergeticSelectionView_maxHeight;

        let height = 0;

        /* Compute this frame's height */
        {
            const audioData = MixEnergetik.AudioManager.getCurrentAudioData();

            let threshold = 0; /* Because maximum is too high */

            /* Wet finger 33% value */
            threshold = (33 / 100) * audioData.maximum;
            height = maxHeight * (audioData.average / threshold);

            /* Constrain this */
            if (height > maxHeight)
            {
                height = maxHeight;
            }
        }

        EnergeticSelectionView_levelHidder.style.height = height + "%";

        window.requestAnimationFrame(EnergeticSelectionView_renderLevel);
    };

    /**
     *  @function showLevel
     *      Shows the production/consumption level.
     */
    let EnergeticSelectionView_showLevel = function showLevel()
    {
        const MapV = MixEnergetik.UI.MapView;
        const RegionM = MixEnergetik.RegionManager;

        const groupItem = MixEnergetik.GroupManager.getActive();

        if (!MapV.isRegion(groupItem))
        {
            return;
        }

        const regionId = groupItem.specialization.regionId;
        let level = 0;

        if (EnergeticSelectionView.status === "production")
        {
            level = RegionM.regions[regionId].getProductionLevel();

            EnergeticSelectionView_levelProdImg.classList.remove("hidden");
        }
        else if (EnergeticSelectionView.status === "consumption")
        {
            level = RegionM.regions[regionId].getConsumptionLevel();

            EnergeticSelectionView_levelConsoImg.classList.remove("hidden");
        }
        else
        {
            throw new Error("Incorrect state");
        }

        let ratio = level / RegionM.energetic.maxLevel;
        let height = (ratio * 100);
        EnergeticSelectionView_maxHeight = height;
        EnergeticSelectionView_display = "SHOWN";
        window.requestAnimationFrame(EnergeticSelectionView_renderLevel);

        EnergeticSelectionView_levelHidder.classList.remove("hidden");
    };

    /**
     *  @function hideLevel
     *      Hides the production/consumption level.
     */
    let EnergeticSelectionView_hideLevel = function hideLevel()
    {
        EnergeticSelectionView_display = "HIDDEN";
        EnergeticSelectionView_levelConsoImg.classList.add("hidden");
        EnergeticSelectionView_levelProdImg.classList.add("hidden");
        EnergeticSelectionView_levelHidder.classList.add("hidden");
    };

    return EnergeticSelectionView;
})();
