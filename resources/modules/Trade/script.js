/*
 *  File:   TradeView.js
 *  Author: Marcelin Lazzarotto
 *  Date:   2018-05-06
 */

"use strict";

/**
 *  @namespace MixEnergetik
 *      The project namespace, to avoid polluting global state.
 */
var MixEnergetik = MixEnergetik || {};
/**
 *  @namespace MixEnergetik.UI
 *      Subnamespace for UI related objects.
 */
MixEnergetik.UI = MixEnergetik.UI || {};

/**
 *  @module TradeView
 *      The manager dealing with the video.
 */
MixEnergetik.UI.TradeView = (function()
{
    let TradeView = {};

    TradeView.initialize = function initialize()
    {
        TradeView_container = document.getElementById(TradeView_CONTAINER_ID);
        TradeView_importWrapper = document.getElementById(TradeView_IMPORT_WRAPPER_ID);
        TradeView_exportWrapper = document.getElementById(TradeView_EXPORT_WRAPPER_ID);
        TradeView_importContainer = document.getElementById(TradeView_IMPORT_CONTAINER_ID);
        TradeView_exportContainer = document.getElementById(TradeView_EXPORT_CONTAINER_ID);
        TradeView_importLevelContainer = document.getElementById(TradeView_IMPORT_LEVEL_ID);
        TradeView_exportLevelContainer = document.getElementById(TradeView_EXPORT_LEVEL_ID);
        TradeView_importImage = document.getElementById(TradeView_IMPORT_IMAGE_ID);
        TradeView_exportImage = document.getElementById(TradeView_EXPORT_IMAGE_ID);

        TradeView_keyControl.setKeys(
            MixEnergetik.Utilitary.KeyControl.DEFAULT_KEYS);

        MixEnergetik.UI.TradeView.hide();

        /* Consider the import container as a droppable target. */
        TradeView_importContainer.addEventListener("dragover", function(event)
        {
            /* /shrug */
            MixEnergetik.DragAndDropManager.targetItem = TradeView;
        }, false);

        /* TEMPORARY SOLUTION */
        TradeView_exportContainer.addEventListener("dragover", function(event)
        {
            /* /shrug */
            MixEnergetik.DragAndDropManager.targetItem = TradeView;
        }, false);
    };

    /**
     *  @function resetImport
     *      After a clear, put import back in the container.
     */
    TradeView.resetImport = function resetImport()
    {
        const TradeM = MixEnergetik.TradeManager;
        const AudioM = MixEnergetik.AudioManager;
        const SndBldr = MixEnergetik.Utilitary.SoundBuilder;

        const style = TradeM.getStyle();

        /* Reset */
        TradeView_clearImport();

        /* Delayed download */
        if ((TradeM.tradeItems[style].import.length === 0)
            && (TradeM.tradeItems[style].importPaths.length !== 0))
        {
            const paths = TradeM.tradeItems[style].importPaths;

            for (let i = 0; i < paths.length; i++)
            {
                let path = paths[i];
                let fileName = path.substring(path.lastIndexOf("/") + 1);
                let soundWrapper = SndBldr.createSoundWrapper(paths[i]);
                let soundIndex = AudioM.provideSound(soundWrapper);
                let sampleItem = new MixEnergetik.UI.SampleItem(soundIndex,
                        fileName);

                TradeM.tradeItems[style].import.push(sampleItem);
            }
        }

        /* Refill */
        const importItems = TradeM.tradeItems[style].import;

        for (let i = 0; i < importItems.length; i++)
        {
            let sampleItem = importItems[i];

            sampleItem.container.classList.add("import");

            TradeView_importItems.push(sampleItem);

            TradeView_importContainer.appendChild(sampleItem.container);
        }
    };

    /**
     *  @function importSample
     *      Imports an export sample into
     */
    TradeView.importSample = function importSample(exportItem)
    {
        MixEnergetik.Utilitary.Check.object(exportItem,
            MixEnergetik.UI.SampleItem,
            "MixEnergetik.UI.TradeView.importSample()", "exportItem",
            "MixEnergetik.UI.SampleItem");

        const TradeV = MixEnergetik.UI.TradeView;
        const regionId = MixEnergetik.TradeManager.regionItem.specialization.regionId;

        if (!TradeV.isExportSample(exportItem))
        {
            return;
        }

        let importedItem = new MixEnergetik.UI.SampleItem(exportItem.soundData.soundIndex,
            exportItem.name);
        importedItem.soundData = exportItem.soundData.clone();
        importedItem.container.classList.add("import");
        importedItem.container.classList.add("export");

        TradeView_importContainer.appendChild(importedItem.container);

        MixEnergetik.TradeManager.exportedItems[regionId] = (MixEnergetik.TradeManager.exportedItems[regionId] || []);
        MixEnergetik.TradeManager.exportedItems[regionId].push(importedItem);
        TradeView_importItems.push(importedItem);

        MixEnergetik.UI.TradeView.updateExportLevel();
    };

    /**
     *  @function revokeSample
     *      Revoke a previously imported sample a sample into TODO
     */
    TradeView.revokeSample = function revokeSample(exportedItem)
    {
        MixEnergetik.Utilitary.Check.object(exportedItem,
            MixEnergetik.UI.SampleItem,
            "MixEnergetik.UI.TradeView.revokeSample()", "exportedItem",
            "MixEnergetik.UI.SampleItem");

        const exportedItemMap = MixEnergetik.TradeManager.exportedItems;

        let getExportedId = function getExportedId()
        {
            for (let regionId in exportedItemMap)
            {
                const exportedItems = exportedItemMap[regionId];

                for (let i = 0; i < exportedItems.length; i++)
                {
                    if (exportedItem === exportedItems[i])
                    {
                        return regionId;
                    }
                }
            }

            return null;
        }

        let regionId = getExportedId();

        if (!regionId)
        {
            return;
        }

        const regionItems = MixEnergetik.UI.MapView.regions;

        /* First, clear the exported sample from regions that imported it */
        for (let regionId in regionItems)
        {
            let regionItem = regionItems[regionId];

            regionItem.removeImportSample(exportedItem.soundData.soundIndex);
        }

        const indexExp = exportedItemMap[regionId].indexOf(exportedItem);
        const indexImp = TradeView_importItems.indexOf(exportedItem);

        TradeView_importContainer.removeChild(exportedItem.container);

        exportedItemMap[regionId].splice(indexExp, 1);

        TradeView_importItems.splice(indexImp, 1);

        MixEnergetik.UI.TradeView.updateExportLevel();
    };

    /**
     *  @function updateImportLevel
     *      Update the level indication. Color in green when the importation
     *      has reached max level.
     */
    TradeView.updateImportLevel = function updateImportLevel()
    {
        const regionItem = MixEnergetik.TradeManager.regionItem;
        const regionId = regionItem.specialization.regionId;
        const regionData = MixEnergetik.RegionManager.regions[regionId];

        if (regionData.getTradeType() !== "TYPE_IMPORT")
        {
            return;
        }

        TradeView_importLevelContainer.classList.remove("decimal");

        const importCount = MixEnergetik.TradeManager.getImportLevel(regionItem);
        const capacity = regionData.getTradeLevel();

        if (importCount === capacity)
        {
            TradeView_importLevelContainer.classList.add("maximumPower");
            TradeView_importLevelContainer.innerHTML = "Limite import";
        }
        else
        {
            const countString = ("" + importCount + "/" + capacity + "");
            TradeView_importLevelContainer.innerHTML = countString;
            TradeView_importLevelContainer.classList.remove("maximumPower");

            if (importCount % 1 !== 0)
            {
                /* Decimal */
                TradeView_importLevelContainer.classList.add("decimal");
            }
        }
    };

    /**
     *  @function updateImport
     *      Update the view according to new information.
     */
    TradeView.updateImport = function updateImport()
    {
        MixEnergetik.UI.TradeView.updateImportLevel();

        TradeView_showImport();
    };

    /**
     *  @function updateExportLevel
     *      Update the level indication. Color in green when the importation
     *      has reached max level.
     */
    TradeView.updateExportLevel = function updateExportLevel()
    {
        const regionItem = MixEnergetik.TradeManager.regionItem;
        const regionId = regionItem.specialization.regionId;
        const regionData = MixEnergetik.RegionManager.regions[regionId];
        const capacity = -regionData.getTradeLevel();
        const exportedItems = MixEnergetik.TradeManager.exportedItems[regionId];
        let exportCount = 0;

        if (exportedItems)
        {
            exportCount = exportedItems.length;
        }

        if (exportCount === capacity)
        {
            TradeView_exportLevelContainer.classList.add("maximumPower");
            TradeView_exportLevelContainer.innerHTML = "Limite export";
        }
        else
        {
            const countString = ("" + exportCount + "/" + capacity + "");
            TradeView_exportLevelContainer.innerHTML = countString;
            TradeView_exportLevelContainer.classList.remove("maximumPower");
        }
    };

    TradeView.updateExport = function updateExport()
    {
        const TradeM = MixEnergetik.TradeManager;
        const AudioM = MixEnergetik.AudioManager;
        const SndBldr = MixEnergetik.Utilitary.SoundBuilder;
        const style = TradeM.getStyle();
        const regionId = TradeM.regionItem.specialization.regionId;

        /* Clean first */
        TradeView_clearExport();

        if ((TradeM.tradeItems[style].export[regionId].length === 0)
            && (TradeM.tradeItems[style].exportPaths[regionId].length !== 0))
        {
            const paths = TradeM.tradeItems[style].exportPaths[regionId];

            for (let i = 0; i < paths.length; i++)
            {
                let path = paths[i];
                let fileName = path.substring(path.lastIndexOf("/") + 1);
                let soundWrapper = SndBldr.createSoundWrapper(path);
                let soundIndex = AudioM.provideSound(soundWrapper);

                let sampleItem = new MixEnergetik.UI.SampleItem(soundIndex,
                    fileName, "NATURE_EXPORT");

                TradeM.tradeItems[style].export[regionId].push(sampleItem);
            }
        }

        const exportMap = TradeM.tradeItems[style].export;
        const exportItems = exportMap[regionId] || [];

        /* Update then */
        for (let i = 0; i < exportItems.length; i++)
        {
            let exportItem = exportItems[i];

            exportItem.container.classList.add("export");

            TradeView_keyControl.addItem(exportItem);

            TradeView_exportItems.push(exportItem);

            TradeView_exportContainer.appendChild(exportItem.container);
        }

        MixEnergetik.UI.TradeView.updateExportLevel();

        TradeView_showExport();
    };

    /**
     *  @function use
     *      Calls the use function on the indicated items.
     *
     *  @param {number[]} soundIndices
     *      The indices of the sound used. Those are used to retrieve the items
     *      to call their function.
     */
    TradeView.use = function use(soundIndices)
    {
        MixEnergetik.Utilitary.Check.array(soundIndices,
            "MixEnergetik.UI.TradeView.use()", "soundIndices");

        for (let i = 0; i < TradeView_importItems.length; i++)
        {
            const sampleItem = TradeView_importItems[i];
            const soundIndex = sampleItem.soundData.soundIndex;

            if (soundIndices.indexOf(soundIndex) !== -1)
            {
                sampleItem.use();
            }
        }
    };

    /**
     *  @function unuse
     *      Mark all the sample items as unused.
     */
    TradeView.unuse = function unuse()
    {
        for (let i = 0; i < TradeView_importItems.length; i++)
        {
            TradeView_importItems[i].unuse();
        }
    };

    /**
     *  @function clear
     *      Cleans the trade view, preparing for DOM changes.
     */
    TradeView.clear = function clear()
    {
        TradeView_clearImport();
        TradeView_clearExport();
    };

    /**
     *  @function show
     *      Makes the view appear.
     */
    TradeView.show = function show()
    {
        TradeView_container.classList.remove("hidden");
    };

    /**
     *  @function hide
     *      Clears and hide the import view.
     */
    TradeView.hide = function()
    {
        TradeView_importLevelContainer.classList.add("hidden");
        TradeView_exportLevelContainer.classList.add("hidden");
        TradeView_importWrapper.classList.add("hidden");
        TradeView_exportWrapper.classList.add("hidden");
        TradeView_exportImage.classList.add("hidden");
        TradeView_importImage.classList.add("hidden");
        TradeView_container.classList.add("hidden");
    };

    /**
     *  @function isImportSample
     *      Checks if given SampleItem object actually corresponds to an import
     *      sample.
     *
     *  @param {MixEnergetik.UI.SampleItem} sampleItem
     *      The SampleItem object to test.
     *
     *  @return {bool}
     *      true if sampleItem is inside the list of items,
     *      false otherwise.
     */
    TradeView.isImportSample = function isImportSample(sampleItem)
    {
        MixEnergetik.Utilitary.Check.object(sampleItem,
            MixEnergetik.UI.SampleItem,
            "MixEnergetik.UI.TradeView.isImportSample",
            "sampleItem",
            "MixEnergetik.UI.SampleItem");

        return (TradeView_importItems.indexOf(sampleItem) !== -1);
    };

    /**
     *  @function isExportSample
     *      Checks if given SampleItem object actually corresponds to an export
     *      sample.
     *
     *  @param {MixEnergetik.UI.SampleItem} sampleItem
     *      The SampleItem object to test.
     *
     *  @return {bool}
     *      true if sampleItem is inside the list of items,
     *      false otherwise.
     */
    TradeView.isExportSample = function isExportSample(sampleItem)
    {
        MixEnergetik.Utilitary.Check.object(sampleItem,
            MixEnergetik.UI.SampleItem,
            "MixEnergetik.UI.TradeView.isExportSample",
            "sampleItem",
            "MixEnergetik.UI.SampleItem");

        return (TradeView_exportItems.indexOf(sampleItem) !== -1);
    };

    /**
     *  @function isExportedSample
     *      Checks if given SampleItem object actually corresponds to an
     *      exported sample.
     *
     *  @param {MixEnergetik.UI.SampleItem} sampleItem
     *      The SampleItem object to test.
     *
     *  @return {bool}
     *      true if sampleItem is inside the list of items,
     *      false otherwise.
     */
    TradeView.isExportedSample = function isExportedSample(sampleItem)
    {
        MixEnergetik.Utilitary.Check.object(sampleItem,
            MixEnergetik.UI.SampleItem,
            "MixEnergetik.UI.TradeView.isExportedSample",
            "sampleItem",
            "MixEnergetik.UI.SampleItem");

        const exportedItemMap = MixEnergetik.TradeManager.exportedItems;

        for (let regionId in exportedItemMap)
        {
            const exportedItems = exportedItemMap[regionId];

            for (let i = 0; i < exportedItems.length; i++)
            {
                if (sampleItem === exportedItems[i])
                {
                    return true;
                }
            }
        }

        return false;
    };

    /**
     *  @function dropTargetOn
     *      Activate user feedback on drop target.
     */
    TradeView.dropTargetOn = function dropTargetOn()
    {
        TradeView_importContainer.classList.add("dropTarget");
    };

    /**
     *  @function dropTargetOff
     *      Remove user feedback on drop target.
     */
    TradeView.dropTargetOff = function dropTargetOff()
    {
        TradeView_importContainer.classList.remove("dropTarget");
    };

    const TradeView_CONTAINER_ID = "TradeContainer";
    const TradeView_IMPORT_WRAPPER_ID = "TradeImportWrapper";
    const TradeView_EXPORT_WRAPPER_ID = "TradeExportWrapper";
    const TradeView_IMPORT_CONTAINER_ID = "TradeImportContainer";
    const TradeView_EXPORT_CONTAINER_ID = "TradeExportContainer";
    const TradeView_IMPORT_LEVEL_ID = "TradeImportLevel";
    const TradeView_EXPORT_LEVEL_ID = "TradeExportLevel";
    const TradeView_IMPORT_IMAGE_ID = "TradeImportImage";
    const TradeView_EXPORT_IMAGE_ID = "TradeExportImage";

    let TradeView_container = null;
    let TradeView_importWrapper = null;
    let TradeView_importContainer = null;
    let TradeView_exportWrapper = null;
    let TradeView_exportContainer = null;
    let TradeView_importLevelContainer = null;
    let TradeView_exportLevelContainer = null;
    let TradeView_exportImage = null; /* images for import export */
    let TradeView_importImage = null; /* images for import export */

    let TradeView_importItems = [];
    let TradeView_exportItems = [];

    let TradeView_keyControl = new MixEnergetik.Utilitary.KeyControl();

    /**
     *  @function showImport
     *      Show only import window.
     */
    let TradeView_showImport = function showImport()
    {
        TradeView_importWrapper.classList.remove("importing");

        TradeView_importLevelContainer.classList.remove("hidden");
        TradeView_importWrapper.classList.remove("hidden");
        TradeView_importImage.classList.remove("hidden");
        TradeView_exportLevelContainer.classList.add("hidden");
        TradeView_exportWrapper.classList.add("hidden");
        TradeView_exportImage.classList.add("hidden");
    };

    /**
     *  @funciton showExport
     *      Show only export window.
     */
    let TradeView_showExport = function showExport()
    {
        TradeView_importWrapper.classList.add("importing");

        TradeView_importLevelContainer.classList.add("hidden");
        TradeView_importWrapper.classList.remove("hidden");
        TradeView_importImage.classList.add("hidden");
        TradeView_exportLevelContainer.classList.remove("hidden");
        TradeView_exportWrapper.classList.remove("hidden");
        TradeView_exportImage.classList.remove("hidden");
    };

    /**
     *  @function clearImport
     *      Clears the import window.
     */
    let TradeView_clearImport = function clearImport()
    {
        for (let i = 0; i < TradeView_importItems.length; i++)
        {
            TradeView_importContainer.removeChild(TradeView_importItems[i].container);
        }

        TradeView_importItems = [];
    };

    /**
     *  @function clearExport
     *      Clears the export window.
     */
    let TradeView_clearExport = function clearExport()
    {
        for (let i = 0; i < TradeView_exportItems.length; i++)
        {
            let exportItem = TradeView_exportItems[i];

            TradeView_exportContainer.removeChild(exportItem.container);

            exportItem.stop();
        }

        TradeView_keyControl.clear();
        TradeView_exportItems = [];
    };

    return TradeView;
})();
