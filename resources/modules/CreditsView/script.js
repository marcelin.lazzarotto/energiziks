/*
 *  File:   CreditsView/script.js
 *  Author: Marcelin Lazzarotto
 *  Date:   2018-08-15
 */

"use strict";

/**
 *  @namespace MixEnergetik
 *      The project namespace, to avoid polluting global state.
 */
var MixEnergetik = MixEnergetik || {};

/**
 *  @namespace MixEnergetik.UI
 *      Subnamespace for UI related objects.
 */
MixEnergetik.UI = MixEnergetik.UI || {};

MixEnergetik.UI.CreditsView = (function()
{
    let CreditsView = {};

    /**
     *  @function initialize
     *      Sets all buttons callback.
     */
    CreditsView.initialize = function initialize()
    {
        CreditsView_container = document.getElementById("CreditsView");
        CreditsView_buttonShow = document.getElementById("CreditsViewButtonShow");
        CreditsView_buttonHide = document.getElementById("CreditsViewButtonClose");

        CreditsView_buttonShow.addEventListener("click", CreditsView_show, false);
        CreditsView_buttonHide.addEventListener("click", CreditsView_hide, false);

        if (MixEnergetik.LoaderManager)
        {
            let hideFId = 0;

            let hideF = function()
            {
                CreditsView_container.classList.add("hidden");
                MixEnergetik.LoaderManager.event.off("complete", hideFId);
            };

            hideFId = MixEnergetik.LoaderManager.event.on("complete", hideF);
        }
        else
        {
            CreditsView_container.classList.add("hidden");
        }
    };

    /**
     *  @function showButton
     *      Hides the credit button.
     */
    CreditsView.showButton = function showButton()
    {
        CreditsView_buttonShow.classList.remove("hidden");
    };

    /**
     *  @function hideButton
     *      Hides the credit button.
     */
    CreditsView.hideButton = function hideButton()
    {
        CreditsView_buttonShow.classList.add("hidden");
    };

    /* Private */

    let CreditsView_container = null;
    let CreditsView_buttonShow = null;
    let CreditsView_buttonHide = null;

    /**
     *  @function show
     *      Show the credits.
     */
    let CreditsView_show = function show()
    {
        CreditsView_container.classList.remove("hidden");
    };

    /**
     *  @function hide
     *      Hide the credits.
     */
    let CreditsView_hide = function hide()
    {
        CreditsView_container.classList.add("hidden");
    };

    return CreditsView;
})();
