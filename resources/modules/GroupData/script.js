/*
 *  File:   GroupDataView.js
 *  Author: Marcelin Lazzarotto
 *  Date:   2018-05-06
 */

"use strict";

/**
 *  @namespace MixEnergetik
 *      The project namespace, to avoid polluting global state.
 */
var MixEnergetik = MixEnergetik || {};
/**
 *  @namespace MixEnergetik.UI
 *      Subnamespace for UI related objects.
 */
MixEnergetik.UI = MixEnergetik.UI || {};

/**
 *  @module GroupDataView
 *      The manager dealing with the region elements.
 */
MixEnergetik.UI.GroupDataView = (function()
{
    let GroupDataView = {};

    /**
     *  @property {string} mode
     *      To keep track of the used mode.
     *      Values are: "MODE_STANDARD" (default) and "MODE_RENEWABLE".
     */
    GroupDataView.mode = "MODE_STANDARD";

    /**
     *  @property {MixEnergetik.UI.SampleItem objectmap} elementItems
     *      Map of sample items so that theres an easy access to the sample
     *      themselves. Identified by the sound index.
     */
    GroupDataView.elementItems = {};

    /**
     *  @function initialize
     */
    GroupDataView.initialize = function initialize()
    {
        GroupDataView_container = document.getElementById(
            GroupDataView_CONTAINER_ID);
        GroupDataView_popupRenewableMode = document.getElementById(
            GroupDataView_POPUP_RENEWABLE_MODE_ID);
        GroupDataView_buttonRenewableMode = document.getElementById(
            GroupDataView_BUTTON_RENEWABLE_MODE_ID);
        GroupDataView_extractionGraph = document.getElementById(
            GroupDataView_EXTRACTION_GRAPH_ID);
        GroupDataView_extractionBars["air"] = document.getElementById(
            GroupDataView_EXTRACTION_BAR_BASE_ID + "Air");
        GroupDataView_extractionBars["earth"] = document.getElementById(
            GroupDataView_EXTRACTION_BAR_BASE_ID + "Earth");
        GroupDataView_extractionBars["fire"] = document.getElementById(
            GroupDataView_EXTRACTION_BAR_BASE_ID + "Fire");
        GroupDataView_extractionBars["water"] = document.getElementById(
            GroupDataView_EXTRACTION_BAR_BASE_ID + "Water");
        GroupDataView_elementSamplesContainer = document.getElementById(
            GroupDataView_ELEMENT_SAMPLES_CONTAINER_ID);

        GroupDataView_buttonRenewableMode.addEventListener("click",
            function(event)
            {
                GroupDataView_toggleRenewableMode();
            }, false);
        GroupDataView_buttonRenewableMode.addEventListener("mouseenter",
            function(event)
            {
                GroupDataView_popupRenewableMode.classList.remove("hidden");
            }, false);
        GroupDataView_buttonRenewableMode.addEventListener("mousemove",
            function(event)
            {
                const parentOffsetX = GroupDataView_container.offsetLeft;
                const parentOffsetY = GroupDataView_container.offsetTop;
                const mouseX = event.pageX;
                const mouseY = event.pageY;
                const width = GroupDataView_popupRenewableMode.offsetWidth;
                const height = GroupDataView_popupRenewableMode.offsetHeight;
                const offsetX = -(width / 2.0);
                const offsetY = -(height + 15.0);

                const MIN_X = -(2.0 * parentOffsetX / 3.0);

                let x = mouseX + offsetX - parentOffsetX;
                let y = mouseY + offsetY - parentOffsetY;

                if (x < MIN_X)
                {
                    x = MIN_X;
                }

                GroupDataView_popupRenewableMode.style.left = x + "px";
                GroupDataView_popupRenewableMode.style.top = y + "px";
            }, false);
        GroupDataView_buttonRenewableMode.addEventListener("mouseleave",
            function(event)
            {
                GroupDataView_popupRenewableMode.classList.add("hidden");
            }, false);

        GroupDataView_extractionGraph.classList.add("hidden");
        GroupDataView_elementSamplesContainer.classList.add("hidden");
        GroupDataView_popupRenewableMode.classList.add("hidden");
    };

    /**
     *  @function prepareSampleItems
     *      Build the sample items corresponding to each element sound.
     *      Should be called before using any sound sample.
     */
    GroupDataView.prepareSampleItems = function prepareSampleItems()
    {
        const SndBldr = MixEnergetik.Utilitary.SoundBuilder;
        const AudioM = MixEnergetik.AudioManager;
        const RegionM = MixEnergetik.RegionManager;
        const MEUISampleItem = MixEnergetik.UI.SampleItem;

        for (let elementId in RegionM.elements)
        {
            /* Prepare silence for non-valid levels */
            let elementSilenceItem = new MEUISampleItem(AudioM.silence.soundIndex,
                "Silence", "NATURE_ELEMENT");
            elementSilenceItem.container.classList.add(elementId);
            GroupDataView_fillSamples[elementId] = elementSilenceItem

            let descriptor = RegionM.elements[elementId];

            for (let level in descriptor.scale)
            {
                if ((!descriptor.scale[level].soundIndex)
                    && (!!descriptor.scale[level].path))
                {
                    const path = descriptor.scale[level].path;
                    const soundWrapper = SndBldr.createSoundWrapper(path);
                    const soundIndex = AudioM.provideSound(soundWrapper);

                    descriptor.scale[level].soundIndex = soundIndex;
                }

                let soundIndex = descriptor.scale[level].soundIndex;

                let sampleItem = new MEUISampleItem(soundIndex, elementId,
                    "NATURE_ELEMENT");
                sampleItem.container.classList.add(elementId);

                GroupDataView.elementItems[soundIndex] = sampleItem;
            }
        }
    };

    /**
     *  @function show
     *      Show the group data view about the given group item.
     *
     *  @param {MixEnergetik.UI.GroupItem} groupItem
     *      The GroupItem object of which we want to see the data.
     */
    GroupDataView.show = function show(groupItem)
    {
        MixEnergetik.Utilitary.Check.object(groupItem,
            MixEnergetik.UI.GroupItem, "MixEnergetik.GroupDataView.show",
            "groupItem", "MixEnergetik.UI.GroupItem");

        const AudioM = MixEnergetik.AudioManager;
        const RegionM = MixEnergetik.RegionManager;

        const regionId = groupItem.specialization.regionId;

        if (regionId)
        {
            const GrpDtV = MixEnergetik.UI.GroupDataView;

            /* Show region info if possible */
            let region = RegionM.regions[regionId];

            /* Send corresponding samples to the region's own group of samples */
            for (let elementId in RegionM.elements)
            {
                let descriptor = RegionM.elements[elementId];
                let power = region.production[descriptor.association];
                let level = RegionM.levelFromScale(power, descriptor.scale);
                let sampleItem = null;

                /* Associate elements */
                if ((level !== -1) && descriptor.scale[level])
                {
                    let soundIndex = descriptor.scale[level].soundIndex;

                    sampleItem = GrpDtV.elementItems[soundIndex];
                }
                else
                {
                    /* Default to silence */
                    sampleItem = GroupDataView_fillSamples[elementId];
                }

                GroupDataView_extractionSamples[elementId] = sampleItem;
                GroupDataView_elementSamplesContainer.appendChild(sampleItem.container);

                if (level === -1)
                {
                    GroupDataView_maxHeights[elementId] = 0;
                }
                else
                {
                    const MIN_HEIGHT = GroupDataView_EXTRACTION_BARS_MINIMAL_HEIGHT;

                    let height = (((level) / (descriptor.maxLevel)) * 100);

                    height = (height < MIN_HEIGHT) ? MIN_HEIGHT : height;

                    GroupDataView_maxHeights[elementId] = height;
                }
            }

            GroupDataView_display = "SHOWN";

            window.requestAnimationFrame(GroupDataView_renderLevels);

            GroupDataView_extractionGraph.classList.remove("hidden");
            GroupDataView_elementSamplesContainer.classList.remove("hidden");
        }
    };

    /**
     *  @function hide
     *      Hide the view.
     */
    GroupDataView.hide = function hide()
    {
        GroupDataView_display = "HIDDEN";
        GroupDataView_extractionGraph.classList.add("hidden");
        GroupDataView_elementSamplesContainer.classList.add("hidden");

        for (let elementId in MixEnergetik.RegionManager.elements)
        {
            let sampleItem = GroupDataView_extractionSamples[elementId];

            if (sampleItem)
            {
                GroupDataView_elementSamplesContainer.removeChild(sampleItem.container);
            }
        }
    };

    /**
     *  @function getElementSampleId
     *      Retrieves the id of the given SampleItem.
     *
     *  @param {MixEnergetik.UI.SampleItem} sampleItem
     *      The SampleItem object to check for.
     *
     *  @returns {string}
     *      The identifier if sampleItem belongs to extractionSamples objectmap;
     *      An empty string otherwise.
     */
    GroupDataView.getElementSampleId = function getElementSampleId(sampleItem)
    {
        MixEnergetik.Utilitary.Check.object(sampleItem,
            MixEnergetik.UI.SampleItem,
            "MixEnergetik.UI.GroupDataView.getElementSampleId()", "sampleItem",
            "MixEnergetik.UI.SampleItem");

        for (let elementId in GroupDataView_extractionSamples)
        {
            if (GroupDataView_extractionSamples[elementId] === sampleItem)
            {
                return elementId;
            }
        }

        return "";
    };

    const GroupDataView_CONTAINER_ID = "GroupData";
    const GroupDataView_POPUP_RENEWABLE_MODE_ID = "GroupDataPopupRenewableMode";
    const GroupDataView_BUTTON_RENEWABLE_MODE_ID = "GroupDataButtonRenewableMode";
    const GroupDataView_EXTRACTION_GRAPH_ID = "ExtractionGraph";
    const GroupDataView_EXTRACTION_BAR_BASE_ID = "ExtractionBar";
    const GroupDataView_ELEMENT_SAMPLES_CONTAINER_ID = "GroupDataElementSamples";

    const GroupDataView_EXTRACTION_BARS_MINIMAL_HEIGHT = 5.0;

    let GroupDataView_container = null;
    let GroupDataView_popupRenewableMode = null;
    let GroupDataView_buttonRenewableMode = null;
    let GroupDataView_extractionGraph = null;
    let GroupDataView_elementSamplesContainer = null;
    let GroupDataView_extractionBars = {};
    let GroupDataView_extractionSamples = {};
    let GroupDataView_fillSamples = {};

    /**
     *  @property {object} maxHeights
     *      The maximum height for the levels.
     */
    let GroupDataView_maxHeights = {};

    /**
     *  @property {string} display
     *      Keeps track of the display.
     *      Possible values are: "SHOWN", "HIDDEN" (default).
     */
    let GroupDataView_display = "HIDDEN";

    /**
     *  @function renderLevels
     *      Renders the level for the calling frame.
     *      Simply changes the hidder rectangle size.
     */
    let GroupDataView_renderLevels = function renderLevels()
    {
        if (GroupDataView_display === "HIDDEN")
        {
            /* Do nothing if we are hidden */
            return;
        }

        const soloItem = MixEnergetik.SoloManager.soloItem;

        let maxHeights = null;

        if (soloItem !== null)
        {
            if (soloItem instanceof MixEnergetik.UI.GroupItem)
            {
                const regionId = soloItem.specialization.regionId;

                if (regionId)
                {
                    const RegionM = MixEnergetik.RegionManager;

                    maxHeights = {};

                    let region = RegionM.regions[regionId];

                    for (let elementId in RegionM.elements)
                    {
                        let descriptor = RegionM.elements[elementId];
                        let power = region.production[descriptor.association];
                        let level = RegionM.levelFromScale(power, descriptor.scale);

                        if (level === -1)
                        {
                            maxHeights[elementId] = 0;
                        }
                        else
                        {
                            const MIN_HEIGHT = GroupDataView_EXTRACTION_BARS_MINIMAL_HEIGHT;

                            let height = (((level) / (descriptor.maxLevel)) * 100);

                            height = (height < MIN_HEIGHT) ? MIN_HEIGHT : height;

                            maxHeights[elementId] = height;
                        }
                    }
                }
                else
                {
                    window.requestAnimationFrame(GroupDataView_renderLevels);
                    return;
                }
            }
            else
            {
                window.requestAnimationFrame(GroupDataView_renderLevels);
                return;
            }
        }

        maxHeights = maxHeights || GroupDataView_maxHeights;

        for (let elementId in maxHeights)
        {
            if ((elementId === "earth")
                && (MixEnergetik.UI.GroupDataView.mode === "MODE_RENEWABLE"))
            {
                GroupDataView_extractionBars[elementId].style.height = "0%";
                continue;
            }

            let height = 0;

            /* Compute this frame's height */
            {
                const audioData = MixEnergetik.AudioManager.getCurrentAudioData();
                const maxHeight = maxHeights[elementId];

                let threshold = 0; /* Because maximum is too high */

                /* Wet finger 33% value */
                threshold = (50 / 100) * audioData.maximum;
                height = maxHeight * (audioData.average / threshold);

                /* Constrain this */
                if (height > maxHeight)
                {
                    height = maxHeight;
                }
            }

            GroupDataView_extractionBars[elementId].style.height = height + "%";
        }

        window.requestAnimationFrame(GroupDataView_renderLevels);
    };

    /**
     *  @function toggleRenewableMode
     *      Change the mode.
     */
    let GroupDataView_toggleRenewableMode = function toggleRenewableMode()
    {
        const RegionM = MixEnergetik.RegionManager;
        const MapV = MixEnergetik.UI.MapView;
        const regionIds = RegionM.regionIds;
        const regionDatas = RegionM.regions;
        const regionItems = MapV.regions;
        const descriptor = RegionM.elements.earth;

        let getSoundIndex = function getSoundIndex(regionId)
        {
            const regionData = regionDatas[regionId];
            const nuclearPower = regionData.production.nuclear;
            const level = RegionM.levelFromScale(nuclearPower,
                descriptor.scale);

            if (level === -1)
            {
                return -1;
            }

            return descriptor.scale[level].soundIndex;
        }

        if (MixEnergetik.UI.GroupDataView.mode === "MODE_STANDARD")
        {
            /* Remove nuclear samples from regions */
            for (let i = 0; i < regionIds.length; i++)
            {
                const regionId = regionIds[i];

                let soundIndex = getSoundIndex(regionId);

                if (soundIndex === -1)
                {
                    continue;
                }

                regionItems[regionId].removeSound(soundIndex);
            }

            GroupDataView_buttonRenewableMode.classList.add("on");

            MixEnergetik.UI.GroupDataView.mode = "MODE_RENEWABLE";
        }
        else /* if (MixEnergetik.UI.GroupDataView.mode === "MODE_RENEWABLE") */
        {
            /* Readd nuclear samples to regions */
            for (let i = 0; i < regionIds.length; i++)
            {
                const regionId = regionIds[i];

                let soundIndex = getSoundIndex(regionId);

                if (soundIndex === -1)
                {
                    continue;
                }

                let soundData = new MixEnergetik.SoundData(soundIndex);
                regionItems[regionId].addSound(soundData);
            }

            GroupDataView_buttonRenewableMode.classList.remove("on");

            MixEnergetik.UI.GroupDataView.mode = "MODE_STANDARD";
        }
    };

    return GroupDataView;
})();
