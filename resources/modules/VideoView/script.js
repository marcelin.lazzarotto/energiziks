/*
 *  File:   VideoView.js
 *  Author: Marcelin Lazzarotto
 *  Date:   2018-04-12
 */

"use strict";

/**
 *  @namespace MixEnergetik
 *      The project namespace, to avoid polluting global state.
 */
var MixEnergetik = MixEnergetik || {};
/**
 *  @namespace MixEnergetik.UI
 *      Subnamespace for UI related objects.
 */
MixEnergetik.UI = MixEnergetik.UI || {};

/**
 *  @module VideoView
 *      The manager dealing with the video.
 */
MixEnergetik.UI.VideoView = (function()
{
    let VideoView = {};

    /**
     *  @property {HTMLVideoElement} videoElement
     *      The video element allowing us to manipulate video.
     */
    VideoView.videoElement = null;

    VideoView.initialize = function initialize()
    {
        VideoView_container = document.getElementById("VideoView");
        MixEnergetik.UI.VideoView.videoElement = document.getElementById("VideoViewMedia");
        VideoView_audioButton = document.getElementById("VideoViewAudioButton");
        VideoView_audioIcon = document.getElementById("VideoViewAudioIcon");
        VideoView_fullScreenButton = document.getElementById("VideoViewFullScreen");

        if (MixEnergetik.UI.MapView)
        {
            VideoView_audioButton.classList.add("hidden");
        }
        else
        {
            VideoView_fullScreenButton.addEventListener("click",
                VideoView_toggleFullScreen, false);
        }

        VideoView_audioButton.addEventListener("click", VideoView_toggleAudio,
            false);

        MixEnergetik.UI.VideoView.videoElement.volume = 1;

        if (MixEnergetik.UI.Wander)
        {
            MixEnergetik.UI.VideoView.videoElement.addEventListener('loadedmetadata',
                function()
                {
                    const duration = MixEnergetik.UI.VideoView.videoElement.duration;
                    const sampleDuration = MixEnergetik.Utilitary.Configuration.Audio.sampleDuration;
                    let numberOfSequences = Math.floor(duration / sampleDuration);

                    MixEnergetik.UI.SequencerView.setNumberOfSequences(numberOfSequences);
                }, false);
        }
    };

    /**
     *  @function setVideoURL
     *      Sets the video inside the video view to be with the given url and
     *      of given name.
     *
     *  @property {string} url
     *      The url of the video to play.
     *  @property {string} name
     *      Name of the video, to have an hint when move the mouse on top of the
     *      element.
     */
    VideoView.setVideoURL = function setVideoURL(url, name)
    {
        MixEnergetik.Utilitary.Check.native(url, "string",
            "MixEnergetik.UI.VideoView.setVideoURL()", "url");
        MixEnergetik.Utilitary.Check.native(name, "string",
            "MixEnergetik.UI.VideoView.setVideoURL()", "name");

        if (url !== VideoView_URL)
        {
            /* Change only if it wasn't the same. */

            MixEnergetik.UI.VideoView.videoElement.src = url;
            MixEnergetik.UI.VideoView.videoElement.title = name;

            MixEnergetik.UI.VideoView.videoElement.load();

            VideoView_URL = url;

            if (MixEnergetik.UI.MapView)
            {
                /* In case MapView is defined, the video is a small region
                * element loop */
            }
            else if (MixEnergetik.UI.VideoSelectionView)
            {
                /*
                 *  TODO: don't do that here, this should be put somewhere else.
                 *  -MLazzarotto, 2019-04-07
                 */
                const VidSelV = MixEnergetik.UI.VideoSelectionView;
                const videoItem = VidSelV.videoItems[name];

                /* We are in /energiziks/wander/ and it is a video with
                 * samples */
                /* Reset things */
                MixEnergetik.UI.SequencerView.clear();
                MixEnergetik.UI.PresetPanelView.clear();
                MixEnergetik.UI.SampleListView.clear();

                /* Set the samples (might be delayed) */
                {
                    const SmplLstV = MixEnergetik.UI.SampleListView;
                    let callbackSId = 0;
                    let callbackS = function()
                    {
                        videoItem.event.off("sampleload", callbackSId);

                        SmplLstV.setSampleItems(videoItem.sampleItems);
                    };

                    if (videoItem.sampleItems.length < videoItem.sampleDatas.length)
                    {
                        callbackSId = videoItem.event.on("sampleload", callbackS);

                        videoItem.loadSamples();
                    }
                    else
                    {
                        SmplLstV.setSampleItems(videoItem.sampleItems);
                    }
                }

                /* Set tags */
                {
                    const TagSelV = MixEnergetik.UI.TagSelectionView;

                    let callbackTId = 0;
                    let callbackT = function()
                    {
                        TagSelV.setRelatedTags(videoItem.tags);
                        videoItem.event.off("tagload", callbackTId);
                    };

                    if (videoItem.status === "STATUS_LOADING_TAGS")
                    {
                        callbackTId = videoItem.event.on("tagload", callbackT);
                    }
                    else
                    {
                        TagSelV.setRelatedTags(videoItem.tags);
                    }
                }
            }

            VideoView_name = name;
        }
    };

    /**
     *  @function hide
     *      Hide the video view.
     */
    VideoView.hide = function hide()
    {
        VideoView_container.classList.add("hidden");
    };

    /**
     *  @function show
     *      Unhide the video view.
     */
    VideoView.show = function show()
    {
        VideoView_container.classList.remove("hidden");
    };

    /**
     *  @function retract
     *      Retracts the video view, for default display (extraction/).
     */
    VideoView.retract = function retract()
    {
        VideoView_container.classList.remove("extended");
    };

    /**
     *  @function extend
     *      Extends the video view, for advanced display (extraction/).
     */
    VideoView.extend = function extend()
    {
        VideoView_container.classList.add("extended");
    };

    /**
     *  @function getVideoURL
     *      Returns the current video url.
     *
     *  @returns {string}
     *      The current video URL.
     */
    VideoView.getVideoURL = function getVideoURL()
    {
        return VideoView_URL;
    };
    /**
     *  @function getVideoName
     *      Returns the current video name.
     *
     *  @returns {string}
     *      The current video name.
     */
    VideoView.getVideoName = function getVideoName()
    {
        return VideoView_name;
    };

    /* Private things */
    let VideoView_URL = "";
    let VideoView_name = "";
    let VideoView_container = null;
    let VideoView_audioButton = null;
    let VideoView_audioIcon = null;
    let VideoView_fullScreenButton = null;

    /*
     *  Possible values:
     *      - VideoView_DISPLAY_NORMAL
     -      - VideoView_DISPLAY_FULLSCREEN
     */
    let VideoView_display = "VideoView_DISPLAY_NORMAL";

    let VideoView_toggleAudio = function toggleMute()
    {
        let video = MixEnergetik.UI.VideoView.videoElement;

        if (video.volume === 0)
        {
            video.volume = 1;
            VideoView_audioIcon.src = MixEnergetik.MasterManager.PROJECT_ROOT
                + "/resources/default/Speaker_Icon.svg";
        }
        else /* if (video.volume === 1) */
        {
            video.volume = 0;
            VideoView_audioIcon.src = MixEnergetik.MasterManager.PROJECT_ROOT
                + "/resources/default/Mute_Icon.svg";
        }
    };

    /**
     *  @function toggleFullScreen
     *      Action when the full screen button is clicked.
     */
    let VideoView_toggleFullScreen = function toggleFullScreen()
    {
        /* For now */
        if (!MixEnergetik.UI.SampleListView)
        {
            return;
        }

        if (VideoView_display === "VideoView_DISPLAY_FULLSCREEN")
        {
            VideoView_container.classList.remove("fullscreen");

            VideoView_display = "VideoView_DISPLAY_NORMAL";

            MixEnergetik.UI.ApplicationControlView.show();
            MixEnergetik.UI.SequencerView.show();
            MixEnergetik.UI.VideoSelectionView.show();
            MixEnergetik.UI.TagSelectionView.showRelated();
            MixEnergetik.UI.PersistenceView.show();
            MixEnergetik.UI.CreditsView.showButton();

            MixEnergetik.UI.ChargeView.retract();
            MixEnergetik.UI.SampleListView.retract();
            MixEnergetik.UI.PresetPanelView.retract();
            MixEnergetik.UI.Exploration.retract();

            document.getElementById("DefaultBackground").classList.remove("hidden");
            document.getElementById("ExtendedBackground").classList.add("hidden");
        }
        else
        {
            VideoView_container.classList.add("fullscreen");

            VideoView_display = "VideoView_DISPLAY_FULLSCREEN";

            MixEnergetik.UI.ApplicationControlView.hide();
            MixEnergetik.UI.SequencerView.hide();
            MixEnergetik.UI.VideoSelectionView.hide();
            MixEnergetik.UI.TagSelectionView.hideRelated();
            MixEnergetik.UI.PersistenceView.hide();
            MixEnergetik.UI.CreditsView.hideButton();

            MixEnergetik.UI.ChargeView.extend();
            MixEnergetik.UI.SampleListView.extend();
            MixEnergetik.UI.PresetPanelView.extend();
            MixEnergetik.UI.Exploration.extend();

            document.getElementById("DefaultBackground").classList.add("hidden");
            document.getElementById("ExtendedBackground").classList.remove("hidden");

            /* Now activate sequencer
             * if a preset is activated, deactivate it beforehand */
            MixEnergetik.GroupManager.deactivate();
            MixEnergetik.SequencerManager.activate();
        }
    };

    return VideoView;
})();
