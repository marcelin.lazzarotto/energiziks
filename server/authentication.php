<?php
/*
 *  File:   Authentication.php
 *  Author: Marcelin Lazzarotto
 *  Date:   2019-02-27
 */

const ME_AUTHENTICATION_SUCCESS = 0;
const ME_AUTHENTICATION_FAILURE = 1;

/**
 *  @function tmpGetLogins
 *      TODO temporary.
 *      Function to retrieve the needed login and password for safety.
 *
 *  @param {string} $filename
 *      The file to seek the identifiers from.
 *  @param {& string} $login
 *      The login to return to the caller.
 *  @param {& string} $password
 *      The password to return to the caller.
 */
function authentication_tmpGetLogins($filename, &$login, &$password)
{
    $file = fopen($filename, 'r');

    $login = fgets($file);
    $password = fgets($file);

    $login = str_replace("\n", '', $login);
    $login = str_replace("\r", '', $login);
    $password = str_replace("\n", '', $password);
    $password = str_replace("\r", '', $password);

    $res = fclose($file);
}

/**
 *  @function test
 *      Used to authenticate the user. Basic HTTP auth. TODO to change.
 *
 *  @param {string} $login
 *      The required login of the user. TODO ^
 *  @param {string} $password
 *      The required password of the user. TODO ^
 *
 *  @returns {integer}
 *      ME_AUTHENTICATION_SUCCESS if the user can be authentified.
 *      ME_AUTHENTICATION_FAILURE otherwise.
 */
function authentication_test($login, $password)
{
    if (isset($_SERVER['PHP_AUTH_USER'])
        && (($_SERVER['PHP_AUTH_USER'] != $login)
            || ($_SERVER['PHP_AUTH_PW'] != $password)))
    {
        unset($_SERVER['PHP_AUTH_USER']);
        unset($_SERVER['PHP_AUTH_PW']);
    }

    if (!isset($_SERVER['PHP_AUTH_USER']))
    {
        header('WWW-Authenticate: Basic realm="Droits restreints"');
        header('HTTP/1.0 401 Unauthorized');

        /* This is done in case of cancel */
        return ME_AUTHENTICATION_FAILURE;

        exit;
    }
    else
    {
        return ME_AUTHENTICATION_SUCCESS;
    }
}
?>
